> Human are the sum of various social relationships, Blog are the sum of huaman and contents.

## Of the blog

### What is it?
First of all, as a website, it is a static blog site with Hexo as the framework, Cloudflare CDN node as the data storage, Waline as the commenting system, and the code is hosted on Gitlib, and the owner/webmaster/Blogger is @Tianxinyang/@si-on/@秉蕳 (hereinafter collectively referred to as the owner). It has bennn named as "skyLark-2020", "Foundation-2022" and now name as "Virtual Stack(子虚栈)-2023", which means <u> a virtual, doomed to extinction, a small hut(栈, the original meanings of chinese) on the cloud for stacking learning and life experiences</u>.


Secondly, as a web log, it is a web notebook in which the owner electronically records logs, thoughts, and ideas.

Finally, as a platform, it is a semi-self-media platform for the owner to create content, summarize experiences, and interact with others.

### Where did it come from?
In terms of origin, it is a by-product of the owner's process of learning Linux.

In terms of data, it's powered by Cloudflare, which provides hosting around the world.

In terms of content, it is a visualization of the owner's daily thoughts - in text and images.

As far as the construction of the main body of the site is concerned, it is a combination of many open source projects.

#### The holy supporters of the blog
* Thanks to [@Tommy Chen](https://zespia.me/about/) for developing the Hexo blogging framework
* Thanks to [@CrazyWong](https://crazywong.com/) for developing and <u>continuously maintaining</u> the 🦋Butterfly theme
* Thanks to [Waline](https://github.com/walinejs/waline) for the commenting service
* Thanks to [Github](https://github.com/about) for the Pages, Actions service!
* Thanks to [Vercel](https://vercel.com/about), [Zeabur](zeabur.com), [Railway](https://railway.app/) for deployment services.
* Thanks to [Cloudflare](https://www.cloudflare.com/zh-cn/people/) for the CDN service.
* Thanks to [@Chanzhaoyu](https://github.com/Chanzhaoyu) for [ChatGPT Utility Page Docker](https://github.com/Chanzhaoyu/chatgpt-web)
* Thanks to [Fontawsome](https://fontawesome.com/) for the great icons!
#### The Creaters of blog's writing tools
* Thanks to [@Tony Grosinger](https://grosinger.net/) for developing [advanced-table-plugin](https://github.com/tgrosinger/advanced-tables-obsidian), which facilitates table insertion.
* Thanks to [@PJ Eby](https://github.com/pjeby) for developing [tag-wrangler](https://github.com/pjeby/tag-wrangler), which facilitates the management of tags
* Thanks to [Memos-ers](https://usememos.com/) for developing Memos with [@Boninall](https://github.com/Quorafind/) for developing [Obsidian-memos](https://github.com/) quorafind/obsidian-memos), which facilitates the recording of ideas.
* Thanks to [@shd101wyy (Yiyi Wang)](https://github.com/shd101wyy) for developing [vscode-markdown-preview-enhanced](https://github.com/shd101wyy/vscode- markdown-preview-enhanced) plugin, which facilitates catalog generation.
* In addition, thanks to the following npm plugin and hexo plugin developers, these high-quality plug-ins greatly enrich the functionality of the blog:
```js
    "@neilsustc/markdown-it-katex": "^1.0.0",
    "hexo": "^5.0.0",
    "hexo-abbrlink": "^2.2.1",
    "hexo-auto-category": "^0.2.1",
    "hexo-blog-encrypt": "^3.1.6",
    "hexo-butterfly-charts": "^1.1.3",
    "hexo-butterfly-extjs": "^1.4.4",
    "hexo-butterfly-tag-plugins-plus": "^1.0.17",
    "hexo-calendar": "^1.0.8",
    "hexo-deployer-git": "^3.0.0",
    "hexo-filter-image": "^1.2.3",
    "hexo-generator-archive": "^1.0.0",
    "hexo-generator-category": "^1.0.0",
    "hexo-generator-feed": "^3.0.0",
    "hexo-generator-indexed": "^1.2.1",
    "hexo-generator-search": "^2.4.3",
    "hexo-generator-seo-friendly-sitemap": "^0.2.1",
    "hexo-generator-tag": "^1.0.0",
    "hexo-hide-posts": "^0.2.0",
    "hexo-markmap": "^1.1.7",
    "hexo-pdf": "^1.1.1",
    "hexo-renderer-ejs": "^2.0.0",
    "hexo-renderer-markdown-it-plus": "^1.0.6",
    "hexo-renderer-pug": "^3.0.0",
    "hexo-renderer-stylus": "^2.0.1",
    "hexo-server": "^2.0.0",
    "hexo-tag-aplayer": "^3.0.4",
    "hexo-tag-dplayer": "^0.3.3",
    "hexo-tag-echarts-new": "^1.0.1",
    "hexo-tag-echarts3": "^1.1.2",
    "hexo-wordcount": "^6.0.1",
    "markdown-it-attrs": "^4.1.6",
    "markdown-it-cjk-breaks": "^1.1.3",
    "markdown-it-container": "^3.0.0",
    "markdown-it-task-lists": "^2.1.1",
    "save": "^2.9.0",
    "three": "^0.138.3",
    "uuid": "^8.3.2"

```
#### Other sources
* Thanks to [WhereMyLife](http://wheremylife.cn/home#top) for the Kindle RSS subscription news push service

Thank a billlllion for the book **'Essay Theory'** written by Mr. Ye Shengtao, which guided the development of this site from the second year (2022-8) onwards, and it would not be an exaggeration to call it the bible of this site.

The owner from the beginning of the year of the rabbit, decided to make every August as a month of writing, dedicated to re-reading "Essay Theory", and rectification, reflecting on the previous year's operation of the blog. And on the day of the anniversary then and there (August 25th 15:10:05), submit a well-written summary of the anniversary.
### Where does it go?
In terms of audience targets, the **owners themselves** come first, followed by **people all over the world** who can think rationally, are interested in the content, and treat people with courtesy (here after referred to as visitors).

In terms of direct goals, it is a tool designed to satisfy the social animal to gain acceptance.

In terms of content goals, it will include material science🔬, math, computer science🖥️, philosophy🌌, literature📖, photography📷, etc. which is where the original intent of ❤️ lies.

In terms of the ultimate goal, it aims to allow the owner to continuously improve his or her outlook on life, recognize the self, get rid of his or her own limitations, and explore the ultimate meaning of life in the process of writing.
### What is it not? (2nd anniversary and beyond)
❌ It is not a profit-making tool.

❌ It is not Twitter, Tumbler or a QQ space for filling all kinds of empty desires.

❌ It (tries its best) not to share the fickle, superficial, and perishable experiences and techniques.

### Attitude towards visitors

This site does not collect any single little information from visiters. 

### Interaction and communication
To facilitate communication, visitors can
1. leave a message in the comments section
2. interact by e-mail (the webmaster's e-mail address is me@si-on.top)
3. Discussion groups, the owner operates the following discussion groups (started on 2023-4-21):

	* Telegram: [group link](https://t.me/sionDaoDaoDao)

---

> 人是各种社会关系的总和，博客是人与内容的总和。

## 关于博客站点——子虚栈

### 它是什么？
首先作为网站，它是一个以Hexo为框架的、以Cloudflare CDN节点为数据储存的、以Waline为评论系统的、代码托管于GitHub上的静态博客站点，博主/站长/Blogger为@田欣洋/@sion tian/@秉蕳(以下统称为所有者)。它被命名为『逐云雀-2020』『Foundation-2022』 『子虚栈-2023』其含义<u>虚拟的，注定消亡的，堆栈学习生活经验的云上小木棚</u>。


其次作为网志(Web log)，它是所有者以电子化的方式记录日志、念头、想法的网络笔记本。

最后作为平台，它是所有者创作内容、总结经验，兼与他人进行互动的半自媒体平台。

### 它从哪里来？
就起源而言，它是所有者在学习Linux过程中得到的副产品。

就数据而言，它由Cloudflare提供的分布于世界各地的主机提供的。

就内容而言，它是所有者日常生活中各种念头的具象化——以文本与影像为载体。

就网站主体的筑建而言，它是许多开源项目的组合。
#### 博客骨架的创造者们
* 感谢 [@陳嘉輝（Tommy Chen）](https://zespia.me/about/) 开发的Hexo博客框架
* 感谢 [@CrazyWong](https://crazywong.com/) 开发并<u>不断维护</u>的🦋Butterfly主题
* 感谢 [Waline](https://github.com/walinejs/waline) 提供的评论服务
* 感谢 [Github](https://github.com/about) 提供的Pages、Actions服务
* 感谢 [Vercel](https://vercel.com/about) 、 [Zeabur](zeabur.com) 、[Railway](https://railway.app/) 提供的部署服务
* 感谢 [Cloudflare](https://www.cloudflare.com/zh-cn/people/) 提供的CDN服务
* 感谢 [@Chanzhaoyu](https://github.com/Chanzhaoyu) 提供的[ChatGPT实用网页Docker](https://github.com/Chanzhaoyu/chatgpt-web)
* 感谢 [Fontawsome](https://fontawesome.com/) 提供的精美图标
#### 博客写作工具的创造者们
* 感谢 [@Licat（Shida Li）@Silver（Erica Xu）](https://obsidian.md/about) 等人开发的Obsidian笔记软件，方便了博客的写作与管理
* 感谢 微软开发的 [Visual Studio Code](https://github.com/Microsoft/vscode/)，方便了博客的部署与管理
#### 博客插件的创造者们
* 感谢 [@Tony Grosinger](https://grosinger.net/)开发的[先进表格插件](https://github.com/tgrosinger/advanced-tables-obsidian)，方便了表格的插入
* 感谢 [@PJ Eby](https://github.com/pjeby)开发的[tag-wrangler](https://github.com/pjeby/tag-wrangler) 方便了标签的管理
* 感谢 [Memos-ers](https://usememos.com/)开发的Memos 与[ @Boninall](https://github.com/Quorafind/) 开发的 [Obsidian-memos](https://github.com/quorafind/obsidian-memos)，方便了想法的记录
* 感谢 [@shd101wyy (Yiyi Wang)](https://github.com/shd101wyy) 开发的[vscode-markdown-preview-enhanced](https://github.com/shd101wyy/vscode-markdown-preview-enhanced)插件，方便了目录的生成
* 此外还感谢如下npm插件与hexo插件的开发者，这些优质插件极大地丰富了博客的功能：
```json
 "@neilsustc/markdown-it-katex": "^1.0.0",
    "hexo": "^5.0.0",
    "hexo-abbrlink": "^2.2.1",
    "hexo-auto-category": "^0.2.1",
    "hexo-blog-encrypt": "^3.1.6",
    "hexo-butterfly-charts": "^1.1.3",
    "hexo-butterfly-extjs": "^1.4.4",
    "hexo-butterfly-tag-plugins-plus": "^1.0.17",
    "hexo-calendar": "^1.0.8",
    "hexo-deployer-git": "^3.0.0",
    "hexo-filter-image": "^1.2.3",
    "hexo-generator-archive": "^1.0.0",
    "hexo-generator-category": "^1.0.0",
    "hexo-generator-feed": "^3.0.0",
    "hexo-generator-indexed": "^1.2.1",
    "hexo-generator-search": "^2.4.3",
    "hexo-generator-seo-friendly-sitemap": "^0.2.1",
    "hexo-generator-tag": "^1.0.0",
    "hexo-hide-posts": "^0.2.0",
    "hexo-markmap": "^1.1.7",
    "hexo-pdf": "^1.1.1",
    "hexo-renderer-ejs": "^2.0.0",
    "hexo-renderer-markdown-it-plus": "^1.0.6",
    "hexo-renderer-pug": "^3.0.0",
    "hexo-renderer-stylus": "^2.0.1",
    "hexo-server": "^2.0.0",
    "hexo-tag-aplayer": "^3.0.4",
    "hexo-tag-dplayer": "^0.3.3",
    "hexo-tag-echarts-new": "^1.0.1",
    "hexo-tag-echarts3": "^1.1.2",
    "hexo-wordcount": "^6.0.1",
    "markdown-it-attrs": "^4.1.6",
    "markdown-it-cjk-breaks": "^1.1.3",
    "markdown-it-container": "^3.0.0",
    "markdown-it-task-lists": "^2.1.1",
    "save": "^2.9.0",
    "three": "^0.138.3",
    "uuid": "^8.3.2"
```

#### 其他的源头活水
* 感谢 [WhereMyLife](http://wheremylife.cn/home#top) 提供的Kindle RSS 订阅新闻推送服务

特别、尤其、非常、极其、严重 感谢叶圣陶老师所著的『作文论』，这本书指导了本站第二年(2022-8)往后的发展，称它为本站的圣经都不为过。

所有者从癸卯兔年开始，决定把每年八月定为写作月，专门重读『作文论』，并整改、反思博客之前一年的经营。并在周年的当天当时当分当秒(8月25日 15:10:05)，提交撰写好的周年总结。
### 它到哪里去？
就受众目标而言，**所有者本身**首当其冲，其次为**能理性思考、对内容有兴趣、以礼待人的**世界各国同胞(以下简称来访者)。

就直接目标而言，它是一个为了满足社会性动物获得认同的工具。

就内容目标而言，它将包含材料科学、数学、计算机科学、哲学、文学、摄影等方面的内容，此亦即初心❤️所在。

就终极目标而言，它的目的在于：让所有者在写作过程中不断完善人生观，认识自我，摆脱自身局限性，探寻人生的终极意义。
### 它不是什么？（二周年往后）
❌ 它不是一个盈利的工具。

❌ 它不是微博、朋友圈、QQ空间，埋填各种空虚的欲望。

❌ 它(尽力)不会分享浮躁的、肤浅的、速朽的经验技巧。

### 对于来访者的态度

本站不收集任何访客信息。

### 互动与交流
为了方便交流，来访者可以
1. 评论区留言
2. 邮件互动（站长邮箱 me@si-on.top）
3. 讨论组，所有者经营有如下讨论组(开始于2023-4-21)：
	 * Telegram：[群组链接](https://t.me/sionDaoDaoDao) 
