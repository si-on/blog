---
title: 简历
date: 2023-12-17 19:13
comments: false
top_img: 
aside: false
highlight_shrink: true
description: 如所见，这是田欣洋的个人简历页
---

## 基本信息
{% pullquote left %}
![me](../../images/resume/me.webp)
![](../../../images/20230709/地下室手记-20231223213822468.webp)
{% endpullquote %}

* 姓名：田欣洋
* 性别：男
* 政治面貌:共青团员
* 学校: 新疆大学
* 现居地: 宁波·鄞州区
* 学位：工学学士
## 简介
本人为新疆大学 2023 级本科毕业生, 所学专业为{% emp 材料成型及控制工程 %} . 擅
长{% label 力学分析与材料结构设计 blue %}、{% label 金属性能优化 blue %}、{% label 铸造成型 blue %}等。   
在校期间,曾参与过周培源力学竞赛获得全国三等奖;并在老师指导下完成
了钛合金固溶时效强化的 (包括式样加工成型、热处理、金相制备、XRD 分析
等)，<u>对铁、钛金属材料的性能优化与表征手段基本了解</u>。擅长使用 AutoCAD、
UG 对模型进行<u>草图绘制与三维建模</u>、使用 ANSYS 对模型的性能进行<u>有限元
分析</u>模拟。系统性学习过<u>统计分析</u>,可以使用 R 语言对数据进行清洗、可视
化分析。熟悉$\LaTeX$、Word 排版软件、Zotero 文献管理软件、精通 PPT,可
以对实验报告进行很好的呈现。
{% progress 76 blue 专业GPA(3.54/5.0) %}  
## 技能
{% progress 90 blue AutoCAD 与 UG(熟练) %}  
{% progress 90 blue ANSYS(熟练) %}  
{% progress 80 blue 金属材料性能优化(熟悉) %}  
{% progress 70 blue R语言与数据分析(较熟悉) %}  
{% progress 60 blue MatLab(较熟悉) %}  
{% progress 60 blue C语言与Python(了解) %}  
{% progress 95 blue PPT(熟练) %}  
![](../../images/resume/成绩单.webp)



## 实习经历
**课程实训 | (皆担任组长)**          工程训练中心·新疆大学 10/2021 - 6/2023

* 大二期间在老师的指导下,组织小组成员,完成了减速器、冷水机组 (空调)
复杂三维模型的建立与组装；
<div class="w-full overflow-hidden rounded" style="height: 90vh;"><iframe src="https://mozilla.github.io/pdf.js/web/viewer.html?file=https://cloud.si-on.top/api/raw/?path=/myworks/2021.6_CDIO%E8%AF%BE%E7%A8%8B%E8%AE%BE%E8%AE%A1%E8%AF%B4%E6%98%8E%E4%B9%A6.pdf" frameborder="0" width="100%" height="100%"></iframe></div>
<div class="w-full overflow-hidden rounded" style="height: 90vh;"><iframe src="https://mozilla.github.io/pdf.js/web/viewer.html?file=https://cloud.si-on.top/api/raw/?path=/myworks/2021.7_%E6%9C%BA%E6%A2%B0%E8%AE%BE%E8%AE%A1%E5%9F%BA%E7%A1%80%E8%AF%BE%E7%A8%8B%E8%AE%BE%E8%AE%A1.pdf" frameborder="0" width="100%" height="100%"></iframe></div>
* 大三时组织小组成员学习 ANSYS,进行了减速器整体与各个部件的有限元分析 (本人负责齿轮啮合部分),得到了指导老师(副教授) 的深切肯定;
<div class="w-full overflow-hidden rounded" style="height: 90vh;"><iframe src="https://mozilla.github.io/pdf.js/web/viewer.html?file=https://cloud.si-on.top/api/raw/?path=/myworks/2022.2_%E5%87%8F%E9%80%9F%E5%99%A8%E9%BD%BF%E8%BD%AE%E5%95%AE%E5%90%88%E6%9C%89%E9%99%90%E5%85%83%E5%88%86%E6%9E%90.pdf" frameborder="0" width="100%" height="100%"></iframe></div>
* 大四设计进行了钛合金的固溶时效强化实验,得到了指导老师的肯定。
<div class="w-full overflow-hidden rounded" style="height: 90vh;"><iframe src="https://mozilla.github.io/pdf.js/web/viewer.html?file=https://cloud.si-on.top/api/raw/?path=/myworks/2023.6_%E7%83%AD%E5%A4%84%E7%90%86%E6%B8%A9%E5%BA%A6%E5%8F%8A%E5%86%B7%E5%8D%B4%E9%80%9F%E5%BA%A6%E5%AF%B9Ti6Al4V%E7%BB%84%E7%BB%87%E5%92%8C%E5%8A%9B%E5%AD%A6%E6%80%A7%E8%83%BD%E7%9A%84%E5%BD%B1%E5%93%8D.pdf" frameborder="0" width="100%" height="100%"></iframe></div>
## 荣誉证书

{% gallery %}
![四级证书](../../images/resume/四级.webp)
![六级证书](../../images/resume/六级.webp)



{% endgallery %}

----
{% gallery %}
![](../../images/resume/力学竞赛.webp)
![](../../images/resume/工训赛.webp)
![](../../images/resume/IMG.webp)
![](../../images/resume/优秀志愿者.webp)
{% endgallery %}
