---
title: 如何编译一本书
tags:
  - LaTeX
  - 奇技淫巧
categories:
  - 学习
  - ⌨️软件编程
  - latex
cover: /images/blogimg/2022%E5%AE%B6%E8%B0%B1%E5%9B%BE_%E7%94%BB%E6%9D%BF%201.png
aside: false
description: 关于通过tex/md来生成Epub、mobi格式的说明，以及通过gitlab CI编译的方法
abbrlink: genealogytree
date: 2022-03-05 12:37:41
katex:
comments:
published: false
copyright:
password:
hidden:
---


厌倦了发说说吗？厌倦了Memo了吗？厌倦了写博客了吗？   
想要不朽吗？想要自己在一两百年后还能被人记起吗？   
那么是时候去写一本书了！

~~不知道写什么~~？（妈的，自己不知道还让灶王爷告诉你不成）不知道怎么操作？真的吗？那你来对地方了，今天，就在这里，咱家教你！读了这篇文章，你可以学会：

2. 怎样通过tex或者md来生成pdf、epub、mobi等格式的电子书
3. 通过git来编辑、修改、发布一本书
4. gitlab CI/CD的基本使用
##  tex4ebook 宏包
tex4ebook宏包可以用于生成各种格式的电子书，下面来介绍下这个新朋友吧：

### 基本使用
该宏包可以通过两种方式运行，一是通过命令行指令：
```bash 
tex4ebook [options] mybook.tex
```

### 最小示例



## gitlab编译
参考文件[^1]

 

[^1]: [Compiling LaTeX documents with GitLab CI · Wiki · Island of TeX / images / texlive · GitLab](https://gitlab.com/islandoftex/images/texlive/-/wikis/Compiling-LaTeX-documents-with-GitLab-CI)