---
title: 数据可视化
abbrlink: unnamedPost
categories: 
tags: 
date: 2023-12-23 16:02:06
updated: 2023-12-23 16:02:06
cover: 
root: ../../
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
katex: false
password: 
theme: xray
hidden: 
published: true
description: 
sticky: 
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}

{% endmarkmap %}

-----
## 随记

> Matplotlib和Pyecharts是Python中常用的两个可视化库，其功能强大，可以方便地绘制折线图、条形图、柱形图、散点图等基础图形，还可以绘制复杂的图形，如日历图、树形图、聚类图等。

我的目的很简单，学会Matplotlib与Pyecharts的使用。
1. matplotlib与matlab相近
2. pyecharts与Echarts有很多关联

所以，这两者本质上不过是Python的一个小插件而已。

数据可视化的工作核心是流程化，样式统一化，不能过于个性，否则那样化出来的东西修改起来是很麻烦的。

## 配置
拒绝累赘，拒绝集成化，边用边安装：