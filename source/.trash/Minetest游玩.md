---
title: Minetest游玩
abbrlink: unnamedPost
categories:
  - 学习
  - ⌨️软件编程
root: ../../
katex: false
theme: xray
published: false
date: 2023-10-21 21:05:04
updated: 2023-10-21 21:05:04
tags: 
cover: 
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
password: 
hidden: 
description: 
sticky: 
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->



<!-- /code_chunk_output -->
{% endmarkmap %}

-----

![开干！](../../../images/20230709/Pasted%20image%2020231021210513.png)