---
title: 留言板
mathjax: 
date: 2023-12-12 
updated:
type:
aside: false
description: 欢迎留言
keywords: 
top_img: "repeating-linear-gradient(90deg, hsla(285,0%,67%,0.05) 0px, hsla(285,0%,67%,0.05) 1px,transparent 1px, transparent 96px),repeating-linear-gradient(0deg, hsla(285,0%,67%,0.05) 0px, hsla(285,0%,67%,0.05) 1px,transparent 1px, transparent 96px),repeating-linear-gradient(0deg, hsla(285,0%,67%,0.08) 0px, hsla(285,0%,67%,0.08) 1px,transparent 1px, transparent 12px),repeating-linear-gradient(90deg, hsla(285,0%,67%,0.08) 0px, hsla(285,0%,67%,0.08) 1px,transparent 1px, transparent 12px),linear-gradient(90deg, rgb(79, 30, 203),rgb(252,69,69));"
aplayer: true
comments: false
highlight_shrink:
---

欢迎来到留言板，在这里可以收听广播📻：点击发现电台，然后搜索即可，推荐“轻松调频”、“文艺之声”、“中国之声”，电台数据来自于[radio-browser](https://www.radio-browser.info)，大部分外国的电台也是可以收到的，请畅所欲听吧！


----
