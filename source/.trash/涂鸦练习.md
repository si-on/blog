---
title: 涂鸦练习
abbrlink: tuuu
categories:
  - 学习
  - "\U0001F30C科学艺术"
  - "\U0001F58C️绘事"
root: ../../
katex: false
theme: xray
published: false
date: 2023-11-01 09:14:19
updated: 2023-11-01 09:14:19
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>
## 模仿练习

