---
title: 木叶如雪
abbrlink: leaves
categories:
  - 生活
  - ✒️随笔
tags: 
root: ../../
katex: false
theme: xray
published: false
date: 2023-12-05 20:53:24
updated: 2023-12-05 20:53:24
cover: /images/Cover/leaves.svg
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---

> <center>向生活求救。</center>
> <p align="right">——Sion</p>

近些日子，心里颇是焦躁，怎样也静不下心。
