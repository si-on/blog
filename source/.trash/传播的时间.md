---
title: 传播的时间
abbrlink: unnamedPost
categories: 
tags: 
date: 2023-10-26 08:30:11
updated: 2023-10-26 08:30:11
cover: 
root: ../../
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
katex: false
password: 
theme: xray
hidden: 
published: true
description: 
sticky: 
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->



<!-- /code_chunk_output -->
{% endmarkmap %}

-----
