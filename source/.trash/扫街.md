---
title: 扫街
abbrlink: Shot4meaningless
categories:
  - 学习
  - "\U0001F30C科学艺术"
  - "\U0001F4F7摄影思考"
  - "\U0001F5BC️摄影练习"
root: ../../
katex: false
theme: xray
published: true
date: 2024-08-24 04:43:09
updated: 2024-08-24 04:43:09
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
<!-- timeline 2023-08-01-->
 
<!-- endtimeline -->
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}

{% endmarkmap %}

-----

从八月五号，相机买来有半个月了，