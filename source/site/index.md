---
title: 子虚栈志📜
mathjax: true
date: 2022-02-18 21:50:32
updated: 
type: 
description: 本部落阁的通览，包含活动日历、文章、分类、标签统计；事略；小事志。
keywords: 
top_img: repeating-linear-gradient(90deg, rgba(198, 198, 198,0.05) 0px, rgba(198, 198, 198,0.05) 1px,transparent 1px, transparent 5px),repeating-linear-gradient(0deg, rgba(198, 198, 198,0.05) 0px, rgba(198, 198, 198,0.05) 1px,transparent 1px, transparent 5px),repeating-linear-gradient(0deg, rgba(198, 198, 198,0.06) 0px, rgba(198, 198, 198,0.06) 1px,transparent 1px, transparent 15px),repeating-linear-gradient(90deg, rgba(198, 198, 198,0.06) 0px, rgba(198, 198, 198,0.06) 1px,transparent 1px, transparent 15px),linear-gradient(90deg, rgba(23,72,170, 0.9),rgba(23,72,170, 0.9));
aplayer: 
aside: false
highlight_shrink: true
---
 <center>
{% bdage 主题,🦋Butterfly||, https://github.com/jerryc127/hexo-theme-butterfly %}  
{% bdage 博客框架 ,Hexo,hexo||0E83CD, https://hexo.io/zh-cn/ %}
{% bdage 部署,cloudflare,cloudflare||brightgreen,https://cloudflare.com/,本站采用双线部署，默认线路托管于Cloudflare||style=social&logoWidth=20 %}  
{% bdage 源码(公开),Gitlab,Gitlab||FCA326, https://gitlab.com/si-on/blog%}
{% bdage 评论,Waline||, https://waline.js.org/ %}  
 </center>
 <center>
{% calendar %}
{"monthLang": "cn", "dayLang": "cn", "title": "博客代码仓库--活动历", "weeks": "30", "width": "100%"}
{% endcalendar %}
</center>
{% heatmapchart forest 2025 no-footer %}博客文章热力图{% endheatmapchart %}
<!-- 文章发布时间统计图 -->
<div id="posts-chart" data-start="2021-05" style="border-radius: 8px; height: 300px; padding: 10px;"></div>
<!-- 文章标签统计图 -->
<!-- <div id="tags-chart" data-length="15" style="border-radius: 8px; height: 300px; padding: 10px;"></div> -->
<!-- 文章分类统计图 -->
{% radarchart years %}文章年度雷达图{% endradarchart %}
{% piechart tags no-footer  %}标签饼图{% endpiechart %}
<div id="categories-chart" data-parent="true" style="border-radius: 8px; height: 500px; padding: 10px;"></div>
<div id="categories-chart" data-parent="true" style="border-radius: 8px; height: 1px; padding: 10px;"></div>
<!-- <div id="posts-calendar" class="js-pjax"></div> -->
<div id="posts-chart" class="js-pjax"></div>
<div id="tags-chart" data-length="20" class="js-pjax"></div>
<!-- <div class="btn-center"> -->
<!--{% btn '/categories/Code/',编程,far fa-hand-point-right,outline blue larger %}-->
<!--{% btn '/categories/周刊',周刊,far fa-hand-point-right,outline pink larger %}-->
<!-- {% btn '/categories/数学/',数学,far fa-hand-point-right,outline red larger %}
{% btn '/categories/杂谈/',杂谈,far fa-hand-point-right,outline purple larger %}
{% btn '/categories/材料科学/',材料科学,far fa-hand-point-right,outline orange larger %} -->
<!--{% btn '/categories/Hexo/',Hexo,far fa-hand-point-right,outline green larger %}-->
<!-- </div> -->

{% folding red,2020年博客大事记%}
{% timeline 🐭庚子2020,red %}
<!-- timeline 2-29 万恶之源☠-->
由于电脑出现故障，进入不了系统，在网上买了急救盘来自救，用Pe试了几次确定是原固态盘炸了。下午在其机械盘上尝试安装Linux并成功安装上Deepin15。搞机生涯正式开始。
<!-- endtimeline -->
<!-- timeline 3-10 -->
第一次装上Deepin+Windows10双系统
<!-- endtimeline -->
<!-- timeline 3-20 -->
在学习Linux过程中逐渐萌发建站的念头，当天购买阿里云轻量级应用服务器，使用LNMP+WordPress建站。
* 域名 ：[ **sky-lark.top**](https://sky-lark.top)
<!-- ![逐云雀·新希望](https://cdn.jsdelivr.net/gh/aornus/blogimg/2022b&amp_bo_HgedAwAAAAARALA_&rf_viewer_311.jpg) --> 

<!-- endtimeline -->

<!-- timeline 4-15 -->
备案成功，我的小世界：sky-lark.top开放了！
<!-- endtimeline -->
<!-- timeline 6-15 -->
重装系统为centos，使用宝塔后台+Wordpress休整。

<!-- endtimeline -->

<!-- timeline 9-10 音乐整理-->
* 手写了歌单
* 注销了豆瓣账号(虽然只用了三个月)，将音乐来源缩小到Spotify上。

<!-- endtimeline -->

<!-- timeline 9-17 -->
读勃朗宁夫人的十四行诗，觉得**Aornus**(印度的山，意为无鸟山)这个词很好听。

<!-- endtimeline -->

<!-- timeline 10-1 -->
网站被土耳其黑客入侵，你们不是站起来的狼，是死的不能再死的死狗！   
恢复到9-25，还好只丢失了几天的数据。   

<!-- endtimeline -->

{% endtimeline %}

{% endfolding %}

{% folding orange,2021年博客大事记 %}
{% timeline 🐮辛丑2021,orange %}
<!-- timeline 1-1  -->
撰写了2021年的宣言——『蓝鸟宣言』，作为今年的指导思想：(2021-9修改润色)
>扬扬白水，寂寂蓝鸟。   
>~~青天尽处，碧空如洗。~~  江天一尽，棘葛千纷。   
>岂滞淹留，怎束苟且。   
>云舒云卷，无论无非。   
>~~因时而潜，因时而翔。~~  月明月晦，时翔时潜。     
>秉梦律行，若痴若愚。   
>~~心悲心喜，沸之盈之。~~ 悲乐纤尘，心沸且腾。     

<!-- endtimeline -->
<!-- timeline 1-2  -->
将网站《逐云雀》清空，更换为CentOS系统，安装宝塔界面。使用网页上传工具，传输了WordPress的源码，更新了PHP8.0。(手机上用JetPack还是不行💀️)
>使用SSH连接，从外网登陆后，在Chrome浏览器中进行管理

<!-- endtimeline -->
<!-- timeline 1-3 -->
将网站定位为 **知识传授、总结与个人兴趣的自留地**。
* 添加菜单『力学探索』、『欣洋自留地』
* JetPack恢复
* 实现了X-Plor文件管理器上传文件(通过FTP)

展望：到二月底，筑建基本框架(PHP、HTML的编辑能力，设计自己的博客)；到三月底，可以利用Linux与实体单片机互动，并实时分析数据，实现小车云定位
<!-- endtimeline -->

<!-- timeline 2-14 -->
有網不設，斯可惜也；設而不文，斯可悲也；既文而毀之，斯可恥也 無慾者，方可經營博客. *小雲雀 skylark.top 的心酸經歷*
<!-- endtimeline -->

<!-- timeline 3-25 -->
域名与服务器到期，觉得没什么意思，没有续期，一月份的展望基本烂尾，第一次建站结束。   
最后导出的WordPress数据丢失，除了OneNote里一些笔记外，什么也没有留下。

<!-- endtimeline -->

<!-- timeline 5-15 -->
得知git托管平台+hexo可以免费建站，初次尝试，但失败。

<!-- endtimeline -->

<!-- timeline 08-25 -->
**摸索了一个下午，成功在本地termux上部署hexo,配合gitee的pages服务(等了几个月了)，第二次建站开始。**
以第一次代码提交的时间：`2021/8/25 15:10:05`，为此博客的诞生时间。
* 域名 ：**aornus.gitee.io**

<!-- endtimeline -->

<!-- timeline 09-07 -->

* 修改主题为`butterfly`，爱不释手。
* 建立了Github上的镜像站。

<!-- endtimeline --> 

<!-- timeline 09-13 -->

增加数学公式的支持，美化了页面。

<!-- endtimeline --> 
<!-- timeline 11-19 -->
设计了头图。

<!-- endtimeline --> 
<!-- timeline 11-25 -->

进入沉静期，为期一月：封笔。

<!-- endtimeline --> 

<!-- timeline 12-27 -->
沉静期结束：删除了一些不必要的内容，更新了一些学习笔记。

<!-- endtimeline --> 


{% endtimeline %}

{% endfolding %}

{% folding yellow,2022年博客大事记%}
{% folding black,2022年快照(netlify)%}
<iframe style="border-radius:12px" src="https://2022.blog.si-on.top" width="100%" height="719.8" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
{% endfolding %}
{% timeline 🐯壬寅2022, yellow %}


<!-- timeline 02-05 -->
开始紫晶子计划-**金石周刊**，
<!-- endtimeline --> 

<!-- timeline 02-13 -->
转移部署到githubpages，

* 域名 ：**aornus.github.io**
<!-- endtimeline --> 

<!-- timeline 02-14 -->
使用Freenom免费域名服务+Cloudflare CDN

* 域名 ：**aornus.tk**
<!-- endtimeline --> 

<!-- timeline 02-20 -->
起了一个英文名sion，更改域名

* 域名：**sion.tk**
<!-- endtimeline --> 

<!-- timeline 02-22 -->
“贰”的狂欢：更换主字体为思源宋体。
<!-- endtimeline --> 

<!-- timeline 03-09 -->
购买新域名，配合cloudflare反代了tg频道

* 新域名:  [**si-on.top**]( https://si-on.top)
* Telegram Channel:  [**Thus Speak Sion**]( https://tg.si-on.top)

<!-- endtimeline --> 

<!-- timeline 03-18 -->
添加了 mindmap 与 echarts支持，
<!-- endtimeline --> 

<!-- timeline 03-19 -->
更换评论系统vline 为[Wline](https://waline.js.org/)
<!-- endtimeline --> 

<!-- timeline 03-24 -->
建立阅读与写作专用子域🪶--"细墨"：

* 域名: https://ink.si-on.top/
<!-- endtimeline -->

<!-- timeline 03-28 -->
成功加入十年之约，虫洞走起!
<a href="https://www.foreverblog.cn/go.html" target="_blank"> <img src="https://img.foreverblog.cn/wormhole_3_tp.gif" alt="" style="width:auto;height:32px;" title=" "></a>
<!-- endtimeline -->

<!-- timeline 03-29 -->
利用Vercel建立onedrive云盘，主要存是一些学习资料与技术文档，~~也会不定期更新[外刊](https://sionedrive.vercel.app/journal)~~

* SiOneDrive：https://sionedrive.vercel.app
<!-- endtimeline -->

<!-- timeline 04-15 -->
关闭访问量、阅读量统计以及访客地图。创建了主页
* Home：https://home.si-on.top/
> 2022-6-15：已经作为[主页](https://www.si-on.top)启用
<!-- endtimeline -->

<!-- timeline 04-16 -->
听说Vercel速度不错，开启了镜像站，顺便绑定上上了之前的两个域名。
1. [Vercel镜像站](http://sion.tk/)=[vercel](https://sion-eta.vercel.app/)
2. [Netify镜像站](https://aornus.tk)=[netlify](https://625a6bec72983054d0757833--cheery-fudge-471d5e.netlify.app/)

网站源码转移到github上。由于找不到哪里有违禁内容，一个多月来，giteepages一直部署不了。4月16日，彻底跟gitee断绝关系。  
<!-- endtimeline -->

<!-- timeline 04-17 -->
~~开启赞赏功能--加密货币（赞赏二维码采用极限识别设计，inspired by Minor's book.）~~
>（某月某日关闭）
<!-- endtimeline -->

<!-- timeline 05-19 -->
琐事繁杂，心神涣散，短暂放空，为期一月。    
红鱼（核心要义：删繁就简、奥卡姆剃刀原则）计划正式启动
<!-- endtimeline -->

<!-- timeline 6-4 -->
* ~~添加[摘句页面](/clip)，主要是来用kindle读书过程中的杂七杂八的混乱想法，通过kindle fere 生成。~~
* ~~添加[笔记](/cache)，想了好久了，每次不凑巧想记些东西都不知道放哪里好。~~
>（以上两个页面于某月某日关闭）
* 菜单描述微调
<!-- endtimeline -->

<!-- timeline 06-10 -->
博客部署到IPFS (interplanetary File System)

* 域名: [ipfs.si-on.top](https://ipfs.si-on.top/)
<!-- endtimeline -->

<!-- timeline 06-15 -->
域名整合:把各个镜像站以及几个小的工具站点都整合到主域名下的二级域名下,将home页设置为了主页。
* 主页：[www.si-on.top](https://www.si-on.top/)
    * 博客：[blog.si-on.top](https://blog.si-on.top/)
    * ~~云盘：~~[cloud.si-on.top](https://cloud.si-on.top/)(Onedrive index项目跑路，站点已关闭)
    * ~~笔记：[note.si-on.top](https://note.si-on.top/)~~（于某月某日关闭））
    * ~~短文：[ink.si-on.top](https://ink.si-on.top/)~~（已合并到主站）
    * ~~镜一：~~[vercel.si-on.top](https://vercel.si-on.top/)(已关闭子域名，作为主站使用)
    * ~~镜二：[ipfs.si-on.top](https://ipfs.si-on.top/)~~（11-22 删除后无法重新部署，废弃之）
    * ~~镜三：[nelify.si-on.top](https://netlify.si-on.top/)~~（12-5 检查，停留在四个月前）
    * ~~国镜：[si-on.gitee.io](https://si-on.gitee.io/)~~ (6-19 恢复了gitee国内镜像)
<!-- endtimeline -->

<!-- timeline 08-14 -->
当gitee开始审核图片的时候，我彻底明白了：独立博客国内的限制只会越来越多。便果断放弃，彻底删除了码云上的所有仓库，提交了销号申请。
<!-- endtimeline -->

<!-- timeline 08-21 -->
参考网上教程，对博客页面进行了修改
 * ~~首页轮播图~~
 * 表格统计
<!-- endtimeline -->

<!-- timeline 9-11 -->
添加读书笔记页面[阅读之旅](https://sionreading.notion.site/)
> 参考了Reimu的[Books](https://muzi502.notion.site/muzi502/284c181664924fa485e94c12b6cabfb1)
<!-- endtimeline -->

<!-- timeline 11-05 -->
对分类与标签进行了重新规划
<!-- endtimeline -->


<!-- timeline 11-19 -->
* 搭配Obsidian，Nextcloud实现了多平台同步写作。
* ~~添加了批注~~(于12-5删除)
* ~~添加了Newsletter电子报订阅~~(于12-7日删除)
<!-- endtimeline -->

<!-- timeline 11-23 -->
* ~~更改短文站点(又名“细墨”)为直排[ink.si-on.top](https://ink.si-on.top) (或为第一个直排hexo主题)~~(已更改为paper主题，已合并到主站)
* 开启Rss3上的站点[乌有斋](https://rss3.si-on.top)（更名为[乌有阁](https://rss3.si-on.top)；2023-04-04更改为“空影”，仍取子虚乌有之意)
<!-- endtimeline -->

<!-- timeline 11-29 -->
文章开头添加了目录导图，感谢项目：[hexo-markmap](https://www.npmjs.com/package/hexo-markmap)以及[markdown-preview-enhanced](https://shd101wyy.github.io/markdown-preview-enhanced/)
<!-- endtimeline -->

<!-- timeline 12-7 -->
* 完善了[关于](/about)页面
* 修改网站名为——子虚栈
* 修改id为——秉蕳
<!-- endtimeline -->
{% endtimeline %}

{% endfolding %}


{% folding green, 2023年博客大事记%}
{% folding black,2023年快照(Github+Vercel)%}
<iframe style="border-radius:12px" src="https://2023.blog.si-on.top" width="100%" height="719.8" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
{% endfolding %}
{% timeline 🐰癸卯2023, green %}
<!-- timeline 01-03 -->
明确了博客的定位：表达我，批评我，反思我。 我表达，我批评，我反思。
<!-- endtimeline -->

<!-- timeline 01-05 -->
建立了基于树莓派的NAS系统与网盘聚合系统(感谢arozOS与Alist开源项目)
* ~~[NAS系统](https://nas.*si-on.top)~~
* ~~[网盘聚合](https://hub.si-on.top)~~(树莓派炸了，死活连不上网，于2023-8-25彻底放弃)
* ~~[开启了umami的访客统计服务](https://cloud.umami.is/analytics/websites/69effd41-cf9a-4c01-8076-cc697a15e7e6)~~(影响速度，于2023.3.9关闭)
<!-- endtimeline -->

<!-- timeline 02-18 -->
* 优化了博客构建流程，转移到vercel进行自动部署
* 文章分类优化为“学习”与“生活”两大部分
* 【聚沙成塔·九九九】计划结束
<!-- endtimeline -->


<!-- timeline 03-03 -->
* 续费了两年的域名（￥62）
* 博客文章封面优化
* 标签删简

<!-- endtimeline -->

<!-- timeline 03-07 -->
- [x] 添加了迟到的todo list支持，感谢[markdown-it-task-lists](https://github.com/revin/markdown-it-task-lists)、[hexo-renderer-markdown-it](https://github.com/hexojs/hexo-renderer-markdown-it)项目
- [x] 更改了淡色方格子背景
- ~~部署了ChatGPT服务：chat.si-on.top~~(六月份秘钥失效，已删除部署)
<!-- endtimeline -->

<!-- timeline 03-10 -->
- 丰富了waline评论的样式
- 使用[Gradient Magic](https://www.gradientmagic.com/)提供的css来替代部分封面图
<!-- endtimeline -->

<!-- timeline 03-22 -->
* ~~通过 [zeabur](zeabur.com) 提供的服务建立了[Memos](https://seed.si-on.top)页面，采用Cloudflare [R2](https://r2.si-on.top) 作为储存桶~~(已弃置)
<!-- endtimeline -->
<!-- timeline 03-23 刻意推广 -->
- ~~加入了个站商店：[商店主页](https://storeweb.cn/site/o/1698)~~(04-05日关闭)
- 加入了中文独立博客：[timqian/chinese-independent-blogs@da0c433](https://github.com/timqian/chinese-independent-blogs/commit/da0c43349c831151092bad3b08067a67cb68b0da)
- ~~申请加入Traveling ：[Issue #1490 · travellings-link/travellings ](https://github.com/travellings-link/travellings/issues/1490)~~(04-21日关闭)
- ~~申请加入RSSblog：[Issue #22 · caibingcheng/rssblog ](https://github.com/caibingcheng/rssblog/issues/22)~~(04-21日关闭)
<!-- endtimeline -->

<!-- timeline 03-25 -->
* 更改了日志的展示：只在feed与sitemap中隐蔽；在其他区域显示
* ~~感谢Zeabur，部署了Alist云盘聚合站：https://alist.si-on.top（于3-27日关闭，我再也不乱白嫖了！）~~
<!-- endtimeline -->

<!-- timeline 03-28 -->
开启了繁体中文支持
<!-- endtimeline -->

<!-- timeline 03-31 -->
开始沉静期，解冻时间：4-21日开斋节，期间个人调整：
1. 暂时关闭本站评论（如需联系，请Email我：mailto:me@si-on.top）
2. 只简单记录日志，更新论文，并停止一切不必要文章写作
3. 停止发布照片到500px、图虫等平台
<!-- endtimeline -->


<!-- timeline 04-03 -->
1. 把Xlog上的文章转移到主站
2. 采用CloudFlare的R2作为图床
<!-- endtimeline -->


<!-- timeline 04-05 -->
1. 隐藏了2月份以前字数比较少的日记
2. 对部分文章进行了调整
> ~~感谢个站商店管理员提出的**本站内容大部分是抄袭**的宝贵意见（个屁）~~
<!-- endtimeline -->

<!-- timeline 04-15 (Feature+++) -->
1. ~~更改字体为思源宋体~~
2. 更改配色为$\text{\color{yellow}活力黄}$
3. 更改点击特效为文字；美化各层级标题样式；~~修改段落`<p>`缩进为`2em`~~;
3. 删除文章日历
4. ~~增加文章编辑入口（如果有来访者发现文章存在问题，或者想要添加/删减内容，欢迎在源文件上进行修改）~~
4. 增加博客仓库提交日历图，感谢项目：[HCLonely/hexo-calendar](https://github.com/HCLonely/hexo-calendar)
5. 开启[Pjax](https://github.com/MoOx/pjax)以实现页内跳转，减少资源浪费，提高加载速度
6. ~~增加文章推送服务，页面停留两分钟提醒订阅。参考[简单浏览器更新推送的实现 | CC的部落格 (ccknbc.cc)](https://blog.ccknbc.cc/posts/implementation-of-simple-browser-update-push/)~~(没什么鸟用，于5月10关闭)
<!-- endtimeline -->

<!-- timeline 04-21 (Feature- - -) -->
沉静期结束
1. 关闭所有冗余特效
2. 删除页脚`footer`、侧栏标签、首页轮播图等没用的元素
3. 优化博文样式，细化背景（参考模仿了[Reimu's blog](https://blog.k8s.li/)）
4. 添加小节序号(二级标题往后)，参考：[markdown标题自动编号](https://duaduac.com/post/741d5d83.html)
<!-- endtimeline -->

<!-- timeline 5-2  -->
~~把Kindle的阅读笔记转移到[阅读写作子域：细墨](https://ink.si-on.top)(简直弄的一塌糊涂，于某月某日弃置之)~~
<!-- endtimeline -->

<!-- timeline 5-20  -->
* 完善了Waline评论的配置，现在有了**昵称和邮箱**、评论字数(5~1000)的限制，
* 添加了博客站点的Turnstile非交互式质询（正常访问是无感的）
<!-- endtimeline -->


<!-- timeline 6-2  -->
调整文章字体为`18px`，关闭了目录(大材小用)以提高阅读体验，对于复杂文章，采用思维导图的方式进行索引。
<!-- endtimeline -->

<!-- timeline 6-8  -->
阅读、摄影、音乐、写作统计整理：
- [x] 阅读采用[Notion](https://sionreading.notion.site/)统计+Excel分析的方法来统计(公开)
- [x] 摄影采用500px自带的[统计](https://500px.com.cn/page/statistics/index?userId=dff7efb584b12a50d685e413b053d3302)（私秘）
- [x] 音乐采用[ListenBrainz](https://listenbrainz.org/user/siontin/)来统计(公开)
- [x] 写作采用博客整理好的[室铭](/site)(即本页面)来统计(公开)
<!-- endtimeline -->

<!-- timeline 6-10  换名字 -->
- 更换id为秉耒，取自陶渊明诗
- 头像更改为第20号作品『Serif』

> 以思源宋体中英文字母『`I`』 为基础，外形为简化的一大一小两耒，寓意可顿锋芒，躬耕于专业field，分清主次，脚踏实地，不去追求浮华假象。
<!-- endtimeline -->

<!-- timeline 6-12  文言之权舆 -->
癸卯仲夏，四月廿五，于火车🚞上阅『百年文言』一书，有所触动，感慨新文化运动百年来文言文所蒙之屈辱，白话文之媚俗也，遂欲践行文言理念于部落阁，以期可通文言之思想，明君子之道义，雅白话之气韵。

部落阁文言化修改凡例：
1. 杂谈、随笔以论、说、原为范，前有序，后有跋。“阅读心得”作“某某拔萃”
2. 游记、摄影随笔以记(骈文)为范，当穷尽华美之词藻，亦兼蓄存真切之情感
3. “室铭”改“阁志”，并撰『子虚栈事略』于页首，以年分**行事志**于其后。
5. “关于”增添“阁主自传”部分
6. 除文学评论等文章，其他如科普、专业笔记等理工类文章仍保持白话文的形式以便于访问者理解。

<!-- endtimeline -->

<!-- timeline 6-21  绸缪牗户 -->
自归宛数日以来，移动网络下博客主站尝无可访达，每每示以『`ERR_CONNECTION_RESET`』，似有被墙之势，然僚站皆无此情形。暗自忖度：吾言也非激，绝非愤世嫉俗者之流，而文之不合乎礼义廉耻、社会主义者，未之有也。斯莫须有而墙之，实难解矣。

然无可奈何，近日或将求诸其他载体也。异日成之，将公示以告诸来访者。诸多不便，尽请谅解。

更新：
- ~~“关于”增加“成本与赞赏”~~(有个鸟用)
- 微信公众号信息与博客同步，异日**或**将同步更新(冇球用)

<!-- endtimeline -->

<!-- timeline 6-22 耒耨(lěi nòu) -->
开始耒耨期，为期半年。
> 耒者，犁也，耨者，锄也。耒耨期者，默然耕耘一方土地之光阴也。

前几日毕业归家，短暂休整。又离家租房，经三天实验式生活后，学习节奏逐渐找到。特在此宣布：自癸卯仲夏，重五端阳日起，秉耒正式开始备考2024二战(二次考研)初试，在此背景下，此阶段博客经营的规划如下：

- 工作日期间，有灵感时正常写作，媒介从数媒转为纸媒。
- 更新周期：由念更转为**周更**(每周五晚发布)
- 内容方向：考研数学计算技巧汇总、材料科学笔记总结、英文写作(大小作文各一篇)、其他(杂谈与随笔等)
- 形式：以数字化的文字为主，但保持纸质影像化的比例
<!-- endtimeline -->

<!-- timeline 7-1 ${\color{red}\text{☭ }}$建党节修改-->
1. 添加了巨多的BlobCat表情
2. ~~添加了即刻短文，感谢@Heo的[教程](https://blog.zhheo.com/p/557c9e72.html)~~(与主题css有冲突，暂时关闭)
3. ~~文章先方出来再慢慢完善~~(这不是一个好策略！)

<!-- endtimeline -->

<!-- timeline 7-7 小暑-->
1. npm组件与主题更新到(4.9.0)
2. 合并了魔改代码，`config.butterfly.yml`配置文件
<!-- endtimeline -->

<!-- timeline 7-11 ⛅-->
~~被墙~~(系Cloudflare CDN节点IP被污染所致，非真正被墙)
<!-- endtimeline -->

<!-- timeline 8-1 $\color{red}\overset{八}{一}$ 建军节修改-->
- ~~八月份二十篇写作规划~~(我怎么急功近利到这个程度了)
- 博客内容整体整理、删减
- 更改博客定位：转向更专业方向，减少杂感、生活类文章的写作
- ~~为期三天的全天制写作试验(8-1～8-3)~~(8月2日放弃)
- 添加了51.la统计
<!-- endtimeline -->

<!-- timeline 8-3 -->
* 添加了国内CDN节点: [香港-镜像站(cn.si-on.top)](https://cn.si-on.top)
* 评论系统的CDN节点：[Waline (commentcdn.si-on.top)](https://commentcdn.si-on.top/)
<!-- endtimeline -->

<!-- timeline 8-14 真·信球 -->
> 同样是考研，人家白天学了一天，晚上熬夜到十二点半背政治、刷题，而我晚上回去却把书包一扔，熬夜写博客。写得好还说得过去，关键他妈的一直写不好，快两周年了，博客还一直在向我不希望的方向——**哗众取宠、愚昧、浅薄**。这个状态大概已经持续好多天了，是时候结束了。

* 关闭了这狗日的垃圾博客
* 停止二十篇的无聊、无益且有害的八月写作计划
* 以后一个月写两篇文章！最多写四篇！
* 重新他妈的进入耒耨期！
<!-- endtimeline -->

<!-- timeline 9-1 第三年小计划·甲 -->
- 增加“不朽”做为写作的目的
- 减少博客对学习与生活的侵蚀
- 增强文章的“雅”气，摆脱俗气
<!-- endtimeline -->

<!-- timeline 9-6 乙·就简，自由地表达 -->
- 关闭所有访客统计
- ~~关闭rss~~(经过hiden插件的调整，成功将日志与rss分离，现在RSS又重启了，订阅可以获取最近30篇全文输出的文章。)
<!-- endtimeline -->

<!-- timeline 9-11 乙·就简 🦊 -->
- 更改博客代码仓库到Gitlab，并借由Deepl翻译完善了[读我(Readme)说明的英文版](https://gitlab.com/si-on/blog/)与[CHANGELOG](https://gitlab.com/si-on/blog/-/blob/main/CHANGELOG?ref_type=heads)。原来的github仓库文件不再更新，将其定格在 [clog.si-on.top](https://clog.si-on.top)
- 删除了178MB过往的图片
- 增加[友链](/friends)页面
<!-- endtimeline -->

<!-- timeline 9-14 丙·易名+寂空 -->
- 更改博客名称为『空在阁』取**空幻与实在兼收、唯心与唯物并蓄**之意
- 更改ID为与名字谐音的“心扬”(“秉蕳”之出处甚是不雅，弃之矣)
- 关闭博客直到12.25，期间关闭评论，绝不更新！
<!-- endtimeline -->

<!-- timeline 9-28～9-30 丁·复 -->
* 名字什么的都是没有意义，又把博客名、ID更改回去了，不再劳神搞这虚东西了
* 更新了九月的念头。
* ~~又开启批注功能~~
<!-- endtimeline -->
<!-- timeline 10-7 戊·嘟了个嘟 -->
添加[长毛象嘟文页面](/Minds/Toots)
<!-- endtimeline -->
<!-- timeline 10-17 己·🦊 -->
- [成功部署](https://gitlab.com/si-on/blog/-/jobs/5310535981)到了[Gitlab的Pages](https://gitlab.si-on.top)上
- 成功部署到了Cloudflare的Pages上
- 添加了Valine作为备用评论
- 更改全局字体为默认（即设备自带的黑体）
- 更改引用字体为汇文明朝体（古色古香的最喜欢了）

<!-- endtimeline -->

<!-- timeline 11-11 庚·🧩 -->
- 修改图片宽度为50%
<!-- endtimeline -->

<!-- timeline 12-17 辛·🌠 -->
- 隐蔽了大部分过于个人化的文章，修改"关于"
- 重启各种统计工具
<!-- endtimeline -->
{% endtimeline %}
{% endfolding %}

{% folding blue, 2024年博客大事记%}
{% timeline 🐉甲辰2024,blue %}
<!-- timeline 1-6 壬·live2d-->
* 保护隐私，去个人化
- ~~增加“猫咪hijiki”看板宠物~~
<!-- endtimeline -->
<!-- timeline 1-12 癸·🐀live2d-3.0 -->
* 参考[Live2dLoader](https://github.com/Weidows-projects/Live2dLoader)项目，增加了moc3模型的支持
* ~~更改桌面宠物为米奇~~(由于区域遮盖导致点击失效，于3-8去除)
<!-- endtimeline -->

<!-- timeline 3-15 甲·另一个方向 -->
~~受[aptx](https://atpx.com/)影响，关闭评论，去个人化~~

<!-- endtimeline -->

<!-- timeline 5-15 乙·融合进化 -->
* 参考MeuiCat的[教程](https://meuicat.com/blog/55/index.html)添加我的装备
* 恢复评论
<!-- endtimeline -->

<!-- timeline 6-21 丙·媚俗 -->
* 再次更改字体为霞鹜文楷([历程](https://mastodon.im/@sion/112654273218187785))
<!-- endtimeline -->

<!-- timeline 7-16 丁·Oh Friends -->
* 开启[友链](/friends/)页面
* ~~受未知[鬼站](https://bupzjx点com)影响，博客源码转为私有~~(2024.8.9已重新公开)
* 参考[Eurkon的文章](https://blog.eurkon.com/post/1213ef82.html)优化了分类、标签统计图表
<!-- endtimeline -->
<!-- timeline 9-1 戊 -->
The End：博客终结

* 2024/9/16 完成域名归档：[子虚栈_3.0](https://2024.blog.si-on.top)
<!-- endtimeline -->
<!-- timeline 9-16 中秋节🌝 己 -->
The Reboot：新起点

* 删除分类“🎭人的底层逻辑”
	* 删除《社会心理学阅读笔记》{% label 2023/02/20 blue %} {% label Note_of_social_psychology green %}：已整理到[阅读之旅](https://sionreading.notion.site/c0f70f71f80e4210be3ac64c09d939a5?pvs=74)中
	* 删除《骑墙宝鉴》{% label 2023/06/29 blue %} {% label textbook4neutralist green %}：对中庸的保留中立态度，念头自然死亡
	* 删除《何以不随波》{% label 2024/1/13 blue %} {% label Kill-waves green %}：阅读引起的无名念头，舍弃之
	* 删除《 “彼得原理”和层级权力(摘录)》{% label 2023/10/26 blue %} {% label The_Peter_Principle&&Hierarchy_Power green %}：读书笔记而已，无用无用！
	* 转移《海边的卡夫卡》、《大人的Small Notes》、《本质主义迷思》至分类“涵泳”
* 删除分类“诗”
* 新增"学习-🎓格致”分类：经济、生物、科普等理科性质的探索，侧重于论文分析，独立于“学习”分类中的任何部分
* “涵泳”分类定义明确：阅读、写作、哲学等文科性质的思考
* 其他文章分类的调整
* 更改“我的装备”页面标题为“My Precious”，添加头图
<!-- endtimeline -->

<!-- timeline 9-1 向外 庚 -->
* 页尾版权声明样式更改：参考[butterfly版权美化教程-小N同学](https://www.imcharon.com/117/)
* 增加爱发电赞助链接，二维码生成工具：[QR Code AI Art Generator - a Hugging Face Space by huggingface-projects](https://huggingface.co/spaces/huggingface-projects/QR-code-AI-art-generator)
<!-- endtimeline -->

<!-- timeline 10-5 辛 -->
* 主题更新至[5.0](https://github.com/jerryc127/hexo-theme-butterfly/releases/tag/5.0.0)
    增加首页小标题
    更改首页布局
* [涵泳](/read)页面添加观影部分
<!-- endtimeline -->

<!-- timeline 10-25 壬 -->
* 增加[田圃](/AwesomeHub/Plant/)页面
<!-- endtimeline -->

{% endtimeline %}
{% endfolding %}

{% folding purple open, 2025年博客大事记%}
{% timeline 🐍己巳2025,purple %}
<!-- timeline 1-23 癸 -->
* 更新版权协议
<!-- endtimeline -->
<!-- timeline 2-24 甲 ՞😃՞ -->
* 整理毛象数据，将重心转移到博客本身，减少废话更新频率。
* 进入旬更模式——每逢甲日更新
* 通过[hexo-feed](https://github.com/sergeyzwezdin/hexo-feed)实现标签与分类的单独rss链接，格式如下`https://blog.si-on.top/[分类名或标签名]/feed.xml`
* 更新写作流，通过ISSUE的形式
* ~~🎭~~关闭分析工具:51la、clarity、clustrmaps.
* 更改字体：汇文明朝$\to$宋体
<!-- endtimeline -->

<!-- timeline 3-1 乙 -->
* 更改字体
    * 标题字体更改为[寒蝉全圆体 ChillRoundF(字重400)](https://github.com/Warren2060/ChillRound)`"寒蝉全圆体"`
    * 正文字体更改为[寒蝉锦书宋 ChillJinshuSong(字重400)](https://github.com/Warren2060/ChillJinshuSong)`"寒蝉锦书宋Pro"`
    * 引用字体更改为[寒蝉正楷体 ChillKai(字重400)](https://github.com/Warren2060/Chillkai)
    * 使用[字体分发工具](https://chinese-font.netlify.app/zh-cn/online-split/)转化为webfont



<!-- endtimeline -->


{% endtimeline %}
{% endfolding %}
