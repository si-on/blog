---
title: 嘟文🐘
tags: 
- 长毛象
- 嘟文
- 表达
date: 2023-10-07 19:35:32
updated: 2024-1-13 06:35:32
top_img: "repeating-linear-gradient(45deg, rgba(208, 208, 208, 0.13) 0px, rgba(208, 208, 208, 0.13) 43px,rgba(195, 195, 195, 0.13) 43px, rgba(195, 195, 195, 0.13) 85px,rgba(41, 41, 41, 0.13) 85px, rgba(41, 41, 41, 0.13) 109px,rgba(88, 88, 88, 0.13) 109px, rgba(88, 88, 88, 0.13) 129px,rgba(24, 24, 24, 0.13) 129px, rgba(24, 24, 24, 0.13) 139px,rgba(92, 92, 92, 0.13) 139px, rgba(92, 92, 92, 0.13) 179px,rgba(37, 37, 37, 0.13) 179px, rgba(37, 37, 37, 0.13) 219px),repeating-linear-gradient(45deg, rgba(18, 18, 18, 0.13) 0px, rgba(18, 18, 18, 0.13) 13px,rgba(48, 48, 48, 0.13) 13px, rgba(48, 48, 48, 0.13) 61px,rgba(130, 130, 130, 0.13) 61px, rgba(130, 130, 130, 0.13) 84px,rgba(233, 233, 233, 0.13) 84px, rgba(233, 233, 233, 0.13) 109px,rgba(8, 8, 8, 0.13) 109px, rgba(8, 8, 8, 0.13) 143px,rgba(248, 248, 248, 0.13) 143px, rgba(248, 248, 248, 0.13) 173px,rgba(37, 37, 37, 0.13) 173px, rgba(37, 37, 37, 0.13) 188px),repeating-linear-gradient(45deg, rgba(3, 3, 3, 0.1) 0px, rgba(3, 3, 3, 0.1) 134px,rgba(82, 82, 82, 0.1) 134px, rgba(82, 82, 82, 0.1) 282px,rgba(220, 220, 220, 0.1) 282px, rgba(220, 220, 220, 0.1) 389px,rgba(173, 173, 173, 0.1) 389px, rgba(173, 173, 173, 0.1) 458px,rgba(109, 109, 109, 0.1) 458px, rgba(109, 109, 109, 0.1) 516px,rgba(240, 240, 240, 0.1) 516px, rgba(240, 240, 240, 0.1) 656px,rgba(205, 205, 205, 0.1) 656px, rgba(205, 205, 205, 0.1) 722px),linear-gradient(90deg, rgb(21, 145, 22),rgb(39, 248, 84))"
root: ../../
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: false
comments: 
katex: false
password: 
theme: xray
hidden: 
published: true
description: 
sticky: 
keywords:
---

> <center>表达本身就会影响你自己。</center>
> <p align="right">——Sion</p>

{% folding green, 页面编辑历史 %}
{% timeline 🐘 %}
<!-- timeline 2023.10.7 傍晚-->
参考@Carlos Life Book所写的《使用Mastodon timeline feed widget將Mastodon時間軸嵌入到Hexo》一文添加了此页面。
<!-- endtimeline -->

<!-- timeline 2023.12.12 晚上-->
同步仓库代码，更新到了[v3.12.0](https://gitlab.com/idotj/mastodon-embed-feed-timeline)
<!-- endtimeline -->

<!-- timeline 2024.1.13 早晨-->
增加了视频、音频支持，感谢[@idotj](https://gitlab.com/idotj/mastodon-embed-feed-timeline/-/issues/28)
<!-- endtimeline -->
{% endtimeline %}
{% endfolding %}

-----

<link rel="stylesheet" href="/css/mastodon-timeline.css">
  <div class="dummy-wrapper">
      <div class="mt-container">
        <div id="mt-body" class="mt-body" role="feed">
          <div class="loading-spinner"> </div>
        </div>
      </div>
    </div>
<script type="javascript" src='/js/mastodon-timeline.js'>  </script>