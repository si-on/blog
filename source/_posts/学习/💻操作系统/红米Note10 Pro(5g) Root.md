---
title: 红米Note10 Pro(5g) Root
abbrlink: Root4redmi-note10-pro-5g_chopin
categories:
  - 学习
  - "\U0001F4BB操作系统"
root: ../../
katex: false
theme: xray
published: false
date: 2024-07-17 20:30:37
updated: 2024-07-17 20:30:37
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---

## 刷机·序
上上个月有[网友](https://anya1014.cn/)在XDA论坛发了个红米note10pro(5g)(代号:chopin)的TWRP开发公告[^2]，瞬间让我精神为之一振，自由刷第三方ROM的时间节点终于到了！然而在后续摸索过程中[发现](https://anya1014.cn/archives/221)开发者提供的TWRP 3.7.0 只适用于安卓11，也就是MIUI12版的，然而我想要安卓13的ROOT，这就意味着TWRP还是走不通。接着又在开发者网站上找到了适配此机KernelSu的[内核](https://anya1014.cn/archives/379)，KernelSU也是Z君之前提过的另一种ROOT方法，当时此机还不支持，而照目前这个情况来看，可以赌一把试试了。

翻车了，在我解锁清空数据后，按照KernelSU的教程进行操作时，发现刷入内核必须要ROOT权限，这也意味着要先ROOT，然而又找不到MIUI14的TWRP，见了鬼了


这款发布于2021年的手机，从一开始的MIUI12更新到了2023年的MIUI14，但今年起已经正式被官方抛弃——更新名单里的红米机型只有note11往后——确认不会更新澎湃OS了[^1]。

现在的电子设备就这样——长江后浪推前浪，前浪拍死在沙滩上，作为一个~~贫穷~~从不喜新厌旧的现代人，很令人苦恼啊。我手里的这台现在一直维持在MIUI14.0.8，系统整体用起来还好，最烦人的不过是今年开始的安装应用时会有未备案提示、安装应用多了要输密码。上次就是因为这个原因刷机（如下）。后来各种问题，在四月份又刷回来了。
{% folding green, 刷机历程 %}
{% timeline 某次安装提示之后, %}
<!-- timeline 2023-12-22 -->
* 刷自带Migisk的国际版MIUI14.0.3
<iframe
  src="https://quote.si-on.top/embed?url=https://mastodon.im/@sion/111454614810600727"
  width=100%  
  height=400
  interactions=  normal
  theme= bird-ui
  allowfullscreen="allowfullscreen" 
  sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox allow-forms"
></iframe>


<!-- endtimeline -->
<!-- timeline 2024-04-21-->
* 刷回国内版MIUI14.0.5，刷机[历程](https://mastodon.im/@sion/112308421133375423)

> 设备是红米note10 pro 5g ，之前为了root刷了国际版14，xda的作者也不维护，最近几周问题颇多。   
> ROM不好找(在此日那些收费的搬运网站祖奶奶一百次)，刷机工具用miflash(线刷)。   
> 一开始刷错了刷成了国际版rom，用3500s刷成砖头，进不了fastboot，发现是刷机时miflash默认刷机完成后自动上锁。用mi unlock撬锁，然后出现了戴雷锋帽米兔fastboot，接着继续刷国内版的rom成功，miui14稳定版镜像，花费1500秒刷完。   
> 然后用adb卸载预装软件，恢复备份，忙活到一点多才睡觉。
> 两点多，一直做清醒梦，或者说是梦魇。挣扎着醒过来后发现，左边kindle，右边小米，黑莓都开着机。辐射太大了。把设备一把抓过，扔到床那头，终于入睡。   

<!-- endtimeline -->
<!-- timeline 2024-04-21-->
* 刷回TWRP+国内版

<!-- endtimeline -->
{% endtimeline %}

{% endfolding %}

今天晚上，什么也不想干，就折腾吧，折腾虽然无用，但自是有一番乐趣，就像搭积木/拆炸弹/信手涂鸦/画工图.....


## 备份
得益于MIUI系统丝滑的备份功能——可以保留用户数据——真正意义上的备份，大概十分钟就能把需要的东西备份好，然后传到电脑上，刷机后再传到手机上进行恢复。

对于我而言备份的关键是
1. 照片视频：包含截图、平常的摄影、航拍[^3]
2. 录音：一些念头、口哨与神经质般的叨叨
3. 偏门应用（需要备份）
	1. Aegis：简洁的2fa客户端，存有Gitlab、Github、非共格晶系的令牌，丢了的话很费事
	2. Kindle：Kindle安卓端，下载麻烦
	3. Lab+：Gitlab安卓端
	4. Lightroom：费老大劲找来的mod版
	5. 千米神眼：母亲让下的监控app，贼多广告，恢复起来麻烦
	6. Snapseed：找安装包麻烦
	7. Termux：因为SSH与gitlab连接好了、Shell的配置也很舒服，恢复起来很麻烦
	8. Walli 4k：插画/壁纸应用，找资源麻烦

另外为了以防万一，跨MIUI版本的备份不能恢复了，还需要额外对一些关键软件备份下
1. Telegram：电脑上登陆下，防止没有设备在线，登录获取验证码贼慢
2. 微信：电脑上登陆下，防止**又自己把自己账号给冻结**了

## 刷机

### 解锁
![](../../../images/20240609/红米Note10%20Pro(5g)%20Root-20240717224751232.webp)






[^1]: [小米澎湃OS 发版进度公告|2023.10.19](https://web.vip.miui.com/page/info/mio/mio/detail?postId=42105346&ref=miui_banner)
[^2]: [Development - [RECOVERY] [11] [chopin] TeamWin Recovery Project For Poco X3 Gt/Redmi Note 10 Pro 5G | XDA Forums](https://xdaforums.com/t/recovery-11-chopin-teamwin-recovery-project-for-poco-x3-gt-redmi-note-10-pro-5g.4669665/)，
[^3]: 但其实没了也没啥大不了，因为最近想改变一下摄影风格，以前的照片没了也无所谓。