---
title: linux 常用命令记录
tags:
  - 奇技淫巧
  - LaTeX
categories:
  - 学习
  - "\U0001F4BB操作系统"
cover: /images/top-5-linux-bashs-and-how-to-install-them.jpg
description: 近些年来学习（玩）LInux过程中遇到的一些问题与备忘
abbrlink: linuxnote
date: 2021-08-02 18:31:36
updated: 2023-10-02 18:31:36
keywords:
copyright:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
mathjax:
highlight_shrink:
---

> 解决的不仅仅是一个问题，而是整个世界！

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
<!-- timeline 2021-08-02-->
 初次整理，并不时地添加东西
<!-- endtimeline -->
<!-- timeline 2024-02-4-->
- 整理了文章架构
* 增加了docker
* 增加了nginx
<!-- endtimeline -->
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}
- [Snap](#snap)
- [Pacman](#pacman)
- [Node.js](#nodejs)
- [SSH使用](#ssh%E4%BD%BF%E7%94%A8)
	- [ssh参数说明](#ssh%E5%8F%82%E6%95%B0%E8%AF%B4%E6%98%8E)
	- [ssh-keygen 参数说明](#ssh-keygen-%E5%8F%82%E6%95%B0%E8%AF%B4%E6%98%8E)
- [Git](#git)
	- [使用](#%E4%BD%BF%E7%94%A8)
- [Docker](#docker)
	- [Docker命令](#docker%E5%91%BD%E4%BB%A4)
		- [常用命令](#%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4)
			- [根据配置文件进行编译](#%E6%A0%B9%E6%8D%AE%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6%E8%BF%9B%E8%A1%8C%E7%BC%96%E8%AF%91)
			- [查看日志](#%E6%9F%A5%E7%9C%8B%E6%97%A5%E5%BF%97)
			- [镜像与容器管理](#%E9%95%9C%E5%83%8F%E4%B8%8E%E5%AE%B9%E5%99%A8%E7%AE%A1%E7%90%86)
		- [命令所有](#%E5%91%BD%E4%BB%A4%E6%89%80%E6%9C%89)
	- [示范](#%E7%A4%BA%E8%8C%83)
		- [Memos](#memos)
		- [Firefish](#firefish)
		- [Gotosocial](#gotosocial)
- [Nginx](#nginx)
- [Podman](#podman)
		- [换源](#%E6%8D%A2%E6%BA%90)
- [杂项](#%E6%9D%82%E9%A1%B9)
	- [储存管理](#%E5%82%A8%E5%AD%98%E7%AE%A1%E7%90%86)
		- [查看磁盘空间](#%E6%9F%A5%E7%9C%8B%E7%A3%81%E7%9B%98%E7%A9%BA%E9%97%B4)
		- [debian/ubuntu/raspberry 扩充root空间](#debianubunturaspberry-%E6%89%A9%E5%85%85root%E7%A9%BA%E9%97%B4)
		- [强制清空回收站](#%E5%BC%BA%E5%88%B6%E6%B8%85%E7%A9%BA%E5%9B%9E%E6%94%B6%E7%AB%99)
		- [debian系列查看软件占用空间](#debian%E7%B3%BB%E5%88%97%E6%9F%A5%E7%9C%8B%E8%BD%AF%E4%BB%B6%E5%8D%A0%E7%94%A8%E7%A9%BA%E9%97%B4)
	- [非常用命令](#%E9%9D%9E%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4)
		- [查看字体](#%E6%9F%A5%E7%9C%8B%E5%AD%97%E4%BD%93)
		- [更新grub](#%E6%9B%B4%E6%96%B0grub)
		- [latex指定编译命令](#latex%E6%8C%87%E5%AE%9A%E7%BC%96%E8%AF%91%E5%91%BD%E4%BB%A4)
	- [非常见问题](#%E9%9D%9E%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98)
		- [Fcitx5输入法在部分应用内无法激活](#fcitx5%E8%BE%93%E5%85%A5%E6%B3%95%E5%9C%A8%E9%83%A8%E5%88%86%E5%BA%94%E7%94%A8%E5%86%85%E6%97%A0%E6%B3%95%E6%BF%80%E6%B4%BB)
		- [双系统时间不一致问题解决](#%E5%8F%8C%E7%B3%BB%E7%BB%9F%E6%97%B6%E9%97%B4%E4%B8%8D%E4%B8%80%E8%87%B4%E9%97%AE%E9%A2%98%E8%A7%A3%E5%86%B3)
		- [LaTex报错：Reference ‘xxx‘ on page ‘xxx‘ undefined on input line ‘xxx‘](#latex%E6%8A%A5%E9%94%99reference-xxx-on-page-xxx-undefined-on-input-line-xxx)

{% endmarkmap %}

## Snap

  ```
   snap list                   列出所有已安装的snap软件包
   snap find <keyword>         按照keyword来寻找可以安装的snap软件包
   snap install <software>           安装软件
   snap refresh <software>           更新软件
   snap refresh all                  更新所有的软件
   snap remove <software>            卸载软件
   snap revert <software>            将软件还原到之前的版本
  ```

> 错误：
>
> ```bash
> error: cannot list snaps: cannot communicate with server: Get "http://localhost/v2/snaps": dial unix /run/snapd.socket: connect: no such file or directory
> ```
>
> 原因：未启动snapd服务
>
> 解决方法：
>
> ```bash
> sudo systemctl start snapd
> ```

## Pacman

- pacman -S 包名：安装多个包，需以空格分隔包名。  
- pacman -Sy 包名：与上面命令不同的是，该命令将在同步包数据库后再执行安装。
- pacman -Sv 包名：在显示一些操作信息后执行安装。
- pacman -U：安装本地包，其扩展名为 pkg.tar.gz。
- `pacman -U: http://www.example.com/repo/example.pkg.tar.xz 安装一个远程包（不在 pacman 配置的源里面）`   

---------

-  pacman -R 包名：该命令将只删除包，保留其全部已经安装的依赖关系
-  `pacman -Rs 包名：在删除包的同时，删除其所有没有被其他已安装软件包使用的依赖关系`
-  pacman -Rsc 包名：在删除包的同时，删除所有依赖这个软件包的程序
-  pacman -Rd 包名：在删除包时不检查依赖。

------

-  pacman -Ss 关键字：在仓库中搜索含关键字的包。
-  pacman -Qs 关键字： 搜索已安装的包。
-  pacman -Qi 包名：查看有关包的详尽信息。
-  pacman -Ql 包名：列出该包的文件。

------

-  pacman -Sw 包名：只下载包，不安装。
-  pacman -Scc：清理所有的缓存文件
-  ~~pacman -Sc~~：清理未安装的包文件，包文件位于 /var/cache/pacman/pkg/ 目录。

> 上面为不推荐命令，会删除所有旧版本包，正确的清理方式如下[^1]  
> 查看占用空间：
>
> ```bash
> du -sh /var/cache/pacman/pkg/ 
> ```
>
> 清理占用：
>
> ```bash
> sudo paccache -r -k 1
> ```

## Node.js

```
node -v 查看版本

npm -v

npm install -g npm 更新npm

npm cache clean -f 清除npm缓存

npm install -g n 安装n模块

n stable 升级node.js到最新稳定版
```

* nvm可以用来管理npm的版本
* pnpm是比较快速的npm替代，然而某些时候仍然需要先用npm来初始化，之后再用pnpm进行管理
## SSH使用
简述
 : ssh是与服务器进行加密通信的工具
 : 实现原理是<u>公私钥非对称加密</u>。即客户端通过已经在服务器认证的公钥的对应私钥与之进行连接
 : 使用的端口一般是**80**。

### ssh参数说明

* -c：选择所加密的密码型式 （blowfish|3des 预设是3des）
* -F：指定ssh指令的配置文件
* **-i：指定身份文件（预设是在使用者的家目录 中的 .ssh/identity）**
* -l：指定连接远程服务器登录用户名
* **-p：指定远程服务器上的端口（默认22）**
* -q：静默模式
* -t：强制配置 pseudo-tty
* -v：打印更详细信息
* -L listen-port:host:port 指派本地的 port 到达端机器地址上的 port

### ssh-keygen 参数说明

* -B	显示指定的公钥/私钥文件的摘要。
* **-b    指定密钥长度。对于RSA密钥，最小要求768位，默认是2048位。**DSA密钥必须恰好是1024位(FIPS 186-2 标准的要求)。
* **-C	提供一个新注释**
* -c    要求修改私钥和公钥文件中的注释。
* **-f	指定密钥文件名。**
* -l	显示公钥文件的指纹数据。它也支持 RSA1 的私钥。对于RSA和DSA密钥，将会寻找对应的公钥文件，然后显示其指纹数据。
* -N	提供一个新的密语。
* -p	要求改变某私钥文件的密语而不重建私钥。程序将提示输入私钥文件名、原来的密语、以及两次输入新密语。
* -t	指定要创建的密钥类型。可以使用："rsa1"(SSH-1) "rsa"(SSH-2) "dsa"(SSH-2)
* -y	读取OpenSSH专有格式的公钥文件，并将OpenSSH公钥显示在 stdout 上。

下面举一个例子：生成4200位的RSA密钥。

```bash 例子：
ssh-keygen -t rsa -b 4200 -C "redmi10@termux" -f redmi10
```

> ssh连接成功但用ssh，`gitclone`仍然失败的解决办法:在`.ssh/`下创建`config`，内容如下：
>
> ```
> Host github.com
> HostName github.com
> User aornus(用户名)
> IdentityFile ~/.ssh/私匙名
> IdentitiesOnly yes
> 
> Host gitee.com
> HostName gitee.com
> User aornus(用户名)
> IdentityFile ~/.ssh/私匙名
> IdentitiesOnly yes
> ```

添加公钥：在`.ssh/authorized_keys`文件中将公钥添加进去即可。

ssh 使用指定密钥连接
```
ssh -i .ssh/pulic root@host
```
## Git
简述
: git是主流的代码仓库管理工具
: 很方便地进行版本控制与~~差异处理~~
: 开源实现主要是Gitlab

### 使用

- git init - 初始化仓库。
- git add . - 添加文件到暂存区。
- git commit - 将暂存区内容添加到仓库中。

> ![](https://www.runoob.com/wp-content/uploads/2015/02/011500266295799.jpg)



## Docker

>此部分笔记所操作时的环境时Arch系的Garuda linux：

Docker可以快速部署项目，省略复杂的数据库配置，只需要一条命令即可。

|   项目   |          内容          |
|:--------:|:----------------------:|
| 操作系统 |      Garuda Linux      |
| 系统类型 |          64位          |
| 内核版本 | Linux 6.3.1-zen1-1-zen |
|   时间   |       2023.5.12        |

linux上安装软件，一般不需要查教程，直接安装即可
```bash
sudo pacman -S docker
# 在写作本文时安装的docker版本为1:23.0.6-1
```

```bash 运行结果
软件包 (3) containerd-1.7.0-1  runc-1.1.7-1  docker-1:23.0.6-1

下载大小：       55.00 MiB
全部安装大小：  233.40 MiB
```

```bash 开启docker
sudo systemctl start docker
```
### Docker命令
#### 常用命令
##### 根据配置文件进行编译
```bash
docker-compose up -d
```

##### 查看日志
```bash
docker logs -f [容器名称或者运行id] 
```
##### 镜像与容器管理
```bash 查看容器
docker ps -a
# -a 查看所有运行的容器
```

```bash 查看容器
docker images
```

```bash 删除容器：先停止，后删除
docker stop [容器名称或者运行id] 
docker rm [容器名称或者运行id]
```
```bash 删除镜像
docker images 
docker rmi [镜像名称或者id]
```
#### 命令所有
:  常用命令
	run 从镜像文件中创建并运行一个新容器
	exec 在运行中的容器中执行命令
	ps 列出容器
	build 从 Dockerfile 中构建映像
	pull 从仓库中下载镜像
	push 将镜像上传到注册表
	images 列出镜像
	login 登录到镜像仓库
	logout 退出镜像仓库
	search 搜索镜像仓库中的镜像
	version 显示 Docker 版本信息
	info 显示全系统信息

:  管理命令
	builder 管理构建
	checkpoint 管理检查点
	container 管理容器
	context 管理上下文
	images 显示镜像
	manifest 管理 Docker 映像清单和清单列表
	network 管理网络
	plugin 管理插件
	
: Swarm 命令
	config 管理 Swarm 配置
	node 管理 Swarm 节点
	service 管理 Swarm 服务
	stack 管理 Swarm 堆栈
	swarm 管理 Swarm

: 命令
	attach 为运行中的容器附加本地标准输入、输出和错误流
	commit 根据容器的更改创建新映像
	**cp 在容器和本地文件系统之间复制文件/文件夹**
	create 创建一个新容器
	diff 检查容器文件系统中文件或目录的更改
	events 从服务器获取实时事件
	**导出 将容器的文件系统导出为 tar 压缩文件**
	历史记录 显示镜像的历史记录
	**导入 从 tar 包中导入内容以创建文件系统映像**
	inspect 返回 Docker 对象的底层信息
	kill 关闭一个或多个正在运行的容器
	加载 从 tar 文件或 STDIN 加载镜像
	日志 抓取容器的日志
	pause 暂停一个或多个容器内的所有进程
	**port 列出容器的端口映射或特定映射**
	rename 重命名容器
	**restart 重启一个或多个容器**
	rm 删除一个或多个容器
	rmi 删除一个或多个镜像
	save 将一个或多个镜像保存到 tar 存档（默认情况下流到 STDOUT）
	start 启动一个或多个已停止的容器
	stats 显示实时容器资源使用统计流
	stop 停止一个或多个正在运行的容器
	tag 创建指向 SOURCE_IMAGE 的标签 TARGET_IMAGE
	top 显示容器的运行进程
	取消暂停 取消暂停一个或多个容器内的所有进程
	更新 更新一个或多个容器的配置
	等待 阻塞，直到一个或多个容器停止，然后打印它们的退出代码

: 全局选项
	--config 字符串 客户端配置文件的位置（默认为"/home/sion/.docker"）。
	-c，--context 字符串 用来连接守护进程的上下文名称（覆盖 DOCKER_HOST 环境变量和使用 "docker context use "设置的默认上下文）。
	-D, --debug 启用调试模式
	-H, --host 列出要连接的守护进程套接字
	-l, --log-level string 设置日志记录级别（"debug"、"info"、"warn"、"error"、"fatalal"）（默认为 "info"）
	--tls 使用 TLS；由 --tlsverify 隐含
	--tlscacert字符串 仅信任由该 CA 签发的证书（默认为"/home/sion/.docker/ca.pem）
	--tlscert 字符串 TLS 证书文件的路径（默认为"/home/sion/.docker/cert.pem）
	--tlskey 字符串 TLS 密钥文件的路径（默认为"/home/sion/.docker/key.pem）
	--tlsverify 使用 TLS 并验证远程
	-v, --version 打印版本信息并退出


### 示范
#### Memos
```bash 
docker run -d  --init  --name memos  --publish 5230:5230   --volume ~/.memos/:/var/opt/memos  neosmemo/memos:stable
```

![](../../../images/20240104/note-for-linux-20240204204245136.webp)
#### Firefish
* 一个可以导入嘟文的长毛象系统，基于日本的misskey开发，非常美观。
* 2024年初，docker部署大概需要占用2G的磁盘

```bash 写好的脚本
git clone https://gitlab.com/siontop/firefish.git && apt update && apt install docker-compose && cd firefish/ && docker-compose up -d && docker logs -f firefish_web
```
#### Gotosocial
* 一个最小的长毛象实例
* 2024年初，大概需要200MB空间，60M内存
> Docker部署，可以再配置文件中环境变量部分添加<u>非docker部署的配置选项</u>


```bash 安装脚本
git clone https://gitlab.com/siontian/social.git && apt update && apt install docker-compose &&  cd ~/social && docker-compose up -d
```

```bash 新建用户me
docker exec -it gotosocial /gotosocial/gotosocial admin account create --username me --email me@si-on.top --password 'Dragon_2024@^_^'
```

```将用户me提升为管理员
docker exec -it gotosocial /gotosocial/gotosocial admin account promote --username me
```

## Nginx
部署好了项目，自然就需要将其绑定到域名上，这时候就需要nginx来帮忙了。

我们知道一个ip可以有65535个端口，每个项目都可以随便选一个部署上去，然而在网页中访问的话只能是80(http)与443(https)两个端口；   
我们也知道一个域名可以有无穷多个子域，一个子域名又可以有无穷多个子子域，一个子域名又有无穷多子子子域...简言之就是无穷多个开放了80/443端口的域名；

Nginx的一个主要功能就是把本地的非80/443端口的项目代理到一个子域名中，真是一个非常神奇的东西。用官方的话说就是：

> [Nginx是一个HTTP服务器，可以将服务器上的静态文件（如HTML、图片）通过HTTP协议展现给客户端](https://zhuanlan.zhihu.com/p/54793789)

## Podman
> 由于podman问题巨多，已经放弃！此部分暂停更新（2024-2-4）
 
鉴于podman占用内存更小，更适合咱家那小小的VPS,所以转向podman：


```bash remove docker
╰─λ sudo pacman -Rsc docker  
正在检查依赖关系...  
  
软件包 (3) containerd-1.7.13-1  runc-1.1.12-1  docker-1:24.0.7-1  
  
全部移去体积：  233.37 MiB  
  
:: 打算删除这些软件包吗？ [Y/n]
```

```bash install podman 
sudo pacman -S podman 

正在解析依赖关系...
正在查找软件包冲突...

软件包 (11) catatonit-0.2.0-3  conmon-1:2.1.10-1  containers-common-1:0.57.2-2  criu-3.18-2  crun-1.14-1  libslirp-4.7.0-1  netavark-1.10.2-1  python-protobuf-25.2-1
slirp4netns-1.2.2-1  yajl-2.1.0-5  podman-4.9.2-1

下载大小：       23.39 MiB
全部安装大小：  103.14 MiB
```

#### 换源
[podman 配置国内镜像\_podman 国内镜像仓库-CSDN博客](https://blog.csdn.net/leave00608/article/details/114156354)

```yaml sudo vim /etc/containers/registries.conf
# 例：使用 podman pull registry.access.redhat.com/ubi8-minimal 时，
# 仅仅会从registry.access.redhat.com去获取镜像。
# 如果直接使用 podman pull ubuntu 时，没有明确指明仓库的时候，使用以下配置的仓库顺序去获取
unqualified-search-registries = ["docker.io", "registry.access.redhat.com"]
 
# 配置仓库的地址，可以直接在location里配置国内镜像例如：docker.mirrors.ustc.edu.cn
# 直接在location里配置的时候，可以不需要后面的 [[registry.mirror]] 内容，
# 但是这样只能配置一个镜像地址，这个镜像挂了就没法尝试其它镜像。
# prefix的值与unqualified-search-registries里配置的得一样，但是可以支持通配符。
# prefix不写的情况下，默认与location的指一样。
[[registry]]
prefix = "docker.io"
location = "docker.io"
 
# 在这里可以配置多个镜像地址，前提是至少有一个[[registry]]配置。
# 需要注意的是，无论 unqualified-search-registries 选择了哪个仓库，
# 都会先从这里的上下顺序开始去拉取镜像，最后才会去匹配上[[registry]]里prefix对应的location位置拉取镜像。
# 所以这里需要注意，上面配置的不同仓库类型，这里配置的镜像并不能是通用的，所以 unqualified-search-registries 配置了多个仓库的时候，就最好直接使用[[registry]] 的 location 指定镜像地址，不要配置 [[registry.mirror]] 了。
# redhat 的国内镜像暂未发现。
[[registry.mirror]]
location = "docker.nju.edu.cn"
[[registry.mirror]]
location = "docker.mirrors.sjtug.sjtu.edu.cn"
 
# 当使用 podman pod create 命令时候，因需要从k8s.gcr.io拉取 pause 镜像，但是该站点在国内被墙了。
# 所以给该站点搞个镜像。以下镜像是阿里云第三方用户，非官方。
# 或者 registry.aliyuncs.com/googlecontainersmirror ，也是第三方用户。
# 目前没找到国内官方的镜像。gcr.mirrors.ustc.edu.cn 返回403不能用了
[[registry]]
prefix = "k8s.gcr.io"
location = "registry.aliyuncs.com/google_containers"
```


## 杂项
### 储存管理
#### 查看磁盘空间
```
df -h
```
#### debian/ubuntu/raspberry 扩充root空间
1. 用cfdisk扩充/dev/mmcblk0空间
2. 用`resize2fs /dev/mmcblk0p2`修复分区

#### 强制清空回收站

```bash
sudo rm -rf ~/.local/share/Trash/*
```
#### debian系列查看软件占用空间

```
dpkg-query --show --showformat='${Installed-Size}\t${Package}  
' | sort -rh | head -25 | awk '{print $1/1024, $2}'
```


### 非常用命令
#### 查看字体
```bash
fc-list
```

#### 更新grub

```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

#### latex指定编译命令

```bash
% !TeX program = xelatex
```

> 使用htlatex可以把文档编译成html格式

### 非常见问题
#### Fcitx5输入法在部分应用内无法激活
```bash 
# sudo vim /etc/profile

export GTK_IM_MODULE=fcitx5 
export QT_IM_MODULE=fcitx5 
export XMODIFIERS="@im=fcitx5"
```

#### 双系统时间不一致问题解决
```bash 在Linux端输入
timedatectl set-local-rtc 1 
```

#### LaTex报错：Reference ‘xxx‘ on page ‘xxx‘ undefined on input line ‘xxx‘

解决方法[^2]：
```latex \label 必须放在 \caption 之后写，否则会报如题错误。
\caption{table xxx}
\label{reference xxx}
```


[^1]:[The Recommended Way To Clean The Package Cache In Arch Linux](https://ostechnix.com/recommended-way-clean-package-cache-arch-linux/) 
[^2]: [LaTex报错：Reference ‘xxx‘ on page ‘xxx‘ undefined on input line ‘xxx‘_Su_Yuan97的博客-CSDN博客](https://blog.csdn.net/NEU_Yuan/article/details/129539364)

