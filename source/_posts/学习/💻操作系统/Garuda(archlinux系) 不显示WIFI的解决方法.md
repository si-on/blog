---
title: Garuda(archlinux系) 不显示WIFI的解决方法
abbrlink: take-back-my-wifi
categories:
  - 学习
  - 💻操作系统
root: ../../
katex: false
theme: xray
published: true
date: 2023-10-01 21:14:05
updated: 2023-10-01 21:14:05
tags:
  - 试验
  - 网络
cover: false
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
password: 
hidden: 
description: 修复wifi连接，修复kde桌面wifi不显示的问题
sticky: 
keywords:
---

## 故障
我的电脑有个毛病：每次安装完Linux后，开机进入Lnux时，都有机率出现WIFI图标消失，只能依靠有线网络或者手机的USB共享网络连接来接入网络，十分烦人。

之前还是偶尔发生，这次已经将近一个月没有自动出现WIFI图标了，今天试着修复下。

![事故现场 由秉蕑截图](../../../images/20230709/no-wifi.webp)


## 排查
### 检查驱动
```shell 检查驱动
lspci -k
```

```shell 输出结果
00:00.0 Host bridge: Intel Corporation Xeon E3-1200 v6/7th Gen Core Processor Host Bridge/DRAM Registers (rev 08)  
Subsystem: Dell Xeon E3-1200 v6/7th Gen Core Processor Host Bridge/DRAM Registers  
Kernel driver in use: skl_uncore  
# 集成主板/寄存器型号：第七代因特尔至强E3 1200 v6（修订版 08）

00:02.0 VGA compatible controller: Intel Corporation UHD Graphics 620 (rev 07)  
DeviceName:  Onboard IGD  
Subsystem: Dell UHD Graphics 620  
Kernel driver in use: i915  
Kernel modules: i915  
# VGA兼容控制器/显卡：(巨垃圾的内置显卡)因特尔UHD 620（修订版 07）

00:04.0 Signal processing controller: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Thermal Subsystem (rev 08)  
Subsystem: Dell Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Thermal Subsystem  
Kernel modules: processor_thermal_device_pci_legacy  
# 信号处理控制器：英特尔至强 E3-1200 v5/E3-1500 v5/6 代酷睿处理器散热子系统（修订版 08）

00:14.0 USB controller: Intel Corporation Sunrise Point-LP USB 3.0 xHCI Controller (rev 21)  
Subsystem: Dell Sunrise Point-LP USB 3.0 xHCI Controller  
Kernel driver in use: xhci_hcd  
Kernel modules: xhci_pci  
# USB 控制器：英特尔公司 Sunrise Point-LP USB 3.0 xHCI 控制器（修订版 21）  


00:14.2 Signal processing controller: Intel Corporation Sunrise Point-LP Thermal subsystem (rev 21)  
Subsystem: Dell Sunrise Point-LP Thermal subsystem  
Kernel driver in use: intel_pch_thermal  
Kernel modules: intel_pch_thermal  
# 信号处理控制器：英特尔公司 Sunrise Point-LP 热子系统（修订版 21）  

00:15.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #0 (rev 21)  
Subsystem: Dell Sunrise Point-LP Serial IO I2C Controller  
Kernel driver in use: intel-lpss  
Kernel modules: intel_lpss_pci  
#  信号处理控制器：英特尔公司 Sunrise Point-LP 串行 IO I2C 控制器 #0 (rev 21)  

00:16.0 Communication controller: Intel Corporation Sunrise Point-LP CSME HECI #1 (rev 21)  
Subsystem: Dell Sunrise Point-LP CSME HECI  
Kernel modules: mei_me  
# 通信控制器：英特尔公司 Sunrise Point-LP CSME HECI 1号（修订版 21） 


00:17.0 SATA controller: Intel Corporation Sunrise Point-LP SATA Controller [AHCI mode] (rev 21)  
Subsystem: Dell Sunrise Point-LP SATA Controller [AHCI mode]  
Kernel driver in use: ahci  
# SATA 控制器：英特尔公司 Sunrise Point-LP SATA 控制器 [AHCI 模式] (rev 21)  

00:1c.0 PCI bridge: Intel Corporation Sunrise Point-LP PCI Express Root Port #1 (rev f1)  
Subsystem: Dell Sunrise Point-LP PCI Express Root Port  
Kernel driver in use: pcieport  
00:1c.4 PCI bridge: Intel Corporation Sunrise Point-LP PCI Express Root Port #5 (rev f1)  
Subsystem: Dell Sunrise Point-LP PCI Express Root Port  
Kernel driver in use: pcieport  
00:1c.5 PCI bridge: Intel Corporation Sunrise Point-LP PCI Express Root Port #6 (rev f1)  
Subsystem: Dell Sunrise Point-LP PCI Express Root Port  
Kernel driver in use: pcieport  
# PCI 桥接器： 英特尔公司 Sunrise Point-LP PCI Express 1、5、6号根端口（修订版 f1）  


00:1f.0 ISA bridge: Intel Corporation Sunrise Point LPC/eSPI Controller (rev 21)  
Subsystem: Dell Sunrise Point LPC Controller/eSPI Controller  
# ISA 桥(总线类型)： 英特尔公司 Sunrise Point LPC/eSPI 控制器（修订版 21）  

00:1f.2 Memory controller: Intel Corporation Sunrise Point-LP PMC (rev 21)  
Subsystem: Dell Sunrise Point-LP PMC  
# 内存控制器：英特尔公司 Sunrise Point-LP PMC（修订版 21）   

00:1f.3 Audio device: Intel Corporation Sunrise Point-LP HD Audio (rev 21)  
Subsystem: Dell Sunrise Point-LP HD Audio  
Kernel driver in use: snd_hda_intel  
Kernel modules: snd_hda_intel, snd_soc_skl, snd_soc_avs  
# 音频设备： 英特尔公司 Sunrise Point-LP HD 音频（修订版 21）  


00:1f.4 SMBus: Intel Corporation Sunrise Point-LP SMBus (rev 21)  
Subsystem: Dell Sunrise Point-LP SMBus  
Kernel driver in use: i801_smbus  
Kernel modules: i2c_i801  
# 系统管理总线：英特尔公司 Sunrise Point-LP SMBus（修订版 21）  

01:00.0 Display controller: Advanced Micro Devices, Inc. [AMD/ATI] Topaz XT [Radeon R7 M260/M265 / M340/M360 / M440/M445 / 530/535 / 620/625 Mobile] (rev c1)  
Subsystem: Dell Radeon 530  
Kernel driver in use: amdgpu  
Kernel modules: amdgpu  
# 显示控制器： Advanced Micro Devices, Inc. [AMD/ATI] Topaz XT [Radeon R7 M260/M265 / M340/M360 / M440/M445 / 530/535 / 620/625 Mobile] (rev c1)  

02:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL810xE PCI Express Fast Ethernet controller (rev 07)  
Subsystem: Dell RTL810xE PCI Express Fast Ethernet controller  
Kernel driver in use: r8169  
Kernel modules: r8169  
# 以太网控制器： Realtek Semiconductor Co. RTL810xE PCI Express 快速以太网控制器（修订版 07）  

03:00.0 Network controller: Qualcomm Atheros QCA9377 802.11ac Wireless Network Adapter (rev 31)  
Subsystem: Dell QCA9377 802.11ac Wireless Network Adapter  
Kernel driver in use: ath10k_pci  
Kernel modules: ath10k_pci
# 网络控制器： 高通创锐讯 QCA9377 802.11ac 无线网络适配器（修订版 31）  

```

从结果中可以看出以太网驱动(r8169)与无线网驱动(ath10k_pci)都被正常安装了，那么驱动大概是没问题了，下面再看下ip连接情况[^1]
```shell 详细输出网络端口信息
ip -s -h -d link show
```

```shell 显示结果
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000  
link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00 promiscuity 0 allmulti 0 minmtu 0 maxmtu 0 addrgenmode eui64 numtxqueues 1 numrxqueues 1 gso_max_size 65536 gso_  
max_segs 65535 tso_max_size 524280 tso_max_segs 65535 gro_max_size 65536 gso_ipv4_max_size 65536 gro_ipv4_max_size 65536  
RX:  bytes packets errors dropped  missed   mcast  
123M    292k      0       0       0       0  
TX:  bytes packets errors dropped carrier collsns  
123M    292k      0       0       0       0  
# 本地环路接口：localhost,状态正常

2: enp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000  
link/ether 3c:2c:30:a6:7e:1c brd ff:ff:ff:ff:ff:ff promiscuity 0 allmulti 0 minmtu 68 maxmtu 1500 addrgenmode none numtxqueues 1 numrxqueues 1 gso_max_size 64000 gso_  
max_segs 64 tso_max_size 64000 tso_max_segs 64 gro_max_size 65536 gso_ipv4_max_size 64000 gro_ipv4_max_size 65536 parentbus pci parentdev 0000:02:00.0  
RX:  bytes packets errors dropped  missed   mcast  
0       0      0       0       0       0  
TX:  bytes packets errors dropped carrier collsns  
0       0      0       0       0       0  
# 以太网(PCI接口位置：bus=2, solt=0),被关闭(从state DOWN 可以看出)

3: wlan0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN mode DORMANT group default qlen 1000  
link/ether 0e:fe:d0:ec:e9:e4 brd ff:ff:ff:ff:ff:ff permaddr 80:2b:f9:b4:bd:db promiscuity 0 allmulti 0 minmtu 256 maxmtu 2304 addrgenmode none numtxqueues 1 numrxqueu  
es 1 gso_max_size 65536 gso_max_segs 65535 tso_max_size 65536 tso_max_segs 65535 gro_max_size 65536 gso_ipv4_max_size 65536 gro_ipv4_max_size 65536 parentbus pci parentde  
v 0000:03:00.0  
RX:  bytes packets errors dropped  missed   mcast  
0       0      0       0       0       0  
TX:  bytes packets errors dropped carrier collsns  
0       0      0       0       0       0  
# 编号为0的无线网设备，也被关闭了(估计这就是问题所在了)

5: enp0s20f0u1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN mode DEFAULT group default qlen 1000  
link/ether 36:dd:9e:02:c4:48 brd ff:ff:ff:ff:ff:ff promiscuity 0 allmulti 0 minmtu 0 maxmtu 65535 addrgenmode none numtxqueues 1 numrxqueues 1 gso_max_size 65536 gso_  
max_segs 65535 tso_max_size 65536 tso_max_segs 65535 gro_max_size 65536 gso_ipv4_max_size 65536 gro_ipv4_max_size 65536 parentbus usb parentdev 1-1:1.0  
RX:  bytes packets errors dropped  missed   mcast  
1.57M   1.93k      0       0       0       0  
TX:  bytes packets errors dropped carrier collsns  
461k   1.51k      0       0       0       0
# 经过对比试验，这个是USB共享网络的设备名
```


## 尝试修复
```shell 开启waln0 设备
ip link set wlan0 up
```

再检查一下，使用`ip link`命令
```shell 再次检查的输出
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000  
link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00  
2: enp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000  
link/ether 3c:2c:30:a6:7e:1c brd ff:ff:ff:ff:ff:ff  
5: enp0s20f0u1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN mode DEFAULT group default qlen 1000  
link/ether 36:dd:9e:02:c4:48 brd ff:ff:ff:ff:ff:ff  
6: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DORMANT group default qlen 1000  
link/ether 62:c2:78:ae:ef:2d brd ff:ff:ff:ff:ff:ff permaddr 80:2b:f9:b4:bd:db
# wifi状态已经是开启了🎉🎉🎉🎉
```
## 连接WIFI
开启iwctl[^2]（这个工具比较美观，操作也简单）
```shell 开启iwd服务并打开iwctl
systemctl start iwd.service && iwctl
```

```shell 在iwctl中显示无线网络设备
[iwd]# device list
Devices                                   *
--------------------------------------------------------------------------------
Name                  Address               Powered     Adapter     Mode
--------------------------------------------------------------------------------
wlan0                 aa:dc:f2:a5:15:ce     on          phy0        station

```
再次验证Wlan0设备已经启动了，现在应该可以连接wifi🛜了

```shell 扫描网络
[iwd]# station wlan0 get-networks  
Available networks  
--------------------------------------------------------------------------------  
Network name                      Security            Signal  
--------------------------------------------------------------------------------  
Tenda_122CC0                      psk                 ****  
X-ray                             psk                 ****  
ChinaNet-244d8687                 open                ****  
ChinaNet-3n4p                     psk                 ****  
WIFI                              psk                 ****  
```

```shell 连接wifi
[iwd]# station wlan0 connect Tenda_122CC0  
Type the network passphrase for Tenda_122CC0 psk.  
Passphrase: **********  
```

```shell 查看连接情况
[iwd]# station wlan0 show  
Station: wlan0  
--------------------------------------------------------------------------------  
Settable  Property              Value  
--------------------------------------------------------------------------------  
Scanning              no  
State                 connected  
Connected network     Tenda_122CC0  
No IP addresses       Is DHCP client configured?  
ConnectedBss          b0:df:c1:12:2c:c1  
Frequency             2417  
Security              WPA2-Personal  
RSSI                  -79 dBm  
AverageRSSI           -79 dBm  
TxBitrate             1000 Kbit/s  
RxBitrate             1000 Kbit/s
```
 最后再分配下动态IP[^3]
```shell 使用dhcpcd来分配ip
dhcpcd wlan0
```

## 修复kde桌面下不显示网络连接选项的问题
```shell 安装kde的网络管理工具(network manager)
sudo pacman -S plasma-nm
```


![完成了](../../../images/20230709/done.webp)


```shell 下面是一些用到的命令：
lspci -k // 检查驱动状态
ip link
iw dev // 以上三种方法都能列出网卡接口名称，如果未列出，说明驱动有问题。

dmesg | grep firmware
dmesg | grep iwlwifi // 这两种方法是在驱动有问题时，可以用来检查和寻找问题。

iw dev wlan0 link // 用来检查网卡是否连接，其中 wlan0 是网卡接口名称

ip link set dev wlan0 up // 用来将无线网卡接口启用

iw dev wlan0 scan // 用来扫描周围的无线热点

iw dev wlan0 connect "your_essid" // 此命令可以直接连接没有加密的热点
iw dev wlan0 connect "your_essid" key 0:your_key // 用来连接WEP加密的热点

wpa_supplicant -D nl80211,wext -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf 
// 这是 wpa_supplicant 连接WPA/WPA2的命令格式

dhcpcd wlan0 // 给无线网卡分配动态IP
```


[^1]: https://blog.csdn.net/luotuo28/article/details/125000308
[^2]: https://wiki.archlinuxcn.org/wiki/%E7%BD%91%E7%BB%9C%E9%85%8D%E7%BD%AE/%E6%97%A0%E7%BA%BF%E7%BD%91%E7%BB%9C%E9%85%8D%E7%BD%AE#iw
[^3]: http://wangbinweb.github.io/htm/18-archlinux-install-wireless-network-card.htm