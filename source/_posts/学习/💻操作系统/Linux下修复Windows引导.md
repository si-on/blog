---
categories:
  - 学习
  - 💻操作系统
abbrlink: fix_Windows_efi_in_Linux
title: Linux下修复Windows引导
tags: 
date: 2023-10-24 08:22:08
updated: 2023-10-24 08:22:08
cover: 
root: ../../
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
katex: false
password: 
theme: xray
hidden: 
published: false
description: 
sticky: 
keywords:
---

> <center>顺其自然</center>
> <p align="right">——Sion</p>

-----
## 事件始末

鄙人在九月份的某一天忽然心血来潮，破戒装了个Graude[^2]，然而正当我陶醉于安装各种开源软件、转移浏览器资料的琐事上时，一个恶果已悄然成熟，落地。

💥Windows系统没了！虽说毕业后就下决心转移到Linux上，但一个图片管理软件的数据不好迁移，便迟迟没有行动。这一下子，备份都还没备份呢，我傻眼了，脑瓜子嗡嗡的。

十月份，尝试在使用Linux上一些牛逼的开源工具进行修复，但奈何技术不行，找不到合适的软件，也就不了了之。十一月份，在某一次整理书摘的时候，专用软件不支持Linux,不得不装了个Windows LSTC，然后用了几天，尝试着去修复死去的另一个系统，居然成功了，这里特地把经验分享下，希望对那些陷入跟我一样窘境的同志有所帮助。

## 事件分析
根据肇事者两个多月的反思，他终于明白了问题所在，现整理其资料，概括如下：

受害者（指电脑）是由两个独立的盘A+B组成的：
1. A盘(`/dev/sda`)速度慢，但容量大
2. B盘(`/dev/sdb`)速度快，但容量小

肇事者先在B盘上全盘安装了Windows系统，但由于盘符判定算法故障或是设置出错，引导分区却被放置到了A盘头部预存的EFI分区中。之后他又利用分盘器将B盘后端分出1/4的大小来安装Linux，安装过程中，将A盘的头部清空来放置Linux的EFI分区，结果导致原先Windows的引导分区被删，系统无法启动，受害者陷入重度昏迷状态。此时B盘的分区表如下：

```txt 
##|v1|## /dev/sdb 的分区表
# 创建于 2023/10/26 19:20:14
类型: "gpt"
排序依据: "分区号"

分区号 起点 终点 类型 角色 标签 状态
1;2048;34815;unknown;primary;"Windows保留分区";"不可用"
2;34816;167806975;ntfs;primary;"Windows11";"昏迷"
3;167806976;250069646;btrfs;primary;"GraudeLinux";"活跃"
```

安装Windows后的分区情况如下：
![](../../../images/20230709/Linux下修复Windows引导-20231205204858238.webp)
## 成功尝试
1. 在A盘空闲分区安装临时Windows,EFi分区放在中间就好（这样仍然可以引导启动），
2. 在Windows上使用编辑引导，将Windows11添加进去
![](../../../images/20230709/Linux下修复Windows引导-20231205204311331.webp)
3. 重启若干次，等待Windows引导系统自动修复即可



## 失败尝试

### U盘急救
先搜索一番：
1. [不慎删除 Win 10 EFI 分区的修复方法 | TripleZ's Blog](https://blog.triplez.cn/posts/rescue-win-10-efi-partition/)
2. [巨坑！windows10误删EFI分区后的恢复操作【已解决】\_删除windows的efi文件后可以用恢复吗-CSDN博客](https://blog.csdn.net/wlher/article/details/108858742)
3. [行家里手：Linux使用WoeUSB制作Windows启动盘 | 薄荷开源网](http://www.mintos.org/soft/woeusb-winboot.html)

然后做了个启动盘，结果却做成了个安装盘。但毕竟可以进入命令行了，便又找到了这个教程进行修复：[Windows 11 UEFI引导修复详细操作步骤！](https://www.disktool.cn/content-center/repair-windows-11-uefi-bootloader-2111.html)。一波三折，坏事成串，在这个教程中我又遇到了要重建MBR的问题，关键一重建，整个系统都没了，重建个鬼啊！放弃之。

![](../../../images/20230709/Pasted%20image%2020231024083414.png)
![](../../../images/20230709/Pasted%20image%2020231024083629.png)



### 尝试使用虚拟机来创建启动盘PE

1. 安装VirtualBox[^1]
![](../../../images/20230709/Pasted%20image%2020231026141950.png)
2. 安装贼慢，启动后无法连接网络与U盘
3. 放弃

[^1]: [VirtualBox在Archlinux中报错"rc=-1908"及无法下载增强工具的解决办法 - 知乎](https://zhuanlan.zhihu.com/p/83391251)
[^2]: 属于ArchLinux系中一款极为美观的发行版。