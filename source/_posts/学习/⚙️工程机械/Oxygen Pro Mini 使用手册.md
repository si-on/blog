---
title: Oxygen Pro Mini 使用手册
abbrlink: Oxygen Pro Mini User Guide
categories:
  - 学习
  - ⚙️工程机械
root: ../../
katex: false
theme: xray
hidden: true
published: false
date: 2025-02-25 22:53:33
updated: 2025-02-25 22:53:33
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
description:
sticky:
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
<!-- timeline 2023-08-01-->
 
<!-- endtimeline -->
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}

{% endmarkmap %}

-----


## 简介
本文为[Oxygen Pro Mini User Guide v1.2](https://www.m-audio.com/support/downloads.html#oxypmini)的非官方中译，仅供参考，使用者应有能力阅读原始文本。

## 安装
### 连接
在开始使用Oxygen Pro Mini 音乐键盘(以下简称Omi)之前，你需要连接硬件，配置软件，设置键盘的操作模式。
**通过附赠的数据线，把Omi连接到电脑上。**将数据线USB-B端插到键盘上，另一端连接到电脑的USB-A口中(也可以连接到电脑的USB拓展坞上)。
> 注意：键盘当且仅当连接到可供电的USB接口中时才能够传输数据。如果你想将其连接到一个已经有其他设备连接的拓展坞上时，为了保证电源稳定，推荐使用供电的拓展坞。