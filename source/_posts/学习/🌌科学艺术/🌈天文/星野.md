---
title: 星野试验
abbrlink: unnamedPost
categories:
  - 学习
  - "\U0001F30C科学艺术"
  - "\U0001F308天文"
root: ../../
katex: false
theme: xray
published: false
date: 2024-11-04 10:22:03
updated: 2024-11-04 10:22:03
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
<!-- timeline 2023-08-01-->
 
<!-- endtimeline -->
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}

{% endmarkmap %}

-----

