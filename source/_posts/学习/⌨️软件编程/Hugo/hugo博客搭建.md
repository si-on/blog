---
title: hugo博客搭建
abbrlink: unnamedPost
categories:
  - 学习
  - ⌨️软件编程
  - Hugo
root: ../../
katex: false
theme: xray
published: false
date: 2023-11-02 08:43:57
updated: 2023-11-02 08:43:57
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---

-----

最近发现Hugo博客自定义(魔改)很方便，正好这个Hexo博客也看累了，今天上午便尝试着用Hugo搭建一个博客，下面是搭建过程中的记录：
1. 安装：在Arch下只需要一个命令即可：`sudo pacman -S hugo`
2. 新建站点：`hugo new site /path/to/site`
```shell
╭─sion@sion in ~ as 🧙 took 93ms
╰─λ hugo new site ~/雨过

Congratulations! Your new Hugo site was created in /home/sion/雨过.

Just a few more steps...

1. Change the current directory to /home/sion/雨过.
2. Create or install a theme:
- Create a new theme with the command "hugo new theme <THEMENAME>"
- Install a theme from https://themes.gohugo.io/
3. Edit hugo.toml, setting the "theme" property to the theme name.
4. Create new content with the command "hugo new content <SECTIONNAME>/<FILENAME>.<FORMAT>".
5. Start the embedded web server with the command "hugo server --buildDrafts".

See documentation at https://gohugo.io/.
```

