---
categories:
  - 学习
  - ⌨️软件编程
  - Python
title: 数据预处理笔记(sklearn)
abbrlink: preprocessing@sklearn
tags:
  - 数据分析
  - 机器学习
date: 2025-03-02 23:52:59
updated: 
cover: /images/Cover/preprocessing@sklearn.svg
root: ../../
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
katex: false
password: 
theme: xray
hidden: 
published: true
description: 数据预处理笔记
sticky: 
keywords: 
highlight_shrink: true
---
## 前言
![导图（通过 Draw.si-on.top绘制）](../../../../images/20250104/数据预处理笔记(sklearn)-20250302095906576.png)

{% tip cogs %}
本文机器学习笔记的应用部分整体上基于`sklearn`的[Preprocessing](https://scikit-learn.org/stable/api/sklearn.preprocessing.htm)模块进行撰写，特征选择基于[decomposition](https://scikit-learn.org/stable/api/sklearn.decomposition.html)模块。
{% endtip %}
{% tip fa-robot %}
本文代码部分由Kimi 辅助撰写，源码顶部标注为Prompt内容。
{% endtip %}
## 数据集
```python X线物料工艺统计数据
data = {
	"steel": ["316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "201J1", "201J1", "201J1", "201J1", "201J2", "201J2", "201J2"],
	"type": ["FP", "FP", "FP", "FP", "FP", "FP", "FP", "FP", "FP", "SP", "SP", "HTA", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "FP", "FP", "HTA", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "FP"],
	"thickness": [0.028, 0.038, 0.04, 0.048, 0.048, 0.055, 0.08, 0.13, 0.13, 0.085, 0.12, 0.098, 0.07, 0.065, 0.078, 0.078, 0.082, 0.11, 0.116, 0.127, 0.13, 0.13, 0.147, 0.15, 0.185, 0.25, 0.202, 0.15, 0.104, 0.104, 0.11, 0.182, 0.104, 0.152, 0.182],
    "tv": [1.6, 1.3, 1.2, 1.9, 2, 2.2, 2.5, 3.2, 3.2, 2.8, 3, 3.5, 1.6, 2.4, 2.2, 2.6, 2.2, 2.5, 2.3, 3.2, 2.5, 2.5, 3.3, 3.2, 3.2, 2.5, 2.5, 4.5, 1.9, 1.9, 2.3, 3.2, 1.9, 3, 3],
    "temp": [1120, 1040, 1030, 1060, 1060, 1070, 1080, 950, 1000, 1130, 1140, 960, 1110, 1090, 1100, 1130, 1100, 1110, 1140, 1140, 1140, 1120, 1140, 1140, 1140, 1100, 1120, 800, 1130, 1130, 1140, 1140, 1130, 1140, 1140]
}
```
## 特征编码
当数据为分类型而不是数值型时，往往需要进行编码，但简单记作1，2，3是行不通的，因为机器会认为这钟排列是有序的。可以看出上面数据集中的`steel`与`type`为**分类型特征**，处理时需要转化为哑变量或者独热处理(one-hot)，为了实现无序的编码，可以通过sklearn的`OneHotEncoder()`类实现。

分析下，可以数据集中的两个分类特征有如下特点：
* `steel`有四个类：316L、3004、201J1、201J2
* `type`有三个类：FP、SP、HTA

```python 独热编码12种，如果数据集中所有存在彼此独立的分类，那么其可以设计成
1:[[1.0.0.0.1.0.0]]
2:[[1.0.0.0.0.1.0]]
3:[[1.0.0.0.0.0.1]]
4:[[0.1.0.0.1.0.0]]
5:[[0.1.0.0.0.1.0]]
6:[[0.1.0.0.0.0.1]]
7:[[0.0.1.0.1.0.0]]
8:[[0.0.1.0.0.1.0]]
9:[[0.0.1.0.0.0.1]]
10:[[0.0.0.1.1.0.0]]
11:[[0.0.0.1.0.1.0]]
12:[[0.0.0.1.0.0.1]]
```


```python 数据集中的两个分类特征有如下特点：steel有四个类：316L、3004、201J1、201J2； type有三个类：FP、SP、HTA。将此数据集中的steel与type分类特征进行特征编码，使用sklearn的onehotencoder实现。
import pandas as pd
from sklearn.preprocessing import OneHotEncoder

# 原始数据
data = {
    "steel": ["316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "201J1", "201J1", "201J1", "201J1", "201J2", "201J2", "201J2"],
    "type": ["FP","FP", "FP", "FP", "FP", "FP", "FP", "FP", "FP", "FP", "SP", "SP", "HTA", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "FP", "FP", "HTA", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "FP"],
}

df = pd.DataFrame(data)

# 显式指定类别顺序（避免自动排序）
steel_categories = ['316L', '304', '201J1', '201J2']
type_categories = ['FP', 'SP', 'HTA']

# 创建编码器并设置参数
encoder = OneHotEncoder(
    categories=[steel_categories, type_categories],
    sparse_output=False,
    drop=None  # 保留所有类别（默认不删除）
)

# 执行编码
encoded_data = encoder.fit_transform(df[['steel', 'type']])

# 转换为可读的 DataFrame 格式（仅用于展示）
encoded_df = pd.DataFrame(
    encoded_data,
    columns=encoder.get_feature_names_out(['steel', 'type'])
)

print(f"总特征维度: {encoded_data.shape[1]}（4 steel + 3 type）")
print("\n编码结果示例:")
print(encoded_df.head(12))
```

![可视化结果](../../../../images/20250104/数据预处理笔记(sklearn)-20250304160738304.png)
## 标准化
将分类型特征处理好后，开始着手其它三组`thickness`、`tv`、`temp`，他们虽属于数值型，但其数值分布相差数量级较大，为了更好地进行后一步处理，在此需要将其标准化——去均值(将其设为0)、去方差(方差为1)，按比例缩放。

> 大多数机器学习算法中目标函数的基础都是——假设所有的特征都是零均值，如果某个特征的方差比其他特征大几个数量级，它就会在算法中占据主导地位，导致学习器产生一种“先入为主”的偏见。

经过计算可得
$$
\begin{aligned}
\text{方差}&\text{ (Variance)} \\
\sigma^2_{\text{thickness}} &= \frac{1}{n} \sum_{i=1}^{n} (x_{\text{thickness}, i} - \mu_{\text{thickness}})^2\\
\sigma^2_{\text{tv}} &= \frac{1}{n} \sum_{i=1}^{n} (x_{\text{tv}, i} - \mu_{\text{tv}})^2 \\
\sigma^2_{\text{temp}} &= \frac{1}{n} \sum_{i=1}^{n} (x_{\text{temp}, i} - \mu_{\text{temp}})^2 \\
\\
\text{标准差}&\text{ (Standard Deviation)} \\
\sigma_{\text{thickness}} &= \sqrt{\sigma^2_{\text{thickness}}} \\
\sigma_{\text{tv}} &= \sqrt{\sigma^2_{\text{tv}}} \\
\sigma_{\text{temp}} &= \sqrt{\sigma^2_{\text{temp}}}
\end{aligned}
$$

|    特征     |    标准差    |    方差    |
| :-------: | :-------: | :------: |
| thickness | 0.050896  | 0.002590 |
|    tv     | 0.689879  | 0.475933 |
|   temp    | 72.008403 | 5185.210 |

### Z-score标准化
如字面意思，此方法主要是实现零均值(Zero Score)，核心函数是`scale()`，默认是各个参数独立地进行标准化。标准化后可以使用`StandardScaler().fit()`观察其缩放比例，以便于日后增加新数据后应用到测试样本。

```python 对这个数据集的thickness、tv、temp进行z-score标准化处理，使用sklearn的scale()函数。要求列出转化前后上面三个特征的标准差、方差，用散点图表示出来，绘制时把thickness、tv、temp之间的三个散点图，与转化后的三个散点图以2*3的形式罗列。
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale

# 数据
data = {
   # "steel": ["316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "316L", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "304", "201J1", "201J1", "201J1", "201J1", "201J2", "201J2", "201J2"],
   #"type": ["FP", "FP", "FP", "FP", "FP", "FP", "FP", "FP", "FP", "SP", "SP", "HTA", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "FP", "FP", "HTA", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "SP", "FP"],
    "thickness": [0.028, 0.038, 0.04, 0.048, 0.048, 0.055, 0.08, 0.13, 0.13, 0.085, 0.12, 0.098, 0.07, 0.065, 0.078, 0.078, 0.082, 0.11, 0.116, 0.127, 0.13, 0.13, 0.147, 0.15, 0.185, 0.25, 0.202, 0.15, 0.104, 0.104, 0.11, 0.182, 0.104, 0.152, 0.182],
    "tv": [1.6, 1.3, 1.2, 1.9, 2, 2.2, 2.5, 3.2, 3.2, 2.8, 3, 3.5, 1.6, 2.4, 2.2, 2.6, 2.2, 2.5, 2.3, 3.2, 2.5, 2.5, 3.3, 3.2, 3.2, 2.5, 2.5, 4.5, 1.9, 1.9, 2.3, 3.2, 1.9, 3, 3],
    "temp": [1120, 1040, 1030, 1060, 1060, 1070, 1080, 950, 1000, 1130, 1140, 960, 1110, 1090, 1100, 1130, 1100, 1110, 1140, 1140, 1140, 1120, 1140, 1140, 1140, 1100, 1120, 800, 1130, 1130, 1140, 1140, 1130, 1140, 1140]
}

# 创建 DataFrame
df = pd.DataFrame(data)

# 提取需要标准化的列
features = df[["thickness", "tv", "temp"]]

# 使用 sklearn.preprocessing.scale() 进行 Z-score 标准化
scaled_features = scale(features)
scaled_df = pd.DataFrame(scaled_features, columns=["thickness", "tv", "temp"])

# 绘制散点图
plt.figure(figsize=(15, 10))

# 定义绘图函数
def plot_scatter(ax, x, y, title, xlabel, ylabel, color="blue"):
    ax.scatter(x, y, c=color)
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

# 第一行：原始数据
plot_scatter(plt.subplot(2, 3, 1), features["thickness"], features["tv"], "Original: Thickness vs TV", "Thickness", "TV")
plot_scatter(plt.subplot(2, 3, 2), features["thickness"], features["temp"], "Original: Thickness vs Temp", "Thickness", "Temp")
plot_scatter(plt.subplot(2, 3, 3), features["tv"], features["temp"], "Original: TV vs Temp", "TV", "Temp")

# 第二行：标准化后的数据
plot_scatter(plt.subplot(2, 3, 4), scaled_df["thickness"], scaled_df["tv"], "Scaled: Thickness vs TV", "Thickness (scaled)", "TV (scaled)", color="red")
plot_scatter(plt.subplot(2, 3, 5), scaled_df["thickness"], scaled_df["temp"], "Scaled: Thickness vs Temp", "Thickness (scaled)", "Temp (scaled)", color="red")
plot_scatter(plt.subplot(2, 3, 6), scaled_df["tv"], scaled_df["temp"], "Scaled: TV vs Temp", "TV (scaled)", "Temp (scaled)", color="red")

plt.tight_layout()
plt.show()

```

![零均值标准化前后](../../../../images/20250104/数据预处理笔记(sklearn)-20250302152506972.png)
### 极差标准化
当希望把均值缩放到指定范围内时，可以采用极差标准化，可以通过
* `MinMaxScaler()`：缩放到0~1
* `MaxAbsScaler()`：缩放到单位大小

```python 将下面数据极差标准化，将原始数据缩放至[0,1]，并将缩放前后的数据进行可视化。通过MinMaxscaler函数实现。
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler

# 数据
data = {
    "thickness": [0.028, 0.038, 0.04, 0.048, 0.048, 0.055, 0.08, 0.13, 0.13, 0.085, 0.12, 0.098, 0.07, 0.065, 0.078, 0.078, 0.082, 0.11, 0.116, 0.127, 0.13, 0.13, 0.147, 0.15, 0.185, 0.25, 0.202, 0.15, 0.104, 0.104, 0.11, 0.182, 0.104, 0.152, 0.182],
    "tv": [1.6, 1.3, 1.2, 1.9, 2, 2.2, 2.5, 3.2, 3.2, 2.8, 3, 3.5, 1.6, 2.4, 2.2, 2.6, 2.2, 2.5, 2.3, 3.2, 2.5, 2.5, 3.3, 3.2, 3.2, 2.5, 2.5, 4.5, 1.9, 1.9, 2.3, 3.2, 1.9, 3, 3],
    "temp": [1120, 1040, 1030, 1060, 1060, 1070, 1080, 950, 1000, 1130, 1140, 960, 1110, 1090, 1100, 1130, 1100, 1110, 1140, 1140, 1140, 1120, 1140, 1140, 1140, 1100, 1120, 800, 1130, 1130, 1140, 1140, 1130, 1140, 1140]
}

# 创建 DataFrame
df = pd.DataFrame(data)

# 提取需要缩放的列
features = df[["thickness", "tv", "temp"]]

# 使用 MinMaxScaler 进行极差标准化
scaler = MinMaxScaler()
scaled_features = scaler.fit_transform(features)
scaled_df = pd.DataFrame(scaled_features, columns=["thickness", "tv", "temp"])

# 打印缩放前后的数据
print("缩放前的数据：")
print(features.head())
print("\n缩放后的数据：")
print(scaled_df.head())

# 可视化
plt.figure(figsize=(15, 6))

# 原始数据
plt.subplot(1, 2, 1)
plt.scatter(features["thickness"], features["tv"], c="blue", label="Original Thickness vs TV", alpha=0.7)
plt.scatter(features["thickness"], features["temp"], c="green", label="Original Thickness vs Temp", alpha=0.7)
plt.scatter(features["tv"], features["temp"], c="red", label="Original TV vs Temp", alpha=0.7)
plt.title("Original Data")
plt.xlabel("Thickness")
plt.ylabel("TV/Temp")
plt.legend()

# 缩放后的数据
plt.subplot(1, 2, 2)
plt.scatter(scaled_df["thickness"], scaled_df["tv"], c="blue", label="Scaled Thickness vs TV", alpha=0.7)
plt.scatter(scaled_df["thickness"], scaled_df["temp"], c="green", label="Scaled Thickness vs Temp", alpha=0.7)
plt.scatter(scaled_df["tv"], scaled_df["temp"], c="red", label="Scaled TV vs Temp", alpha=0.7)
plt.title("Scaled Data")
plt.xlabel("Thickness (scaled)")
plt.ylabel("TV/Temp (scaled)")
plt.legend()

plt.tight_layout()
plt.show()

```
![极差标准化前后](../../../../images/20250104/数据预处理笔记(sklearn)-20250303151722462.png)

> 代码中的关键步骤是`scaled_features = scaler.fit_transform(features)`，这行代码通过`fit`：计算数据的最小值和最大值（拟合数据）， `transform`再根据公式，利用fit(拟合阶段)计算的最小值和最大值，将数据缩放到   [0, 1]   范围内。
### 正则化
正则化：缩放单个样本使其具有单位范数的过程。每个特征相当于相当于一个维度，N个特征的一个样本可以看作一个N维向量。范数可以理解为向量的长度，正则化就是把这些向量长度缩放到1。通过`normalize()`函数实现缩放。有三种实现方法：
* 特征值除以特征值绝对值之和
* 特征值除以特征值平方和的开方
* 特征值除以特征值中的最大值

```python 通过normalize()函数，分别使用11，12，max范数，对每对特征（thickness-tv，thickness-temp，temp-tv）进行正则化处理，并可视化原始数据与三幅正则化后的数据。每行四幅图，三对特征共十二幅图。
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize

# 数据
data = {
    "thickness": [0.028, 0.038, 0.04, 0.048, 0.048, 0.055, 0.08, 0.13, 0.13, 0.085, 0.12, 0.098, 0.07, 0.065, 0.078, 0.078, 0.082, 0.11, 0.116, 0.127, 0.13, 0.13, 0.147, 0.15, 0.185, 0.25, 0.202, 0.15, 0.104, 0.104, 0.11, 0.182, 0.104, 0.152, 0.182],
    "tv": [1.6, 1.3, 1.2, 1.9, 2, 2.2, 2.5, 3.2, 3.2, 2.8, 3, 3.5, 1.6, 2.4, 2.2, 2.6, 2.2, 2.5, 2.3, 3.2, 2.5, 2.5, 3.3, 3.2, 3.2, 2.5, 2.5, 4.5, 1.9, 1.9, 2.3, 3.2, 1.9, 3, 3],
    "temp": [1120, 1040, 1030, 1060, 1060, 1070, 1080, 950, 1000, 1130, 1140, 960, 1110, 1090, 1100, 1130, 1100, 1110, 1140, 1140, 1140, 1120, 1140, 1140, 1140, 1100, 1120, 800, 1130, 1130, 1140, 1140, 1130, 1140, 1140]
}

# 创建 DataFrame
df = pd.DataFrame(data)

# 提取特征对
pairs = {
    "thickness-tv": df[["thickness", "tv"]],
    "thickness-temp": df[["thickness", "temp"]],
    "temp-tv": df[["temp", "tv"]]
}

# 定义范数类型
norms = ["l1", "l2", "max"]

# 可视化
plt.figure(figsize=(20, 15))

# 遍历每对特征
for i, (pair_name, pair_data) in enumerate(pairs.items(), start=1):
    # 原始数据
    plt.subplot(3, 4, (i - 1) * 4 + 1)
    plt.scatter(pair_data.iloc[:, 0], pair_data.iloc[:, 1], c="blue", label="Original")
    plt.title(f"Original {pair_name}")
    plt.xlabel(pair_data.columns[0])
    plt.ylabel(pair_data.columns[1])

    # 正则化后的数据
    for j, norm in enumerate(norms, start=2):
        normalized_data = normalize(pair_data, norm=norm)
        plt.subplot(3, 4, (i - 1) * 4 + j)
        plt.scatter(normalized_data[:, 0], normalized_data[:, 1], c="red", label=f"{norm} norm")
        plt.title(f"{norm.upper()} Norm {pair_name}")
        plt.xlabel(pair_data.columns[0])
        plt.ylabel(pair_data.columns[1])

plt.tight_layout()
plt.show()

```
![分别使用三种正则化方法对thickness、tv、temp进行正则化处理](../../../../images/20250104/数据预处理笔记(sklearn)-20250303163326568.png)


`normalize` 是 `sklearn.preprocessing` 模块中的一种数据正则化方法。它通过参数 `X` 接收需要正则化的数组，参数 `norm` 接收正则化算法的标识符，可选值为 `'l1'`、`'l2'` 和 `'max'`，默认值为 `'l2'`。参数 `axis=1` 表示按样本进行正则化，而 `axis=0` 表示按特征进行正则化，默认值为 `axis=1`。

* 当 `norm='l1'` 时，正则化方法是将样本的各个特征值除以这些特征值的绝对值之和。这种变换属于线性变换，因此变换后的结果会落在以 (0,1) 和 (1,0) 为端点的连线上。
* 当 `norm='l2'` 时，正则化方法是将样本的各个特征值除以这些特征值的平方和的平方根。经过这种变换后，样本的各个特征值的平方和为 1，即变换后的样本会落在以 (0,0) 为圆心、1 为半径的单位圆上。
* 当 `norm='max'` 时，正则化方法是将样本的各个特征值除以样本中特征值的最大值。经过这种变换后，样本会落在以 x=1 和 y=1 为边界的直线上。

## 插补
由于各种原因，真实世界的许多数据集都包含缺失数据，一般被定义为`NaN`，或者是其他占位符。这些数值不能被机器学习直接兼容，但其中存在价值，舍弃又觉得可惜。为此需要从已知数据中推断出缺失的数值。
sklearn.preprocessing的`imputer()`类用于处理缺失值，估算策略有：均值；中位数；众数。其实处理缺失值的最佳方法是预测或手动添加，此类方法过于粗劣，在此不过多研究。

值得一提的是有一种高维映射插补的方式——高维映射插补（采用独热编码方式），将K个离散的属性值拓展为K+1个属性值，若此值缺失，则将拓展后的第K+1属性值设为1。
$$
\begin{aligned}
&\begin{bmatrix}
8 & 1 & 6 \\
3 & 5 & 7 \\
4 & 9 & 0 \\
\end{bmatrix}
\quad
\xrightarrow{\text{🚀升维}}
\quad
\begin{bmatrix}
8 & 1 & 6 & 0 \\
3 & 5 & 7 & 0\\
4 & 9 & 0 & 1 \\
0 & 0 & 0 & 0 \\
\end{bmatrix}
\end{aligned}
$$

## 特征降维
当数据集的特征太多时，需要进行数据降维以加快训练进度。（虽然本数据中特征只有三种，降维后得不偿失，但为了学习，姑且尝试下吧）
### PCA分析（主成分分析）
通过正交化方法，将高维空间的原始数据映射到低维空间中。关键点：**寻找一组最优正交基，使得高维数据集的样本点投影到低维空间后，新的样本点在低维空间中尽可能地分散。**
```python 将下面数据极差标准化，将原始数据缩放至[0,1]，然后使用PCA方法对此数据进行特征降维，并用seaborn可视化数据集前后的变化
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA

# 数据
data = {
    "thickness": [0.028, 0.038, 0.04, 0.048, 0.048, 0.055, 0.08, 0.13, 0.13, 0.085, 0.12, 0.098, 0.07, 0.065, 0.078, 0.078, 0.082, 0.11, 0.116, 0.127, 0.13, 0.13, 0.147, 0.15, 0.185, 0.25, 0.202, 0.15, 0.104, 0.104, 0.11, 0.182, 0.104, 0.152, 0.182],
    "tv": [1.6, 1.3, 1.2, 1.9, 2, 2.2, 2.5, 3.2, 3.2, 2.8, 3, 3.5, 1.6, 2.4, 2.2, 2.6, 2.2, 2.5, 2.3, 3.2, 2.5, 2.5, 3.3, 3.2, 3.2, 2.5, 2.5, 4.5, 1.9, 1.9, 2.3, 3.2, 1.9, 3, 3],
    "temp": [1120, 1040, 1030, 1060, 1060, 1070, 1080, 950, 1000, 1130, 1140, 960, 1110, 1090, 1100, 1130, 1100, 1110, 1140, 1140, 1140, 1120, 1140, 1140, 1140, 1100, 1120, 800, 1130, 1130, 1140, 1140, 1130, 1140, 1140]
}

# 创建 DataFrame
df = pd.DataFrame(data)

# 极差标准化
scaler = MinMaxScaler()
scaled_data = scaler.fit_transform(df)
scaled_df = pd.DataFrame(scaled_data, columns=df.columns)

# PCA 降维
pca = PCA(n_components=2)  # 降维到 2 维
pca_result = pca.fit_transform(scaled_data)

# 将 PCA 结果转换为 DataFrame
pca_df = pd.DataFrame(pca_result, columns=['PC1', 'PC2'])

# 设置 Seaborn 风格
sns.set_theme(style="whitegrid")

# 创建一个图形窗口
plt.figure(figsize=(14, 6))

# 原始数据的小提琴图
plt.subplot(1, 2, 1)
sns.violinplot(data=scaled_df, inner="quartile", palette="viridis")
plt.title("Original Data (Normalized)")
plt.xlabel("Features")
plt.ylabel("Normalized Values")

# PCA 降维后的数据的小提琴图
plt.subplot(1, 2, 2)
sns.violinplot(data=pca_df, inner="quartile", palette="viridis")
plt.title("PCA Reduced Data (2D)")
plt.xlabel("Principal Components")
plt.ylabel("Values")

plt.tight_layout()
plt.show()

```

![将三个特征降维至两个特征前后的对比](../../../../images/20250104/数据预处理笔记(sklearn)-20250304163149555.png)
如果多维特征空间中一组数据是线性相关的，

### LDA降维
省略之
### TSNE降维
省略之

## 后记
> 数据集的标准化、正则化、特征降维是为应对不同数量级对机器学习性能的影响、减小计算量的一种无奈之举。实际上，这些预处理操作都会不可避免地造成信息的丢失。例如，在训练分类模型时，虽然有的问题在原始数据集上取得了不错的结果，但使用标准化后的数据集训练分类器，性能反而有所下降。因此，应该根据问题的复杂程度、计算量的大小，具体问题具体分析，可以比较使用上述预处理方法训练的估计器模型与使用标准化、降维后的数据集训练的估计器，在计算量与估计器性能之间取得平衡。

学了一个多星期，终于结束了，接下来要到大头了——算法篇！