#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 22:25:15 2024
Updated on Tue Jan 23 23:49:11 2024
Version: 0.0.1
Author: sion
Description: 求解倒班与星期的重合。一周七天，倒班六天，查找倒班休息日与周末重合的时间与天数
"""


print('🌄本程序可以计算三班倒（白白夜夜休休）与正常工作日休息时间的重合情况。')
days = int(input('⚪请输入要计算的天数：'))

# 用不可改变的元组来定义星期循环、倒班循环与格式化的汉字输出
weekcycle = (1, 2, 3, 4, 5, 6, 7)
workcycle = (1, 2, 3, 4, 5, 6)
datecn = ('一', '二', '三', '四', '五', '六', '日')

i = 0
j = int(input('⚪今天周几？'))-1
k = int(input('⚪倒班第几天？'))-1

for i in range(days):
    # print(i, weekcycle[j], workcycle[k]) 调试语句
    if (weekcycle[j] == 6 or weekcycle[j] == 7) and (workcycle[k] == 5 or workcycle[k] == 6):
        print('从今天开始的第', i+1, '天，休息日重合，该天是周',
              datecn[weekcycle[j]-1], '，倒班第', datecn[workcycle[k]-1], '天', sep='')
    # 星期与倒班的计数循环
    j += 1
    if j > 6:
        j = 0
    k += 1
    if k > 5:
        k = 0
