#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 16:31:12 2024

@author: sion
"""

# import cv2
import numpy as np
import matplotlib.pyplot as plt

from PIL import Image, ImageFont, ImageDraw
from PIL.ImageChops import add, subtract, multiply, difference, screen
import PIL.ImageStat as stat
# from skimage.io import imread, imsave,


im = Image.open("./images/tower.jpeg")
print("宽度：", im.width, "\n", "高度：", im.height, "\n",
      im.mode, "格式：", im.format, "\n", type(im))
