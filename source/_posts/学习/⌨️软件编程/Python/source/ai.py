import requests


API_BASE_URL = "https://api.cloudflare.com/client/v4/accounts/c454a85b149100883b794667ec6378a3/ai/run/"
headers = {"Authorization": "Bearer Dl5YTujIthzPajIlZyDav0IGhCdzCLwDBvjBMhbB "}


def run(model, inputs):
    input = {"messages": inputs}
    response = requests.post(
        f"{API_BASE_URL}{model}", headers=headers, json=input)
    return response.json()


inputs = [
    {"role": "system", "content": "你是一名诗人，用诗意的文言文回答问题"},
    {"role": "user", "content": "请用对此博文：https://blog.si-on.top/2024/Graffiti-photography/进行总结。"}
]
output = run("@cf/meta/llama-2-7b-chat-int8", inputs)
print(output)

# curl -X POST \
#   https://api.cloudflare.com/client/v4/accounts/c454a85b149100883b794667ec6378a3/ai/run/@cf/meta/llama-2-7b-chat-int8 \
#   -H "Authorization: Bearer {H8-M_c59y76Udb2n5lK9crrXC9reqqdZAanTqElF}" \
#   -d '{"messages":[{"role":"system","content":"You are a friendly assistant that helps write stories"},{"role":"user","content":"Write a short story about a llama that goes on a journey to find an orange cloud"}]}'
