#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 13:22:12 2024

@author: sion
"""

# 类的试验
year = 2024
month = 1
day = 21
temperature = 15.3
week = 'Sunday'
weather = 'cloudy'
name = 'sion'
date = [year, month, day]
date_tuple = (year, month, day)
HUB = {date_tuple, name, week, weather, temperature}
# 布尔值
print('year的布尔值为：', bool(year))
print('name的布尔值为：', bool(name))
print('🫥空布尔值为：', bool())
print('\n')
# 整数型
print('temperature的整型值为', int(temperature))
print('🫥空整型值为：', int())
print('\n')

# 浮点型
print('month的浮点型值为', float(month))
print('year的浮点型值为', float(year))
print('🫥空浮点型值为：', float())
print('\n')

# 列表型
print('列表date的值为', list(date))
print('🫥空列表型的值为', list())
print('\n')

# set集合型
print('集合HUB的值为：', HUB)
