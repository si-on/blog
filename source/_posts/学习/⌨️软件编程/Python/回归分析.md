---
title: 回归分析笔记（Live update）
abbrlink: regression@sklearn
categories:
  - 学习
  - ⌨️软件编程
  - Python
root: ../../
katex: false
theme: xray
hidden: false
published: true
description: 回归分析笔记（持续更新中）
date: 2025-03-06 10:43:20
updated: 2025-03-06 10:43:20
tags:
  - 数据分析
  - 机器学习
  - 数值分析
cover: /images/Cover/regression@sklearn.svg
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
password: 
sticky: 
keywords:
---

![](../../../../images/20250104/回归分析-20250306104325336.png)
## 回归方程拟合度评价
* $SST$：总偏差平方和：因变量实际值与因变量平均值的差的平方和，反应因变量取值的总体波动情况
* $SSR$：回归平方和：因变量回归值与其均值的差的平方和，反映了因变量偏差值<u>由X的关系</u>所引起的偏差，可以由回归曲线进行解释。
* $SSE$：残差平方和：因变量实际值与回归值的差的平方和，反映了因变量偏差值<u>由X的关系以外的其他关系</u>所引起的偏差，不可以由回归曲线进行解释。
$$ SST=SSR+SSE$$
定义:
$$R2=SSR/SST$$
其取值范围为$0\sim 1$越接近于1说明拟合效果越好。
## 线性回归实战
`sklearn`通过`linear_model`模块（广义线性模型）来进行线性回归预测，其模型原理为：
$$
y(w,x)=w_0+w_1x_1+w_2x_2+\cdots+w_px_p
$$


