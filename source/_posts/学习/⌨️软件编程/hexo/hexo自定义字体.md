---
title: Hexo 自定义字体
abbrlink: oh_my_font
categories:
  - 学习
  - ⌨️软件编程
  - hexo
tags:
  - 主题魔改
root: ../../
katex: false
theme: xray
published: true
description: 自定义字体，自有博客之基本操作也，自由独立之体现也，审美志趣之征显也。
date: 2023-09-29 08:57:10
updated: 2023-09-29 08:57:10
cover: false
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
password: 
hidden: true
sticky: 
keywords:
---


## 概述

当下之世界，字体[^1]大抵可分宋、楷、黑、艺四类，其形各异，其用有别。

宋，出自印刷工业，历来为纸质书籍中最为常用之字体，辨识度高，富有水墨文雅之气。其中佼佼者，有方正、adobe-std、思源等，余以为最佳者，思源宋体也。

楷，自古人手书文字演变而来，常用于文字引用、内容强调、古诗文排版等，气势浑厚，富有艺术感。其众者甚多，开源之霞鹜文楷可谓一优秀者也。

黑，现代设计思想浸润字体而成也，电子设备UI、网路版面、代码展示最常用之，如视窗、苹果、Linux三大操作系统，皆以黑体为基本。开源之黑体，余以文泉驿微米黑为上品。

艺，凡不属于其上三者，余皆为艺术字体也。艺术字体，设计师天马行空之创作，样式繁多，不可一概而论。其中GNU-Unifont为像素风格中最是完善者，其他如钢笔、书法等风格亦为余所喜者。

## 字体之选择
正文主宋，标题、代码用黑，引用为楷。此亦即$\LaTeX$之原始版式，阅读体验甚好。下面为余所收集之字体下载网站，来访者前往可下载使用之。

1. [Adobe开源字库](https://github.com/adobe-fonts/)
2. [谷歌字库](https://fonts.google.com/)    
3. [中文开源字体集](https://github.com/DrXie/OSFCC)    
4. [中文开源字体站](https://font.gentleflow.tech/)    
5. [GNU Unifont](https://unifoundry.com/unifont/)    
6. [方正字体库](https://www.foundertype.com/index.php/FindFont/index/hot/121/hot_name/%E5%85%8D%E8%B4%B9.html)    
7. [自由字体](https://ziyouziti.com/index-index-all.html)    
8. [新蒂字体](http://sentyfont.com/) （个人非商用）
## 字体之格式
字体皆以文件储存，格式为统一之用，现有`TTF(TrueType Font)`、`OTF (OpenType Font)`、`WOFF(Web Open Font Format)`、`WOFF2`为主流，其作用场景不同，文件大小各异。

论其大小，TTF与OTF大，WOFF小，WOFF2最小。另有一法，非取全字库而单取所用之诸字，则文件可简化也。
### 格式转换之工具
>所用者书，不用者不书。

此处介绍一WOFF2生成工具：`woff2`
```shell
extra/woff2 1.0.2-4
Web Open Font Format 2 reference implementation
```
使用极为简单：`woff2_compress oh_my_font.otf`
```shell
╭─sion@sion in ~/下载 as admin took 222ms  
╰─λ woff2_compress HanyiSentyYongleEncyclopedia-2020.ttf  
Processing HanyiSentyYongleEncyclopedia-2020.ttf => HanyiSentyYongleEncyclopedia-2020.woff2  
Compressed 5800508 to 2911822.
```
尺寸几乎可减小一半。

再借由[字体分块](https://chinese-font.netlify.app/)来提高加载速度，效果甚佳[^2]。


## 字体之引入
### 本地字体引入
此处使用css样式来引入
```css
@font-face {
  font-family: 'yongle';	
  src: url(/fonts/HanyiSentyYongleEncyclopedia-2020.woff2);
  font-weight: normal;
  font-style: normal;
}
```

然而似乎并没有作用，怪事啊！怪事啊！
### 网络字体引入
1. 谷歌字体
``` css
- <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+SC:wght@500,600,800,900&display=swap" rel="stylesheet">
```
2. 霞鹜文楷
```css
- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lxgw-wenkai-webfont/1.7.0/style.min.css" />
```

使用时直接更改选项即可：
```yaml butterfly主题配置文件
font-family: "LXGW WenKai"

#font-family: "Noto Serif SC",shusong,"LXGW WenKai"
```

[^1]: 此文所论皆开源或个人非商用字体，用者仍需斟酌一二，以防不必要之麻烦。
[^2]: [Drizzle，中文字体分片，2023.10.11 ](https://shiyu.dev/archives/2627/%e4%b8%ad%e6%96%87%e5%ad%97%e4%bd%93%e5%88%86%e7%89%87/)