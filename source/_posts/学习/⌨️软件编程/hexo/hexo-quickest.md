---
title: hexo 一条龙
tags:
  - Hexo
  - 奇技淫巧
categories:
  - 学习
  - ⌨️软件编程
  - hexo
cover: ../../../../images/blogimage/raw/master/hexo.svg
description: 让hexo快速从云端落地
abbrlink: hexo-quickest
date: 2021-12-19 19:43:42
katex:
comments:
copyright:
aside:
password:
hidden:
---

# hexo 一条龙服务

为了更好更快速的部署hexo博客，避免频繁的操作（都是因为懒），这里我将总结所有必须的命令，最终实现三分钟将hexo博客从远程同步到本地，并进行管理。

1. git 配置（全局）
   
   ```shell
   git config --global user.name "aornus"
   git config --global user.email aornus@matrixmail.com
   git config --list
   ```

2. ssh配置
   
   * 公私钥生成
   
   ```shell
   cd ~/.ssh
   ssh-keygen -t rsa -b 4200 -C "linux@laptop" -f linux
   cat linux.pub
   //添加公钥到git账户
   ssh -i ~/.ssh/linux -T git@git***.com
   ```
   
   * ssh连接成功后，在.ssh 下创建`config`，内容如下：
   
   ```
   Host github.com
   HostName git***.com
   User aornus(用户名)
   IdentityFile ~/.ssh/私匙名
   IdentitiesOnly yes
   ```
   
   * 克隆远端项目
   
   ```bash
   git clone git@git***.com:aornus/aornus.git
   ```

3. npm配置
   
   ```bash
   sudo pacman -S npm   
   ```
   
   顺便更新到最新版本并使用淘宝源：[node使用](https://www.codenong.com/cs106531292/)
   
   ```shell
   npm config set registry https://registry.npm.taobao.org
   npm install -g npm
   #然后会有提示，按照最新版就好，这里是8.3.0版
   npm install -g npm@8.3.0
   ```
   
   安装hexo包 （全局）
   
   ```shell
   npm install -g hexo-cli
   ```

4. hexo恢复部署
   
   ```shell
   cd ~/aornus
   npm install --save
   hexo clear && hexo s -g
   ```

4. 安装常用hexo插件包并启用数学公式支持
   
   ```shell
   npm un hexo-renderer-marked hexo-renderer-kramed --save
   ```
   
   ```shell butterfly主题常用包2022-5-22
   npm install hexo-renderer-pug hexo-renderer-stylus hexo-deployer-git  hexo-wordcount hexo-hide-posts  hexo-blog-encrypt  hexo-generator-feed  hexo-generator-sitemap  hexo-tag-aplayer  hexo-pdf  hexo-renderer-markdown-it  @neilsustc/markdown-it-katex  hexo-butterfly-extjs--save
   ```
   
6. picgo配置（gitee）

   1. 下载picgo的[Appimagine](https://github.com/Molunerfinn/picgo/releases)
      
      快速下载-->[地址](https://github.91chi.fun//https://github.com//Molunerfinn/PicGo/releases/download/v2.3.0/PicGo-2.3.0.AppImage)

   2. 安装插件，这里选用的是*zhanghuid*开发的`gitee 2.05`
      
      ```
      #我的仓库地址：../../../../images/blogimage
      #配置如下：
         owner:aornus
         repo:blogimage
         parh:
         token:0qa*********d9
         message:picgo linux
      ```

## 故障记录

在一次往gitlab中添加windows11的密钥时，出现如下错误：
```shell
PS C:\Users\agape\.ssh> ssh -i .\windows11 -T git@gitlab.com
Bad permissions. Try removing permissions for user: UNKNOWN\\UNKNOWN (S-1-5-21-867685769-3139671178-1785471411-1001) on file C:/Users/agape/.ssh/config.
Bad owner or permissions on C:\\Users\\agape/.ssh/config
```
经排查后，确认是**config的权限给了一个不知名的用户**（可能是病毒，也可能是双系统中linux里授权的）解决方法就是<u>删掉此用户的权限</u>：即解除继承，然后删除。

![](../../../../images/20240104/hexo-quickest-20240112082232402.webp)

```shell 成功解决🎉
PS C:\Users\agape\.ssh> ssh -i .\windows11 -T git@gitlab.com
The authenticity of host 'gitlab.com (172.65.251.78)' can't be established.
ED25519 key fingerprint is SHA256:eUXGGm1YGsMAS7vkcx6JOJdOGHPem5gQp4taiCfCLB8.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'gitlab.com' (ED25519) to the list of known hosts.
Welcome to GitLab, @si-on!
``` 
