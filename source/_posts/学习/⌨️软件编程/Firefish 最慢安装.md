---
title: Firefish 安装与维护手册
abbrlink: firefish-the-slowest
categories:
  - 学习
  - ⌨️软件编程
tags:
  - 站长
  - 运维
cover:  /images/Cover/firefish-the-slowest.svg
root: ../../
katex: false
theme: xray
published: true
description: '安装Firefish,以最慢的方式'
date: 2024-05-02 13:09:27
updated:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
sticky:
keywords:
---


{% folding green, 📑目录 %}
{% markmap 300px %}
- [运行维护技巧](#%E8%BF%90%E8%A1%8C%E7%BB%B4%E6%8A%A4%E6%8A%80%E5%B7%A7)
	- [添加Cloudflare R2对象储存](#%E6%B7%BB%E5%8A%A0cloudflare-r2%E5%AF%B9%E8%B1%A1%E5%82%A8%E5%AD%98)
- [安装](#%E5%AE%89%E8%A3%85)
	- [更新并安装基本依赖](#%E6%9B%B4%E6%96%B0%E5%B9%B6%E5%AE%89%E8%A3%85%E5%9F%BA%E6%9C%AC%E4%BE%9D%E8%B5%96)
		- [安装nodejs与pnpm](#%E5%AE%89%E8%A3%85nodejs%E4%B8%8Epnpm)
		- [安装PostgreSQL](#%E5%AE%89%E8%A3%85postgresql)
		- [安装PGroonga拓展](#%E5%AE%89%E8%A3%85pgroonga%E6%8B%93%E5%B1%95)
		- [安装Redis](#%E5%AE%89%E8%A3%85redis)
		- [安装FFmpeg](#%E5%AE%89%E8%A3%85ffmpeg)
	- [配置数据库](#%E9%85%8D%E7%BD%AE%E6%95%B0%E6%8D%AE%E5%BA%93)
	- [配置火鱼](#%E9%85%8D%E7%BD%AE%E7%81%AB%E9%B1%BC)
	- [编译火鱼](#%E7%BC%96%E8%AF%91%E7%81%AB%E9%B1%BC)
	- [网络配置](#%E7%BD%91%E7%BB%9C%E9%85%8D%E7%BD%AE)
	- [启动火鱼服务](#%E5%90%AF%E5%8A%A8%E7%81%AB%E9%B1%BC%E6%9C%8D%E5%8A%A1)
	- [升级](#%E5%8D%87%E7%BA%A7)
	- [实用技巧](#%E5%AE%9E%E7%94%A8%E6%8A%80%E5%B7%A7)
{% endmarkmap %}
{% endfolding %}
## 运行维护技巧
### 添加Cloudflare R2对象储存
人称赛博菩萨的Cloudflare提供了10G的免费储存，对于个人网站项目来说已经很多了。

我是一个爱嘟嘟的人，也经常发照片，从我的日常使用来看。半年大概能嘟嘟250Mb的东西，10G就足够嘟二十年了，到时候都成中年大叔了。

R2储存创建就不说了，这里只说明下对应关系。


| R2                                          | 示例                                                                  | Firefish     |
| ------------------------------------------- | ------------------------------------------------------------------- | ------------ |
| Name（名称）                                    | `fish`                                                              | 储存桶（Bucket）  |
| S3 API                                      | `https://c454a85b149100883b794667ec6378a3.r2.cloudflarestorage.com` | 端点（Endpoint） |
| Public R2.dev Bucket URL（公共 R2.dev 存储桶 URL） | `https://pub-8b2a53bb459641bb82a3132f46b4cf7e.r2.dev`               | 根URL         |
| Custom Domains（自定义域）                        | `https://fish.si-on.top`                                            | 根URL         |
| Access Key ID（访问密钥ID）                       | `******************`                                                | Access Key   |
| Secret Access Key（机密访问密钥）                   | `*****************`                                                 | Secret  Key  |


![配置截图](../../../images/20240104/Firefish%20最慢安装-20240526221810093.webp)




## 安装
依赖项：
1. NodeJS V20
2. PostgreSQL V16 （PGroonga 拓展）
3. Redis V7
4. Nginx
5. FFmpeg

* Rust 1.74
* C/C++ 编译器
* Python 3

### 更新并安装基本依赖
`sudo apt update && sudo install build-essential python3 curl wget git lsb-release`

#### 安装nodejs与pnpm
1. 设定版本变量为20：`NODE_MAJOR=20`
2. 下载安装包：`curl -fsSL "https://deb.nodesource.com/setup_${NODE_MAJOR}.x" | sudo -E bash -`
```shell 输出结果
sion@fish:~ $ curl -fsSL "https://deb.nodesource.com/setup_${NODE_MAJOR}.x" | sudo -E bash -
#------------------------------不华丽的分割线--------------------
2024-05-02 13:22:38 - Installing pre-requisites
Hit:1 https://mirrors.aliyun.com/docker-ce/linux/debian bookworm InRelease       Hit:2 http://deb.debian.org/debian bookworm InRelease
Hit:3 http://deb.debian.org/debian-security bookworm-security InRelease
Hit:4 http://deb.debian.org/debian bookworm-updates InRelease
Hit:5 http://archive.raspberrypi.com/debian bookworm InRelease               
Hit:6 https://pkg.cloudflare.com/cloudflared bookworm InRelease          
Reading package lists... Done
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
apt-transport-https is already the newest version (2.6.1).
ca-certificates is already the newest version (20230311).

curl is already the newest version (7.88.1-10+deb12u5).
gnupg is already the newest version (2.2.40-1.1).
gnupg set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.

Hit:1 https://mirrors.aliyun.com/docker-ce/linux/debian bookworm InRelease
Hit:2 http://deb.debian.org/debian bookworm InRelease
Hit:3 http://archive.raspberrypi.com/debian bookworm InRelease  
Hit:4 http://deb.debian.org/debian-security bookworm-security InRelease  
Hit:5 http://deb.debian.org/debian bookworm-updates InRelease       
Get:6 https://deb.nodesource.com/node_20.x nodistro InRelease [12.1 kB]
Hit:7 https://pkg.cloudflare.com/cloudflared bookworm InRelease
Get:8 https://deb.nodesource.com/node_20.x nodistro/main arm64 Packages [6,788 B]
Fetched 18.9 kB in 4s (5,169 B/s) 
Reading package lists... Done

2024-05-02 13:23:02 - Repository configured successfully. To install Node.js, run: apt-get install nodejs -y
```
1. 安装nodejs：`sudo apt-get install nodejs -y`
```shell 输出结果
sion@fish:~ $ sudo apt-get install nodejs -y
----------------------------------------
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following NEW packages will be installed:
  nodejs
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.

Need to get 30.7 MB of archives.
After this operation, 194 MB of additional disk space will be used.
Get:1 https://deb.nodesource.com/node_20.x nodistro/main arm64 nodejs arm64 20.12.2-1nodesource1 [30.7 MB]
Fetched 30.7 MB in 33s (938 kB/s) 

Selecting previously unselected package nodejs.
(Reading database ... 79321 files and directories currently installed.)
Preparing to unpack .../nodejs_20.12.2-1nodesource1_arm64.deb ...
Unpacking nodejs (20.12.2-1nodesource1) ...
Setting up nodejs (20.12.2-1nodesource1) ...
Processing triggers for man-db (2.11.2-2) ...

```

4. 开启pnpm：`sudo corepack enable && corepack prepare pnpm@latest --activate`
```shell 输出结果
sion@fish:~$ sudo corepack enable && corepack prepare pnpm@latest --activate
#------------------------------不华丽的分割线--------------------
Preparing pnpm@latest for immediate activation...

```
5. 检查版本：`node --version && pnpm --version `
```shell 输出结果
sion@fish:~ $ node --version && pnpm --version
#------------------------------不华丽的分割线--------------------
v20.12.2
9.0.6
```

#### 安装PostgreSQL 
1. 由于debian仓库内置PostgreSQL的软件包是15，按照firefish文档要求版本需要是16, 所以要添加软件源后才能安装，参考[安装教程](https://www.postgresql.org/download/linux/debian/)并u根据架构得到如下安装代码：
```shell 安装代码
sudo apt install curl ca-certificates  

sudo install -d /usr/share/postgresql-common/pgdg  

sudo curl -o /usr/share/postgresql-common/pgdg/apt.postgresql.org.asc --fail https://www.postgresql.org/media/keys/ACCC4CF8.asc 

sudo sh -c 'echo "deb [signed-by=/usr/share/postgresql-common/pgdg/apt.postgresql.org.asc arch=arm64 ] https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'  

sudo apt update  && sudo apt -y install postgresql-16

```
{% tip error %}
本设备为树莓派arm架构，需要在软件源前面加上 arch=arm64,否则会报错：

`N: Skipping acquire of configured file 'main/binary-armhf/Packages' as repository 'https://apt.postgresql.org/pub/repos/apt bookworm-pgdg InRelease' doesn't support architecture 'armhf'`
{% endtip %}

{% tip success %}
然而不管这个错误，安装时也会自动选择架构正常安装。
{% endtip %}


```shell 输出结果
sion@fish:~ $ sudo apt update  && sudo apt -y install postgresql-16
#------------------------------不华丽的分割线--------------------
Hit:1 https://mirrors.aliyun.com/docker-ce/linux/debian bookworm InRelease
Hit:2 http://deb.debian.org/debian bookworm InRelease                            Hit:3 http://deb.debian.org/debian-security bookworm-security InRelease          Hit:4 http://archive.raspberrypi.com/debian bookworm InRelease                   Hit:5 http://deb.debian.org/debian bookworm-updates InRelease                    Hit:6 https://pkg.cloudflare.com/cloudflared bookworm InRelease                  Hit:7 https://apt.postgresql.org/pub/repos/apt bookworm-pgdg InRelease
Hit:8 https://deb.nodesource.com/node_20.x nodistro InRelease
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done

The following additional packages will be installed:
  libcommon-sense-perl libjson-perl libjson-xs-perl libllvm16 libpq5 libsensors-config libsensors5 libtypes-serialiser-perl libxslt1.1 libz3-4 postgresql-client-16 postgresql-client-common postgresql-common
  ssl-cert sysstat
  
Suggested packages:
  lm-sensors postgresql-doc-16 isag
  
The following NEW packages will be installed:
  libcommon-sense-perl libjson-perl libjson-xs-perl libllvm16 libpq5 libsensors-config libsensors5 libtypes-serialiser-perl libxslt1.1 libz3-4 postgresql-16 postgresql-client-16 postgresql-client-common
  postgresql-common ssl-cert sysstat
0 upgraded, 16 newly installed, 0 to remove and 0 not upgraded.
Need to get 47.5 MB of archives.
After this operation, 216 MB of additional disk space will be used.

Get:1 http://deb.debian.org/debian bookworm/main arm64 libjson-perl all 4.10000-1 [87.5 kB]
Get:2 https://apt.postgresql.org/pub/repos/apt bookworm-pgdg/main arm64 postgresql-client-common all 259.pgdg120+1 [36.9 kB]
Get:3 http://deb.debian.org/debian bookworm/main arm64 ssl-cert all 1.1.2 [21.1 kB]
Get:4 https://apt.postgresql.org/pub/repos/apt bookworm-pgdg/main arm64 postgresql-common all 259.pgdg120+1 [182 kB]
Get:5 http://deb.debian.org/debian bookworm/main arm64 libcommon-sense-perl arm64 3.75-3 [23.0 kB]
Get:6 http://deb.debian.org/debian bookworm/main arm64 libtypes-serialiser-perl all 1.01-1 [12.2 kB]
Get:7 http://deb.debian.org/debian bookworm/main arm64 libjson-xs-perl arm64 4.030-2+b1 [90.7 kB]
Get:8 https://apt.postgresql.org/pub/repos/apt bookworm-pgdg/main arm64 libpq5 arm64 16.2-1.pgdg120+2 [207 kB]
Get:9 https://apt.postgresql.org/pub/repos/apt bookworm-pgdg/main arm64 postgresql-client-16 arm64 16.2-1.pgdg120+2 [1,850 kB]
Get:10 http://deb.debian.org/debian bookworm/main arm64 libz3-4 arm64 4.8.12-3.1 [6,282 kB]
Get:11 https://apt.postgresql.org/pub/repos/apt bookworm-pgdg/main arm64 postgresql-16 arm64 16.2-1.pgdg120+2 [17.2 MB]
Get:12 http://deb.debian.org/debian bookworm/main arm64 libllvm16 arm64 1:16.0.6-15~deb12u1 [20.6 MB]                                                             Get:13 http://deb.debian.org/debian bookworm/main arm64 libsensors-config all 1:3.6.0-7.1 [14.3 kB]                                                            Get:14 http://deb.debian.org/debian bookworm/main arm64 libsensors5 arm64 1:3.6.0-7.1 [33.3 kB]                                                            Get:15 http://deb.debian.org/debian bookworm/main arm64 libxslt1.1 arm64 1.1.35-1 [231 kB]                                                                       Get:16 http://deb.debian.org/debian bookworm/main arm64 sysstat arm64 12.6.1-1 [570 kB]                                                                                                                             
Fetched 47.5 MB in 4min 48s (165 kB/s)                                                                                                                                                                              
Preconfiguring packages ...
Selecting previously unselected package libjson-perl.
(Reading database ... 84759 files and directories currently installed.)
Preparing to unpack .../00-libjson-perl_4.10000-1_all.deb ...
Unpacking libjson-perl (4.10000-1) ...
Selecting previously unselected package postgresql-client-common.
Preparing to unpack .../01-postgresql-client-common_259.pgdg120+1_all.deb ...
Unpacking postgresql-client-common (259.pgdg120+1) ...
Selecting previously unselected package ssl-cert.
Preparing to unpack .../02-ssl-cert_1.1.2_all.deb ...
Unpacking ssl-cert (1.1.2) ...
Selecting previously unselected package postgresql-common.
Preparing to unpack .../03-postgresql-common_259.pgdg120+1_all.deb ...
Adding 'diversion of /usr/bin/pg_config to /usr/bin/pg_config.libpq-dev by postgresql-common'
Unpacking postgresql-common (259.pgdg120+1) ...
Selecting previously unselected package libcommon-sense-perl:arm64.
Preparing to unpack .../04-libcommon-sense-perl_3.75-3_arm64.deb ...
Unpacking libcommon-sense-perl:arm64 (3.75-3) ...
Selecting previously unselected package libtypes-serialiser-perl.
Preparing to unpack .../05-libtypes-serialiser-perl_1.01-1_all.deb ...
Unpacking libtypes-serialiser-perl (1.01-1) ...
Selecting previously unselected package libjson-xs-perl.
Preparing to unpack .../06-libjson-xs-perl_4.030-2+b1_arm64.deb ...
Unpacking libjson-xs-perl (4.030-2+b1) ...
Selecting previously unselected package libz3-4:arm64.
Preparing to unpack .../07-libz3-4_4.8.12-3.1_arm64.deb ...
Unpacking libz3-4:arm64 (4.8.12-3.1) ...
Selecting previously unselected package libllvm16:arm64.
Preparing to unpack .../08-libllvm16_1%3a16.0.6-15~deb12u1_arm64.deb ...
Unpacking libllvm16:arm64 (1:16.0.6-15~deb12u1) ...
Selecting previously unselected package libpq5:arm64.
Preparing to unpack .../09-libpq5_16.2-1.pgdg120+2_arm64.deb ...
Unpacking libpq5:arm64 (16.2-1.pgdg120+2) ...
Selecting previously unselected package libsensors-config.
Preparing to unpack .../10-libsensors-config_1%3a3.6.0-7.1_all.deb ...
Unpacking libsensors-config (1:3.6.0-7.1) ...
Selecting previously unselected package libsensors5:arm64.
Preparing to unpack .../11-libsensors5_1%3a3.6.0-7.1_arm64.deb ...
Unpacking libsensors5:arm64 (1:3.6.0-7.1) ...
Selecting previously unselected package libxslt1.1:arm64.
Preparing to unpack .../12-libxslt1.1_1.1.35-1_arm64.deb ...
Unpacking libxslt1.1:arm64 (1.1.35-1) ...
Selecting previously unselected package postgresql-client-16.
Preparing to unpack .../13-postgresql-client-16_16.2-1.pgdg120+2_arm64.deb ...
Unpacking postgresql-client-16 (16.2-1.pgdg120+2) ...
Selecting previously unselected package postgresql-16.
Preparing to unpack .../14-postgresql-16_16.2-1.pgdg120+2_arm64.deb ...
Unpacking postgresql-16 (16.2-1.pgdg120+2) ...
Selecting previously unselected package sysstat.
Preparing to unpack .../15-sysstat_12.6.1-1_arm64.deb ...
Unpacking sysstat (12.6.1-1) ...
Setting up postgresql-client-common (259.pgdg120+1) ...
Setting up libsensors-config (1:3.6.0-7.1) ...
Setting up libpq5:arm64 (16.2-1.pgdg120+2) ...
Setting up libcommon-sense-perl:arm64 (3.75-3) ...
Setting up libz3-4:arm64 (4.8.12-3.1) ...
Setting up ssl-cert (1.1.2) ...
Setting up libsensors5:arm64 (1:3.6.0-7.1) ...
Setting up libtypes-serialiser-perl (1.01-1) ...
Setting up libjson-perl (4.10000-1) ...
Setting up libxslt1.1:arm64 (1.1.35-1) ...
Setting up libllvm16:arm64 (1:16.0.6-15~deb12u1) ...
Setting up sysstat (12.6.1-1) ...

Creating config file /etc/default/sysstat with new version
update-alternatives: using /usr/bin/sar.sysstat to provide /usr/bin/sar (sar) in auto mode
Created symlink /etc/systemd/system/sysstat.service.wants/sysstat-collect.timer → /lib/systemd/system/sysstat-collect.timer.
Created symlink /etc/systemd/system/sysstat.service.wants/sysstat-summary.timer → /lib/systemd/system/sysstat-summary.timer.
Created symlink /etc/systemd/system/multi-user.target.wants/sysstat.service → /lib/systemd/system/sysstat.service.
Setting up libjson-xs-perl (4.030-2+b1) ...
Setting up postgresql-client-16 (16.2-1.pgdg120+2) ...
update-alternatives: using /usr/share/postgresql/16/man/man1/psql.1.gz to provide /usr/share/man/man1/psql.1.gz (psql.1.gz) in auto mode
Setting up postgresql-common (259.pgdg120+1) ...

Creating config file /etc/postgresql-common/createcluster.conf with new version
Building PostgreSQL dictionaries from installed myspell/hunspell packages...
Removing obsolete dictionary files:
Created symlink /etc/systemd/system/multi-user.target.wants/postgresql.service → /lib/systemd/system/postgresql.service.
Setting up postgresql-16 (16.2-1.pgdg120+2) ...
Creating new PostgreSQL cluster 16/main ...
/usr/lib/postgresql/16/bin/initdb -D /var/lib/postgresql/16/main --auth-local peer --auth-host scram-sha-256 --no-instructions
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "en_GB.UTF-8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /var/lib/postgresql/16/main ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... Asia/Shanghai
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok
Processing triggers for man-db (2.11.2-2) ...
Processing triggers for libc-bin (2.36-9+rpt2+deb12u6) ...
```


2. 开启Postgresql：`sudo systemctl enable --now postgresql`

```shell 输出结果
sion@fish:~ $ sudo systemctl enable --now postgresql  
#------------------------------不华丽的分割线--------------------
Synchronizing state of postgresql.service with SysV service script with /lib/systemd/systemd-sysv-install.  
Executing: /lib/systemd/systemd-sysv-install enable postgresql
```

```shell 检查版本
sion@fish:~ $ psql --version  
psql (PostgreSQL) 16.2 (Debian 16.2-1.pgdg120+2)
```


#### 安装PGroonga拓展
跟着[官网教程](https://pgroonga.github.io/install/debian.html)走

```shell 添加源
$ sudo apt install -y -V ca-certificates lsb-release wget
$ wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
$ sudo apt install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
$ wget https://packages.groonga.org/debian/groonga-apt-source-latest-$(lsb_release --codename --short).deb
$ sudo apt install -y -V ./groonga-apt-source-latest-$(lsb_release --codename --short).deb
$ sudo apt update
```

```shell 安装
sion@fish:/etc/apt/sources.list.d $  sudo apt install -y -V postgresql-16-pgdg-pgroonga

#------------------------------不华丽的分割线--------------------
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
   libarrow1500 (15.0.2-1)
   libgroonga0 (14.0.2-1)
   libprotobuf32 (3.21.12-3)
   libre2-9 (20220601+dfsg-1+b1)
   libsnappy1v5 (1.1.9-3)
   libutf8proc2 (2.8.0-1)
The following NEW packages will be installed:
   libarrow1500 (15.0.2-1)
   libgroonga0 (14.0.2-1)
   libprotobuf32 (3.21.12-3)
   libre2-9 (20220601+dfsg-1+b1)
   libsnappy1v5 (1.1.9-3)
   libutf8proc2 (2.8.0-1)
   postgresql-16-pgdg-pgroonga (3.2.0-1)
0 upgraded, 7 newly installed, 0 to remove and 0 not upgraded.
Need to get 11.0 MB of archives.
After this operation, 58.7 MB of additional disk space will be used.
Get:1 https://packages.groonga.org/debian bookworm/main arm64 libgroonga0 arm64 14.0.2-1 [1,917 kB]
Get:2 http://deb.debian.org/debian bookworm/main arm64 libprotobuf32 arm64 3.21.12-3 [821 kB]              
Get:3 https://apache.jfrog.io/artifactory/arrow/debian bookworm/main arm64 libarrow1500 arm64 15.0.2-1 [7,384 kB]                                             
Get:4 https://packages.groonga.org/debian bookworm/main arm64 postgresql-16-pgdg-pgroonga arm64 3.2.0-1 [612 kB]  
Get:5 http://deb.debian.org/debian bookworm/main arm64 libre2-9 arm64 20220601+dfsg-1+b1 [156 kB]      Get:6 http://deb.debian.org/debian bookworm/main arm64 libsnappy1v5 arm64 1.1.9-3 [25.4 kB]            
Get:7 http://deb.debian.org/debian bookworm/main arm64 libutf8proc2 arm64 2.8.0-1 [60.6 kB]            Fetched 11.0 MB in 45s (244 kB/s)                                                                                                                                                                                   
Selecting previously unselected package libprotobuf32:arm64.
(Reading database ... 87232 files and directories currently installed.)
Preparing to unpack .../0-libprotobuf32_3.21.12-3_arm64.deb ...
Unpacking libprotobuf32:arm64 (3.21.12-3) ...
Selecting previously unselected package libre2-9:arm64.
Preparing to unpack .../1-libre2-9_20220601+dfsg-1+b1_arm64.deb ...
Unpacking libre2-9:arm64 (20220601+dfsg-1+b1) ...
Selecting previously unselected package libsnappy1v5:arm64.
Preparing to unpack .../2-libsnappy1v5_1.1.9-3_arm64.deb ...
Unpacking libsnappy1v5:arm64 (1.1.9-3) ...
Selecting previously unselected package libutf8proc2:arm64.
Preparing to unpack .../3-libutf8proc2_2.8.0-1_arm64.deb ...
Unpacking libutf8proc2:arm64 (2.8.0-1) ...
Selecting previously unselected package libarrow1500:arm64.
Preparing to unpack .../4-libarrow1500_15.0.2-1_arm64.deb ...
Unpacking libarrow1500:arm64 (15.0.2-1) ...
Selecting previously unselected package libgroonga0:arm64.
Preparing to unpack .../5-libgroonga0_14.0.2-1_arm64.deb ...
Unpacking libgroonga0:arm64 (14.0.2-1) ...
Selecting previously unselected package postgresql-16-pgdg-pgroonga.
Preparing to unpack .../6-postgresql-16-pgdg-pgroonga_3.2.0-1_arm64.deb ...
Unpacking postgresql-16-pgdg-pgroonga (3.2.0-1) ...
Setting up libre2-9:arm64 (20220601+dfsg-1+b1) ...
Setting up libutf8proc2:arm64 (2.8.0-1) ...
Setting up libsnappy1v5:arm64 (1.1.9-3) ...
Setting up libprotobuf32:arm64 (3.21.12-3) ...
Setting up libarrow1500:arm64 (15.0.2-1) ...
Setting up libgroonga0:arm64 (14.0.2-1) ...
Setting up postgresql-16-pgdg-pgroonga (3.2.0-1) ...
Processing triggers for postgresql-common (259.pgdg120+1) ...
Building PostgreSQL dictionaries from installed myspell/hunspell packages...
Removing obsolete dictionary files:
Processing triggers for libc-bin (2.36-9+rpt2+deb12u6) ...

```

#### 安装Redis
参考[官网教程](https://redis.io/docs/latest/operate/oss_and_stack/install/install-redis/install-redis-on-linux/)

```shell 安装
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt-get update
sudo apt-get install redis

```

```shell 结果
sion@fish:~ $ sudo apt-get install redis  
#------------------------------不华丽的分割线--------------------
Reading package lists... Done  
Building dependency tree... Done  
Reading state information... Done  
The following additional packages will be installed:  
 redis-server redis-tools  
Suggested packages:  
 ruby-redis  
The following NEW packages will be installed:  
 redis redis-server redis-tools  
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.  
Need to get 1,383 kB of archives.  
After this operation, 9,310 kB of additional disk space will be used.  
Do you want to continue? [Y/n] y  
Get:1 https://packages.redis.io/deb bookworm/main arm64 redis-tools arm64 6:7.2.4-1rl1~bookworm1 [1,297 kB]  
Get:2 https://packages.redis.io/deb bookworm/main arm64 redis-server arm64 6:7.2.4-1rl1~bookworm1 [66.9 kB]                                                                                                            
Get:3 https://packages.redis.io/deb bookworm/main arm64 redis all 6:7.2.4-1rl1~bookworm1 [19.7 kB] Fetched 1,383 kB in 45s (30.7 kB/s)                 
Selecting previously unselected package redis-tools.  
(Reading database ... 87805 files and directories currently installed.)  
Preparing to unpack .../redis-tools_6%3a7.2.4-1rl1~bookworm1_arm64.deb ...  
Unpacking redis-tools (6:7.2.4-1rl1~bookworm1) ...  
Selecting previously unselected package redis-server.  
Preparing to unpack .../redis-server_6%3a7.2.4-1rl1~bookworm1_arm64.deb ...  
Unpacking redis-server (6:7.2.4-1rl1~bookworm1) ...  
Selecting previously unselected package redis.  
Preparing to unpack .../redis_6%3a7.2.4-1rl1~bookworm1_all.deb ...  
Unpacking redis (6:7.2.4-1rl1~bookworm1) ...  
Setting up redis-tools (6:7.2.4-1rl1~bookworm1) ...  
Setting up redis-server (6:7.2.4-1rl1~bookworm1) ...  
Setting up redis (6:7.2.4-1rl1~bookworm1) ...  
Processing triggers for man-db (2.11.2-2) ...
```

开启redis服务器
```shell
sion@fish:~ $ sudo systemctl enable --now redis-server  
#------------------------------不华丽的分割线--------------------
Synchronizing state of redis-server.service with SysV service script with /lib/systemd/systemd-sysv-install.  
Executing: /lib/systemd/systemd-sysv-install enable redis-server  
Created symlink /etc/systemd/system/redis.service → /lib/systemd/system/redis-server.service.  
Created symlink /etc/systemd/system/multi-user.target.wants/redis-server.service → /lib/systemd/system/redis-server.service.
```

```shell 检查版本
sion@fish:~ $ redis-cli --version  
redis-cli 7.2.4
```

#### 安装FFmpeg
这个软件很强大，也很占空间
```shell 安装费时间
sudo apt install ffmpeg
```

### 配置数据库
1. 创建角色
```shell
sudo -u postgres createuser --no-createdb --no-createrole --no-superuser --encrypted --pwprompt firefish
```
2. 创建数据库
```
sudo -u postgres createdb --encoding='UTF8' --owner=firefish firefish_db
```
3. 开启PGronngaa拓展
```
sudo -u postgres createdb --encoding='UTF8' --owner=firefish firefish_db
```

### 配置火鱼
1. 创建用户
```
sudo useradd --create-home --user-group --shell /bin/bash firefish
sudo su --login firefish
```

2.  安装Rust Toolchain（编译）
```shell 安装费时间
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
```shell 检查版本
firefish@fish:~ $ cargo --version
cargo 1.77.2 (e52e36006 2024-03-26)
```
3. 克隆火鱼仓库
```shell 费时间
git clone --branch=main https://firefish.dev/firefish/firefish.git
#------------------------------不华丽的分割线--------------------
Cloning into 'firefish'...
remote: Enumerating objects: 307009, done.
remote: Counting objects: 100% (2112/2112), done.
remote: Compressing objects: 100% (678/678), done.
remote: Total 307009 (delta 1451), reused 2055 (delta 1427), pack-reused 304897
Receiving objects: 100% (307009/307009), 405.72 MiB | 567.00 KiB/s, done.
Resolving deltas: 100% (232379/232379), done.

```
4. 修改配置文件
```shell
cd firefish
cp .config/example.yml .config/default.yml
vim .config/default.yml
```
```yml 配置文件
url: https://si-on.top  # 域名
port: 3000

db:
  host: localhost
  port: 5432
  db: firefish_db
  user: firefish
  pass: your-database-password  # 上面创建的firefish用户及密码

```
### 编译火鱼
```shell 费时间，占空间
pnpm install --frozen-lockfile
NODE_ENV=production NODE_OPTIONS='--max-old-space-size=3072' pnpm run build

```

吃了个晚饭，看了个日落，航拍了一个钟头，回来后终于成功了！

储存空间从8G升到10G又到了12G


接着进行数据库迁移：（这一步是闪代码最多的，看着贼刺激，这么复杂的东西运行起来居然不会出错，好神奇）
```shell
pnpm run migrate

----------------------
...
query: UPDATE "user" SET "alsoKnownAs" = NULL WHERE "alsoKnownAs" = '{}'
query: COMMENT ON COLUMN "user"."alsoKnownAs" IS 'URIs the user is known as too'
query: ALTER TABLE "user" DROP COLUMN "alsoKnownAsOld"
query: INSERT INTO "migrations"("timestamp", "name") VALUES ($1, $2) -- PARAMETERS: [1714099399879,"AlterAkaType1714099399879"]
Migration AlterAkaType1714099399879 has been  executed successfully.
query: COMMIT

```
### 网络配置
按照官方教程，用ufw（管理防火墙的）打开本地端口，
```shell
sudo apt install ufw
sudo ufw default deny
sudo ufw allow 80
sudo ufw allow 443
sudo ufw --force enable
```

用Caddy或nginx代理解析到域名：安装Caddy
```shell Caddy安装
sudo apt install debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
sudo apt update
sudo apt install caddy

# check version
caddy version

```
配置Caddy
```shell
sudo mv /etc/caddy/Caddyfile /etc/caddy/Caddyfile.bak
sudo nano /etc/caddy/Caddyfile

```
编辑配置文件
```config
your-server-domain.example.com {
	reverse_proxy http://127.0.0.1:3000

	log {
		output file /var/log/caddy/firefish.log
	}
}

```
重启Caddy
```shell
sudo systemctl restart caddy

```

但本教程的操作环境是一个安装使用cloudflaredn进行内网穿透的树莓派，安装好后解析3000端口到域名就行了，不需要设置代理：
![](../../../images/20240104/Firefish%20最慢安装-20240502201958402.webp)
### 启动火鱼服务
创建配置文件：

```shell
sudo vim /etc/systemd/system/firefish.service
```

文件内容：
```yml service
[Unit]
Description=Firefish daemon
# Requires=redis.service caddy.service postgresql.service # 不用Caddy就把这行注释掉
After=redis.service caddy.service postgresql.service network-online.target

[Service]
Type=simple
User=firefish
Group=firefish
UMask=0027
ExecStart=/usr/bin/pnpm run start
WorkingDirectory=/home/firefish/firefish
Environment="NODE_ENV=production"
Environment="npm_config_cache=/tmp"
Environment="NODE_OPTIONS=--max-old-space-size=3072"
# uncomment the following line if you use jemalloc (note that the path varies on different environments)
# Environment="LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2"
StandardOutput=journal
StandardError=journal
SyslogIdentifier=firefish
TimeoutSec=60
Restart=always

CapabilityBoundingSet=
DevicePolicy=closed
NoNewPrivileges=true
LockPersonality=true
PrivateDevices=true
PrivateIPC=true
PrivateMounts=true
PrivateUsers=true
ProtectClock=true
ProtectControlGroups=true
ProtectHostname=true
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectKernelLogs=true
ProtectProc=invisible
RestrictNamespaces=true
RestrictRealtime=true
RestrictSUIDSGID=true
SecureBits=noroot-locked
SystemCallArchitectures=native
SystemCallFilter=~@chown @clock @cpu-emulation @debug @ipc @keyring @memlock @module @mount @obsolete @privileged @raw-io @reboot @resources @setuid @swap
SystemCallFilter=capset pipe pipe2 setpriority

[Install]
WantedBy=multi-user.target

```

启动火鱼
```shell
sudo systemctl enable --now firefish
```



### 升级
升级前要关注下“[给管理员的建议](https://firefish.dev/firefish/firefish/-/blob/develop/docs/notice-for-admins.md)”，然后更换用户为firefish，停机，合并代码，重建，重启：
```shell 
sudo su --login firefish
cd ~/firefish
```

```shell 停止服务
systemctl stop firefish.service
```
```shell
git checkout -- packages/backend/assets && git pull --ff origin main
```
```shell
corepack prepare pnpm@latest --activate && pnpm install --frozen-lockfile && NODE_ENV='production' NODE_OPTIONS='--max_old_space_size=3072' pnpm run rebuild && pnpm run migrate
```
```shell
sudo systemctl start firefish.service
```

```shell 合成命令
sudo su --login firefish && cd ~/firefish && systemctl stop firefish.service && git checkout -- packages/backend/assets && git pull --ff origin main && corepack prepare pnpm@latest --activate && pnpm install --frozen-lockfile && NODE_ENV='production' NODE_OPTIONS='--max_old_space_size=3072' pnpm run rebuild && pnpm run migrate && systemctl start firefish.service
```
{% tip %}
【2024.7.30】更新时，受网络影响，npm包的下载会中断：
```shell error
 ETIMEDOUT  request to https://r.cnpmjs.org/@types/eslint/-/eslint-9.6.0.tgz failed, reason:

FetchError: request to https://r.cnpmjs.org/@types/eslint/-/eslint-9.6.0.tgz failed, reason:
    at ClientRequest.<anonymous> (/home/firefish/.cache/node/corepack/v1/pnpm/9.6.0/dist/pnpm.cjs:66479:18)
    at ClientRequest.emit (node:events:519:28)
    at TLSSocket.socketErrorListener (node:_http_client:500:9)
    at TLSSocket.emit (node:events:531:35)
    at emitErrorNT (node:internal/streams/destroy:169:8)
    at emitErrorCloseNT (node:internal/streams/destroy:128:3)
    at process.processTicksAndRejections (node:internal/process/task_queues:82:21)
Progress: resolved 1339, reused 1217, downloaded 121, added 121
```

这时候多试几次即可：` pnpm install --frozen-lockfile && NODE_ENV='production' NODE_OPTIONS='--max_old_space_size=3072' pnpm run rebuild && pnpm run migrate`
{% endtip %}
{% tip %}
【2024.7.6】更新时，受内存大小限制（比如本站部署在4g的树莓派上，内存就很小，编译时经常中断），有时候更新在` pnpm run rebuild`时会中断，这时候需要关闭无用软件，增加可用内存。
{% endtip %}
#### rust升级
```
Scope: 5 of 6 workspace projects
packages/firefish-js build$ pnpm tsc --project tsconfig.json ; pnpm tsc-alias --project tsconfig.json
└─ Running...
packages/backend-rs build$ pnpm run fetch && napi build --features napi --no-const-enum --platform --release --output-dir ./built/ -- --frozen
│ > backend-rs@0.0.0 fetch /home/firefish/firefish/packages/backend-rs                                                                                      │ > cargo fetch --locked --manifest-path ../../Cargo.toml
│ error: package `napi-derive v3.0.0-alpha.21` cannot be built because it requires rustc 1.80 or newer, while the currently active rustc version is 1.77.2
│ Either upgrade to rustc 1.80 or newer, or use
│ cargo update napi-derive@3.0.0-alpha.21 --precise ver
│ where `ver` is the latest version of `napi-derive` supporting rustc 1.77.2
│ Internal Error: Build failed with exit code 101
│     at ChildProcess.<anonymous> (file:///home/firefish/firefish/node_modules/.pnpm/@napi-rs+cli@3.0.0-alpha.64_@emnapi+runtime@1.2.0_@types+node@20.16.…
│     at Object.onceWrapper (node:events:634:26)
│     at ChildProcess.emit (node:events:531:35)
│     at ChildProcess._handle.onexit (node:internal/child_process:294:12)
└─ Failed in 7.7s at /home/firefish/firefish/packages/backend-rs
/home/firefish/firefish/packages/backend-rs:
 ERR_PNPM_RECURSIVE_RUN_FIRST_FAIL  backend-rs@0.0.0 build: `pnpm run fetch && napi build --features napi --no-const-enum --platform --release --output-dir ./built/ -- --frozen`
Exit status 1
 ELIFECYCLE  Command failed with exit code 1.
 ELIFECYCLE  Command failed with exit code 1.
firefish@fish:~/firefish $ rustc --version
rustc 1.77.2 (25ef9e3d8 2024-04-09)
firefish@fish:~/firefish $ rustup self update
```
### 实用技巧
* 要为所有用户添加自定义CSS，请编辑`./custom/assets/instance.css`。
* 要添加静态资产（例如启动屏幕的图像），请将它们放在`./custom/assets/`目录中。然后它们将在`https://yourserver.tld/static-assets/filename.ext`上可用。
* 要添加自定义语言环境，请将它们放在`./custom/locales/`目录中。如果您将自定义语言环境命名为与现有语言环境相同，它将覆盖它。如果您给它一个唯一的名称，它将被添加到列表中。还要确保文件名的第一部分与您所基于的语言环境相匹配。（例如：`en-FOO.yml`）
* 添加自定义错误图像，请将它们放在`./custom/assets/badges`目录中，替换已有的文件。
* 要添加自定义声音，请仅将mp3文件放在`./custom/assets/sounds`目录中。
- 要更新自定义资产而不重建，只需运行pnpm run gulp。
* 要阻止ChatGPT、Common Crawl或其他爬虫索引您的实例，请取消注释`./custom/robots.txt`中的相应规则。


-------------------------
完美，现在我了一个邮箱同款毛象ID了：

邮箱： me@si-on.top
长毛象：@me@si-on.top