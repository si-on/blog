---
title: 点室W13
abbrlink: Rescue-the-ice-treader
categories:
  - 生活
  - 周记
root: ../../
katex: false
theme: xray
published: false
description: 拯救薄冰者。
date: 2024-12-13 23:26:23
updated: 2024-12-13 23:26:23
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
sticky:
keywords:
---

> <center>已履薄冰，何不止之？已临深渊，何不返之？已入暗谷，便莫求安慰，莫久踌躇，云雀在天，逐云者安可休滞？</center>
> <p align="right">——《与来日之我书》田欣洋2019.12</p>
## 与B面之我书
世上总有一类负面的事，你或许清楚其中利害，或许洞若观火，但是却无法控制，你或许能施以方法，动以手段，甚至矫枉过正，但最终仍无法自拔，因为它总是在无形无意之中侵入。[去年在《破釜沉舟》中](/2023/plan022)我把它叫做『毒』，现在想想还是叫回『肆纵』罢了，因为它就代表那种东西——放纵的，肆虐的，即时的，享乐主义的。

(看到这里，你大概会想“按照我的性格，以及这篇文章的发布环境，这次绝对是要先狠狠批判自己，然后做个信誓旦旦，踌躇满志的总结收尾吧”

当然不是！

如果这都被你猜中，那我岂不太无趣太固化了。
)

首先要明确一点，我不在会去认真考虑事情的对与错、好与坏了！因为一旦思考这一点，就必然要去
1. 赞同甲，否定乙；
2. 然后否定赞同甲，否定否定乙，
3. 接着否定否定赞同甲，否定否定否定乙，
4. 布拉布拉布拉.......
5. 最后得到：既要批判地继承甲，也要继承地批判乙，总归甲乙都是不可缺的。

人都喜欢否定，都喜欢批判，喜欢毁灭，喜欢暴虐的风摧枯拉朽，喜欢决堤的水横流四海，喜欢用他山之石对完玉之瑕攻而又攻。

可以预见，曾几何时，现几何时，来几何时，我都会在这些判断中左右互搏，改弦易张，倒旗易帜。但所幸今夜的我思想暂时澄澈，写下了这篇文字，就有了自救的希望，就能从中庸主义泥潭中拔出腿来，就能跳下“骑着的墙”。

## 凛然

大概在2020年左右，我不知从哪里听来一个概念[^1]即——人生的重大时刻，这个时刻代表那些很如结绳记事一般值的打结的时刻，比如
* 走在路上，忽然下决心要干要学会吹竹笛
* 吃着饭忽然觉得活得太投入了，以至于忽略了很多东西，清醒的片刻甚至觉得周围人都很恐怖，然后幻想这是不是未来的自己穿越到自己身上来回忆过往了
* 看到书里的某句观点后拍案叫绝，立马改变了对整个世界的看法
* 一个人长途旅行，在铺位上思来想去睡不着，然后谋划了A、B、C、D
* .....

 这种时刻很大程度上类似于禅宗的“顿悟”——噗地一下，瞬间明白了。而后这些年，我便刻意去体验或制造这种时刻。然而事与愿违，一旦被观察，哪怕是有一丢丢"这个时刻可能是重大时刻"的感觉，预测就立马不准了，它最后也果然不能成为那种时刻。

那绳结在

如果不去否定，不去批判，那该怎样思考呢？

Pixelfed开发者dansup博客里有一句引用：『"When you don't create things, you become defined by your tastes rather than your ability. Your tastes only narrow & exclude people. So create." —Why The Lucky Stiff, Goodreads』









## \*附件（与来日之我书）
已履薄冰，何不止之？已临深渊，何不返之？已入暗谷，便莫求安慰，莫久踌躇，云雀在天，逐云者安可休滞？

现在的你可好？我在时间的这头，你在时间的那头，逝者如斯夫，不舍昼夜，虽然线性的时间让我们无法相见，但借由这封信——穿越时间的胶囊，我可以把思想传递，把梦想接力。你！准备好我希望的冲击了吗？

来者！

我现在以你之过往与你对话，废话休言，只有一问：你现在是否已更深入地了解这个世界？若无有，则启动 Plan“惩”，持续时间一周。若已有之，则我希望你以计算机为一面，英语为一面，物理为一面，设计为一面，绘画为一面，计划为一面来继续深入。了解此世界，非四年之功，乃需几辈子的努力。若已有安身立命之技能，则吾甚慰，望可秉终身学习之志，继续努力。


来者！吾愿当下之你，可以承吾志愿，继续制定计划来生活。计划自“拯救薄冰者-绝夭阏者-...”可到 Plan100号乎？工具计划从“闪跃-...”可到 Tool 42号乎？

此外，这是一些对于你的期许：
1. 第一，我希望你已经能透析一切图纸，成为一个如达芬奇一般的机械 Master。
2. 第二，我希望你在课余已经掌握了 Java 和 Python 编程语言 ，并能熟练地使用命令行去操作电脑，而且已经在某个地方租用了一个服务器，让手里这黑莓能畅游世界。
3. 第三，我希望你已掌握了不少技能，可以从一无所有开始，靠自己赚钱谋生，并为家里减轻压力  。
4. 第四，我希望你、她、他、他们都还存在且幸福地生活着。


最后，我希望你能还记着这两句箴言：
>不断的让自己有新理想、新计划，使自己有新的发挥，生活才不致平淡无奇，生命的价值也才能充分地显现 。


>夫志当存高远，慕先贤，绝情欲，弃凝滞，使庶几之志，揭然有所存，恻然有所感；忍屈伸，去细碎，广咨问，除嫌吝，虽有淹留，何损于美趣，何患于不济。若志不强毅，意不慷慨，徒碌碌滞于俗，默默束于情，永窜伏于凡庸，不免于下流矣！

<p align="right">寄信人：洋欣田</p>
<p align="right">收信人：田欣洋</p>
<p align="right">时间：2019-12-11</p>

[^1]: 我敢肯定，这绝对不是自发的，但这又有什么关系呢，它终归是落入我思想中了。