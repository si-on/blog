---
title: 点室W3
abbrlink: rocks09-22
categories:
  - 生活
  - 周记
cover: images/20240609/Weeky3-泛泛放逸-20240922222255537.png
root: ../../
aside: 
katex: false
theme: xray
hidden:
published: true
date: 2024-09-22 19:54:41
updated: 2024-09-22 19:54:41
tags:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
comments:
password:
description: 点室第三周周记。
sticky:
keywords:
---
## 给出租屋起个名字吧
按照去年七月份形成的惯例，这个月搬到了新出租屋，也要给它起个名字。

回忆一下，上次叫做“三严室”，意思是“严重积极，严重高兴，严重向上”，模仿《平凡的世界》中田晓霞跟孙少平对汉语的“特殊”运用。这次叫什么名字好呢？

大概古人起名字都是很随便的，像孔子给他儿子起名孔鲤，字伯鱼，拿到现代来说就是叫孔鲤鱼，还有梁启超的饮冰室，听起来都极为随意。那么，我随便点也没有什么大不了吧，嘻嘻嘻嘻。

![看房时拍的](../../../images/20240609/Weeky3-泛泛放逸-20240922204150011.png)

### 吃葡萄不吐葡萄皮儿屋
骑车回出租屋的路上，见路口一大叔卖葡萄，便顺手买了一袋，所以在出租屋里吃的第一个水果是葡萄。这次买的葡萄不好吃（30%的都不甜），少籽，吃得还不错，便囫囵个儿吞下了，皮儿都没吐，这不现成的名字嘛——“吃葡萄不吐葡萄皮儿屋”、“不吃葡萄倒吐葡萄皮儿室”。

有点长，那么“桃香屋”怎样？这是另外一个故事了

> 
> 某日经过A路口，忽被一阵桃香摄住魂儿，飘也似的近前去，挑肥拣瘦，择红弃绿，捧了好一大把才收手。回去后，打开袋子，透红的桃子骨碌碌得滚出来，一个一个洗得好不干净。没等水干，没等十五月亮爬上来，我便忍不住狠命地啃下去。   
> 
> 然而，吃下去后却没有那般香甜了，不过一堆嚼碎的果肉，甜甜的汁水，在唇齿间胡乱翻腾。远不如**闻**所能带来的感官上的刺激强烈。回头一想庙里神像前的贡品，我说怎不吃了去，神仙原来真是懂得享受的。
> 
> 早上带个桃子去上班，路上又想起此事，且不论尝与闻的区别(因为嗅觉、味觉、嗅觉与味觉的联系，过于医学化)，但就其感官体验而言，桃子为何这样香？
> 
> 古人说，桃之夭夭，其华灼灼，其叶蓁蓁，所以有蕡其实。他们以为花开得灿烂，果实累累能把白云压碎便是好的。在物质资料匮乏的时代，多——自然就会令人满足了。但在二十一世纪，这个物质资料极其丰富，社会关系高度和谐，人们精神境界极大提高的时代，这并不能回答我的问题。
> 
> 香精香料专家说『γ-己内酯、γ-十一内酯、δ-癸内酯、己 醇、己醛、乙酸顺-3-己烯酯、乙酸己酯和苯甲醛等 8 种主要香气成分是桃果实的特征香气成分，其中以**γ-癸内酯**为代表的内酯类化合物 是构成水蜜桃果实香气的关键成分』噢～原来是γ-癸内酯啊，但γ-癸内酯又是什么东西？十个碳的含有羧基，又含有羟基的抽象图形？！不行，太虚了，这也不足以解答我的疑虑。
> 
> 难不成是距离感，亵玩不如远观？还是因果香而对果肉的不确定性引起的奇妙心理反应？还是由猴子进化成的人类锁死到基因与迷因里的特殊代码之故？亦或是刺激，压制的吃的如古老的奥义书中所描刻的欲望？拜物教？物神化？...？？？
> 
> 到底为什么这样香呢？
> 
> 为何这样香呢？
> 
> 为毛这样香呢？
> 
> 咋他娘哩恁香！
> 
> 这问题本身就有毛病，管他甚么的，我一天闻个十回八回把桃香闻腻烦了不就成了。

呀，也不行，“桃香屋”、桃色诱惑、闻香识女人、有点色情的味道，罢了罢了。
### 彩虹🌈小屋
某天早上被南边窗外的晨曦的红光照醒，睁眼看才五点二十，离日出没多久了，赶忙跑到楼顶看日出，没想到在西边看到彩虹了，虽然也就十几秒，但让给我高兴了一整天。

“彩虹🌈小屋”怎样？有点星露谷、小女孩的感觉，不是很适合啊。
![](../../../images/20240609/Weeky3-泛泛放逸-20240922210034584.png)
### 云斋
 中秋节月亮太美[^1]，书兴大发，随笔一挥，写成一幅大字，唔哈哈哈哈啊~ ![泛泛放逸 甲辰中秋 田欣洋书](../../../images/20240609/Weeky3-泛泛放逸-20240922210844771.png)
 贴墙上后，很有感觉，关键是还被隔壁的老大娘夸了哩
 
 ...
 “小伙子，还是你这字写的好啊！”
“怎个好法呢？”
“它个儿大啊！”
“...”

这句诗，是我的签名。在2020年一次晚上，同母亲妹妹一块去外婆家转悠[^3]，他们聊家长里短，我就随便翻电子书，正好读到曹操的这句话，很是触动，他居然是个虚无主义者！然后在网上一搜，竟然没有任何人演绎、解读、使用过，那我就不客气了。

“诗，以后你就跟我子虚栈的姓了，谁欺负你，我保准第一时间爬过网线打它！”

既如此，那就叫做“~~放逸屋~~”(放映室)、"~~泛屋~~"(饭屋)，"~~逸屋~~"(义乌)[^4]。

### 予室翘翘屋
刮台风的时候，窗户被吹的哐当当响 ，很有诗经里那只猫头鹰的感觉，那就叫做——“**予室翘翘屋**”(翘翘：形容危而不稳的样子)
![](../../../images/20240609/Weeky3-泛泛放逸-20240922211244815.png) 

### 点室
某次大雨，急匆匆地坐公交回去，到门口才发现没有带钥匙，便冒着大雨，灰溜溜地冒着暴雨回了厂里，一路上边听《平面国》，边躲水坑。一个小时后，到厂里，书听完，鞋湿透。

中间听到关于“点国”的描刻(书里是以反面例子进行讽刺的)，有些体会，我不就是跟那点国国王一样，活在自己的世界吗？刚好出租屋也不大，干脆就叫“点室”罢了，做一个点点国的国王又有什么错呢？

> “他自己就是他的整个世界，也是他的整个宇宙。”

> “它填满的就是它本身。它想的就是它说的。它说的就是它听到的。它自己是思想者、叙述者和倾听者；它自己是思维、语言和声音。它是唯一，它是所有的所有。啊，多么快乐！啊，存在的快乐！”

> “啊，欢娱！啊，思想的欢愉！有什么是思想做不到的呢！它自己的思想，被它自身听见，这思想暗示它的轻蔑，从而加强它的快乐！掀起甜蜜的反叛，从而导致最后的胜利！啊，万物归一的神圣创造力！啊，欢愉，存在的欢愉！”

终于，苦苦了纠结一个钟头，找到了一个跟我同频共振的词语了，就决定是你了——“点室”。

为了防止往后谐音成电视，干脆直接承认了：点室Terminal Vision，即点室最终版



## 周总结 
这周主要干了几件事
1. 开了爱发电，把兴趣堂而皇之地大庭广众
	1. 第一阶段：整理摄影流
	2. 第二阶段：书法+篆刻
	3. 第三阶段：绘画、建模
	4. 第四阶段：MIDI编曲
2. 继续适应转白后新工作的节奏与内容、转变对领导的态度、缓和与旧同事的关系。
3. 购置出租屋所需的日常物品，同时在厂里宿舍铺位上保留基本的必需品，进一步实现工作与生活的完全脱离。

### 杂念
* 上个月分三期买了相机、镜头，这个月又租了房，导致开支明显升高，几乎到了入不敷出的地步，工资日前核算一番，这些必须的花费几乎占了70%，虽然是暂时的，但也足以让我感到压力，下个月真要勒紧裤腰带了。

* 专注力，似乎不如以前了，尤其是看东西就犯困，不过我感觉这跟我之前九个月的夜班有离不开的关系：倒班的时候几乎一直在动作，即时偷偷看书也是看一会儿活动一会儿，找马哥谈谈体会，顺便扯一通人生大道理，再找赵兄探讨一会儿机械原理。但现在我有更多的时间呆到电脑屏幕前，一直盯着文字不活动，自然是会犯困的。着并不是年龄的原因，而且现在我不就连着打字两个钟头屁事没有，越打越兴奋吗？
- 关于未来的计划，我想继续考研，但又担心静不下心，关键是有些兴趣还没有得到全面的试验，这么早就又憋在心里，迟早会在某天炸开，扰得我内心不得安宁。那么就明年吧，今年的主要矛盾呢，还是“渴望自由全面发展与物质生活条件的限制之间的矛盾”。利用有限的Money，实现最多次，最多周期的兴趣试验（这与爱发电上的发展规划一致）。
### 下周规划
* 趁领导都不明确我应该干什么的时候，抓紧时间偷偷学习，用以实现更完美的工艺控制。
* 完成《拜物》一篇，并对自己目前的拜物、拜金主义倾向进行批判，并提出解决之道。
* 把每周的每一天取一个特定主题的名字，让生活不至于太无聊。如周一洗衣日，周二野跑日，周三骑行日，周四阅读日，周五给家里打电话日，周六电影日，周日总结日。
* 把信马由缰的兴趣套住一个，锚在生活中，每周始终“如一”，不轻易改变来迎合新兴趣，这周~~暂定~~已定**练书法**，每天回去花个把钟头练习笔画、间架结构、最后写个大字发出来。
### 阅读
这周虽然从图书馆借来了好些书，但都读不进去，具体而言
1. 《Live2D基础教程》：画画用的，过程繁琐，软件不开源，学着没劲头
2. 成长类的书：
	1. 《当众孤独》《学会夸自己》：很功利性，寻找身份认同的，越读越可悲
	2. 《清单革命》，观点很好，但不值得投入大量精力去读。
3. 科普：《感知机器》、《撒野：全球第一本野跑指南》：不值得专门读
4. 文哲：《指环王与哲学》：体会很深，以至于总想写体会，导致阅读中断。
### 摄影


![阵雨时的雨幡](../../../images/20240609/Weeky3-泛泛放逸-20240922222253292.png)

![落花](../../../images/20240609/Weeky3-泛泛放逸-20240922222253762.png)

![繁花](../../../images/20240609/Weeky3-泛泛放逸-20240922222253983.png)

![野鹤与枯树](../../../images/20240609/Weeky3-泛泛放逸-20240922222254498.png)

![炸云的日出](../../../images/20240609/Weeky3-泛泛放逸-20240922222254729.png)

![都市嘟嘟](../../../images/20240609/Weeky3-泛泛放逸-20240922222255158.png)

![快乐就像摩西分红海](../../../images/20240609/Weeky3-泛泛放逸-20240922222255537.png)

![蓝烟](../../../images/20240609/Weeky3-泛泛放逸-20240922222255946.png)

![水烟](../../../images/20240609/Weeky3-泛泛放逸-20240922222256411.png)

![草帽山上的信号塔](../../../images/20240609/Weeky3-泛泛放逸-20240922222256701.png)

![窜出来的花](../../../images/20240609/Weeky3-泛泛放逸-20240922222256964.png)

![水葫芦花](../../../images/20240609/Weeky3-泛泛放逸-20240922222257421.png)

![彩虹](../../../images/20240609/Weeky3-泛泛放逸-20240922222257797.png)

![水葫芦花](../../../images/20240609/Weeky3-泛泛放逸-20240922222258287.png)

![捕鱼大爷](../../../images/20240609/Weeky3-泛泛放逸-20240922222258603.png)

![良莠不齐的莠](../../../images/20240609/Weeky3-泛泛放逸-20240922222259065.png)

![管路的罗列](../../../images/20240609/Weeky3-泛泛放逸-20240922222259386.png)


{% wow animate__flip %}  
{% link 《点室W3》原图获取, https://ifdian.net/p/268caaa878ef11efa64a5254001e7c00, /images/20240609/Weeky3-泛泛放逸-20240922222255537.png %}
{% endwow %}

[^1]: 据说是因为SIon买了便宜的五仁月饼，糖分太多，结果吃到吐了。
[^3]: 跟外婆在一个村里，母亲几乎每周都要回去一两次
[^4]: 现代人真难啊！知道的太多了，全都是谐音！