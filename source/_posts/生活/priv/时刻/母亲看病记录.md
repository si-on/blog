---
title: 母亲看病记录
abbrlink: unnamedPost
categories:
  - 生活
  - priv
  - 时刻
tags: []
root: ../../
katex: false
theme: xray
published: false
date: 2023-10-16 15:23:14
updated: 2023-10-16 15:23:14
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->



<!-- /code_chunk_output -->
{% endmarkmap %}

-----

## 时间节点
10.13 晨，母亲头晕厉害，当日办理乡医院离院手续，下午进城住院，初步检查，决定住院。
10.14 送东西，晚间妹与母亲吵架，头晕加剧
10.15 初步检查结果出来，未发现严重问题
10.16 上午输水效果不好，决定更细致检查，中午母亲崩溃，我收拾东西，下午前往照顾。下午母亲状态转好，晚间去河边转悠。
10.17 上午进行五项超声波检查，一切正常。下午胸闷，做心电图检查。晚饭后去月亮桥附近转悠，回去后头疼加剧。
10.18 一天待在病房，中午、晚上去楼下转悠，母亲睡前状态极好，晚上准备19号满七天回家
10.19 上午10点半输完水，立即办理出院，中午到家
## 关于病

### 关于药
#### 心口疼
1. 酒石酸美托洛尔片(25mg)
2. 单硝酸异山梨酯片(20mg)
## 关于母亲

## 关于妹妹

## 关于家





