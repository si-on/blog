---
title: 归家无用论
abbrlink: unnamedPost
categories:
  - 生活
  - "\U0001FAB6杂谈"
root: ../../
katex: false
theme: xray
published: false
date: 2025-01-26 03:52:48
updated: 2025-01-26 03:52:48
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

溪之有源，叶之有根，