---
title: 拜物
abbrlink: myprecious
categories:
  - 生活
  - "\U0001FAB6杂谈"
root: ../../
katex: false
theme: xray
published: false
date: 2024-09-17 21:22:01
updated: 2024-09-17 21:22:01
tags:
cover:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
aside:
comments:
password:
hidden:
description:
sticky:
keywords:
---
## 楔子：从魔戒到哲学
最近《指环王·力量之戒》(*The Lord of the Rings: The Rings of Power*)第二季开播，燃起了我读《魔戒》原著的动力，便在C市图书馆借来了本来读。

从序言中得知，《魔戒》成书于{% emp 二战 %} 期间，我不禁一惊，那样奇幻的故事，祥和而美好的世界，竟然是在一片焦土中写就的，如何不让人心生感概。但托老不喜欢大家联系书中事物与现实世界的对应，我也不就提这一层关系了（{% psw 魔戒代表原子弹！ %} ）。

{% timeline J·R·R·托尔金主要作品的时间线, %}
<!-- timeline 1937-->
 [The Hobbit (霍比特人)](https://en.wikipedia.org/wiki/The_Hobbit)
<!-- endtimeline -->
<!-- timeline 1954-1955 指环王系列-->
* [The Fellowship of the Ring (护戒使者)](https://en.wikipedia.org/wiki/The_Fellowship_of_the_Ring)
* [The Two Towers (双塔奇兵)](https://en.wikipedia.org/wiki/The_Two_Towers)
* [The Return of the King (王者归来)](https://en.wikipedia.org/wiki/The_Return_of_the_King)
<!-- endtimeline -->

<!-- timeline 1977 -->
* [The Silmarillion (精灵宝钻)](https://en.wikipedia.org/wiki/The_Silmarillion)
<!-- endtimeline -->
{% endtimeline %}

在后续搜索《魔戒》相关的书籍时，


熟悉我的人，大概知道，我是喜欢毁灭的，而且以为『批评使人进步』、『极其严厉的批评使人极其严厉地进步』。本文批判的对象仍然是我。

### 十块钱的界限
2023年8月的一个寻常下午，我走出诸葛书屋，听着阅读之声播送的『四世同堂』，冒着稀稀拉拉的小雨走到了近处工地一小贩云集的路口。

在一家新开的流动炒面店前，我点了一份炒面。鼓鼓囊囊的一大碗，有菜有肉，咸淡适宜，还贴心的加了蒜酱。吃完后我询问价钱，当老板报出十块的时候，我心里不是心疼，也不是犹豫，反而是一种得意/这回可赚到了的感觉。

吃完饭，走在路上，收音机里的只言片语把我带回到那个北京城被日本占领的年代，那个生存是一种磨磨难的年代。





把一顿花费超过十块的饭习以为常的时候，我



现在的我在工资的滋润下已经彻底失去了对钱的感觉。至少10块钱还达不到我的敏感阈值。

这样会带来一个好处，至少是我不再抠门儿了，不再把每一分钱都规划的好好的，不再受父母每月按时定量分配的限资源的限制。这是摆脱拜金主义的一个方向。


但却是拜物的另一个陷阱。


我要给予，馈赠，每月选10个博客赠与资助。



