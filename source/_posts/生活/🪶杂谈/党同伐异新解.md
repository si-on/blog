---
title: 党同伐异新解
abbrlink: how2be_shit
categories:
  - 生活
  - 🪶杂谈
tags:
  - 愤世嫉俗
  - 批判性思考
root: ../../
katex: false
theme: xray
published: false
description: 人的同质化
date: 2023-10-01 10:20:06
updated: 2023-10-01 10:20:06
cover: /images/20230709/partisan.svg
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
password: 
hidden: 
sticky: 
keywords:
---

> <center>我们的乏的古人想了几千年，得到一个制驭别人的巧法：可压服的将他压服，否则将他抬高。而抬高也就是一种压服的手段，常常微微示意说，你应该这样，倘不，我要将你摔下来了。</center>
> <p align="right">——鲁迅《华盖集》</p>
## 是不是爱好，大家说了算
听歌是个爱好吗？我不知道，但大家都说要喜欢听歌，不听便落后了，要挨打啊，要成了异己啊，所以我也听歌。

听歌是个爱好吗？我不确定，但大家都在晒歌单，捧歌手，我若不喜欢点什么，便土气了，便要为人所不屑啊，所以我也爱好听歌。

听歌是个爱好吗？我略有犹豫，但大家都说“生活不可一日不放歌！”、“耳机就是我的命根子！”、“不听歌，毋宁死！”，我若一日不听歌，便成了活死人了，要被边缘化啊，所以我也喜欢听歌。

