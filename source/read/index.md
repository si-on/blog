---
title: 涵泳📚
mathjax: true
date: 2023-08-26 09:00:32
updated: 
type: 
aside: false
description: 阅读管理与备忘
keywords: 阅读，书摘
top_img: repeating-linear-gradient(90deg, hsla(285,0%,67%,0.05) 0px, hsla(285,0%,67%,0.05) 1px,transparent 1px, transparent 96px),repeating-linear-gradient(0deg, hsla(285,0%,67%,0.05) 0px, hsla(285,0%,67%,0.05) 1px,transparent 1px, transparent 96px),repeating-linear-gradient(0deg, hsla(285,0%,67%,0.08) 0px, hsla(285,0%,67%,0.08) 1px,transparent 1px, transparent 12px),repeating-linear-gradient(90deg, hsla(285,0%,67%,0.08) 0px, hsla(285,0%,67%,0.08) 1px,transparent 1px, transparent 12px),linear-gradient(90deg, rgb(79, 30, 203),rgb(252,69,69));
aplayer: true
comments: false
highlight_shrink:
---
{% tip success %}
本页从属于秉蕳的**阅读、摄影、音乐、写作统计计划**
<p align="right">✍更新于2024.10.3</p>
{% endtip %}


>读书切戒在慌忙，涵泳工夫兴味长。
>未晓不妨权放过，切身须要急思量。
>『读书』陆九渊

阅读是为了内化，内化说白了就是把东西烂到心里，然后变成自己的一部分。所以呢？读书用读『九阴真经』的态度是很不错的。至于分享欲、读书笔记，那就去他妈的曲里拐弯罗圈腿吧。

## 阅读统计：
阅读是件困难事，很容易放弃，而适当的统计可以让这个过程更有成就感，让阅读更持续更顺利地进行下去。咱家在六月份(2023)把逐渐把以前(2017+)可以找到的阅读记录进行汇总(日记、图书馆阅读记录、豆瓣等)，整理到了[阅读之旅](https://sionreading.notion.site/)上。

读完一本书后，添加一些必要的数据，打标签，闲了再从相机里(实体书)或豆瓣里(电子书)上传封面，很是方便。之后再把导出的数据用excel整理，echarts来绘制图表，一目了然。

<iframe style="border-radius:12px" src="/pdf/charts/媒介.html" width="100%" height="400" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
<iframe style="border-radius:12px" src="/pdf/charts/reading-month.html" width="100%" height="400" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

![阅读年度统计](https://pic.si-on.top/2023/08/%E9%98%85%E8%AF%BB%E5%B9%B4%E5%BA%A6%E7%BB%9F%E8%AE%A1.png)

## 书摘

此[PDF文件](https://mozilla.github.io/pdf.js/web/viewer.html?file=https://blog.si-on.top/pdf/书摘.pdf)是从[书见](https://memo.bookfere.com)里导出的书摘，此外还有一个Kindle风格的书摘单页：[Kindle.si-on.top](https://Kindle.si-on.top)
```html pdf嵌入代码
<div class="w-full overflow-hidden rounded" style="height: 90vh;"><iframe src="https://mozilla.github.io/pdf.js/web/viewer.html?file=https://blog.si-on.top/pdf/书摘.pdf" frameborder="0" width="100%" height="100%"></iframe></div>
```

<iframe style="border-radius:12px" src="https://Kindle.si-on.top" width="100%" height="719.8" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>



## 观影
电影每每能给人带来超越文字的体会，看过一场电影，仿佛就活过了另一种人生，走过了另外一条路，超脱了原来的经验与自己。这种体会往往会以一种莫名的情绪萦绕在心头，或喜或忧，或欢笑或泪目，总能带来一种新的体会。

今天开始，我将开始记录观影/观视频中的体会（博客目前已经生活化了，再生活一点又有什么呢）

### 力量之戒

![回光返照](../images/20241012/Pasted%20image%2020241003195223.png)
![自不量力](../images/20241012/Pasted%20image%2020241003195326.png)



![太惨了](../images/20241012/Pasted%20image%2020241003200547.png)

![居然哭了，这个角色丰富起来了](../images/20241012/Pasted%20image%2020241003200709.png)


![](../images/20241012/Pasted%20image%2020241003202746.png)

![](../images/20241012/Pasted%20image%2020241003203223.png)