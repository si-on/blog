---
title: 街坊📮
mathjax: true
date: 2021-09-13 21:59:16
updated: 2023-09-11 13:53:45
description: 自2023-9-11日起，开启街坊页面
keywords:
top_img: 
aside: false
comments:
---
> 不要走在我后面，因为我可能不会引路；不要走在我前面，因为我可能不会跟随；请走在我的身边，做我的朋友。 --阿贝尔·加缪

> Don’t walk behind me, I may not lead.   
> Don’t walk in front of me, I may not follow.   
> Just walk beside me and be my friend.   


{% sitegroup %}  
{% site  Lyunvy's Blog, url=https://blog.lyunvy.top, screenshot=https://blog.lyunvy.top/images/screenshot.webp, avatar=https://blog.lyunvy.top/images/linklogo.webp, description=错把良辰作素日，不识美景怨天时。 %}  
{% site 蒙奇日记, url=https://luffy.cc, screenshot=../images/20240609/index-20240730110119186.webp, avatar= https://luffy.cc/img/luffy.webp, description=用文字记录生活 %}  
{% site 墨希MoXiify, url=https://note.moxiify.cn, screenshot=../images/20240609/index-20240805194745480.webp, avatar= https://q1.qlogo.cn/g?b=qq&nk=3188468169&s=640, description=做一条，逆流的鱼。 %} 
{% endsitegroup %}

-----

- class_name: 
  class_desc: 
  link_list:
    - name: CrazyWong
      link: https://blog.crazywong.com/
      avatar: https://blog.crazywong.com/img/avatar.png
      descr: 熱衷學習，熱衷生活(butterfly主题作者)
    - name: 始终 
      link: https://liam.page/ 
      avatar: https://liam.page/images/avatar/avatar.webp
      descr: 不忘初心(Latex技术博客)
    - name: 海牛懒猫
      link: https://manateelazycat.github.io/
      avatar: https://manateelazycat.github.io/favicon.ico
      descr: 折腾+阅读+开源
    - name: 一大加贝
      link: https://tianheg.co
      avatar: https://tianheg.co/images/logo.png
      descr: 会认识自己的人
    - name: 木子
      link: https://blog.k8s.li
      avatar: https://blog.k8s.li/avatar.png
      descr: 简单、优质、深邃的博客 
    - name: 离别歌
      link: https://www.leavesongs.com/
      avatar: https://www.leavesongs.com/static/cactus/images/favicon.ico
      descr:  黑客/程序员/作家
    - name: 老杰的博客
      link: https://oldj.net/
      avatar: https://oldj.net/favicon.ico
      descr:  AI/思考/阅读/编程
    - name: 书趣
      link: https://mybookjoy.com/
      avatar: https://baileysbookshome.files.wordpress.com/2020/11/cropped-tab-logo.jpg?w=32
      descr:  Kindle/书单/英文
    - name: 写意
      link: https://writee.org/read
      avatar: https://writee.org/favicon.ico
      descr:  去中心化写作平台(030)
    - name: Jamie Zawinski
      link: https://www.jwz.org/blog/
      avatar: https://www.jwz.org/favicon.ico
      descr:  Hack



