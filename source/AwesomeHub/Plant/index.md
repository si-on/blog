---
title: 田圃事略
top_img: /images/20241012/IMG_20240509_094923%20(1).jpg
katex: false
aside: false
date: 2024-10-25 
comments:
keywords:
aplayer:
highlight_shrink:
description: 
---


## 绿植第一期•代号：若仙
三月份买水仙花一盆，两个月后开花。
![初开](../../images/20241012/IMG_20240311_222552.jpg)
![斯美，若仙！斯香，若霖！](../../images/20241012/IMG_20240315_194548.jpg)
## 绿植第二期•代号：🪴维尔姆兰
* 时间：2024/5/30~2025/2/13
* 位置：宿舍四楼朝北的阳台
* 花的罗列
	* 毛蕨：完美适应
	* 三叶草：完美适应
	* ~~春飞蓬：四个月后，因为气温变化、一年生草本自然死亡的缘故，春飞蓬死亡~~
	* 一小撮木贼：艰难适应

自从水仙花谢了后，宿舍的阳台就处于荒芜状态，除了上个月移植的一棵三叶草和一颗春飞蓬（生命力巨旺盛，根都长到土外面了）外，基本上都是土。

今天午饭后，在A桥下面偶然看到了一堆蕨类植物，便满心欢喜地薅回来种了，蕨类植物在流行文化中有一种复古的文艺的含义，就算没有花那么艳丽，陶冶陶冶情操也是极好的。

薅蕨草的时候刚好在听童声合唱：迷人的维尔姆兰，那么这次翻新叫做🪴维尔姆兰计划好了

查了后发觉这种蕨类植物叫做『毛蕨』，南方很稀松平常的一种蕨。就是不知道等到长大了会不会跟『风之谷』动画里那样放出来满天的孢子。
![翻修前，由黑莓Priv拍摄](../../images/20241012/index-20241231230547767.png)
![翻修后，由黑莓Priv拍摄](../../images/20241012/index-20241231230556100.png)

![《毛蕨：维尔姆兰最后的骄傲》，由尼康Zfc拍摄于2024.12.13](../../images/20241012/index-20241231230750881.png)
## 绿植第三期•代号：🌪️有机漩涡(Organic Swirl)
* 时间：2024/10/27~2024/2/13
* 位置：点室16楼朝南的阳台—>撇室五楼朝北阳台
* 花的罗列
	* 茉莉：元祖，盆里放有从天一阁带回来的长有苔藓的小瓦片(11.20开始，有枝枯萎，11.30将枯枝剪掉，12.31状态正常)
	* 玫瑰：某天忘记浇水，正赶上大晴天，一天被四朵花被活生生晒死。它上边悬挂的枯花是一种警醒——兴趣要培养，呵护，散养只会长出来叶子，开不成花。(11.23长出两花苞，12.1初开，12.15盛开，12.31渐衰)
	* 十二卷：9.15购置，一开始根没扎牢，导致旧叶枯萎，新叶才长出来，12.15长出第三片新叶，1.27生意盎然)
	* 多肉：楼顶某人家花园里多肉的小分株(~9.20完美移植)
	* ~~水仙：绿植第一『斯美，若仙』留下来的两颗球茎+新买的四颗(长得“杀气腾腾”，~12.10天冷，根生长缓慢，1-24转移到土中，2-6搬家时“放生”)~~
	* ~~兰花：10.27，挖土时看见的，挺好看(完美存活，2-6搬家时“放生”)~~
	* ~~指甲草：10.27 挖土时发现，挑了一棵小的(开花，花落，存活，天冷瑟缩，2-6搬家时“放生”)~~
	* 罗汉松小枝：10.27看水培下能不能生根，整个迷你版的，成功的话，就是第三里唯一的木本了。(仍然存活，还绿油油的，简直神了，12.31，此枝仍然绿油油的，不死而且又长出来一层叶子就很离谱，2.13仍然不死，我愿称之为——『不死罗汉』)
	* ~~君子兰：新买的，两天了没枯萎，希望可以活下去。(10.27左右，花架坍塌，导致连根折断，11.25号发现枯萎，R.I.P放置于下层花盆中，12.31，叶子未干枯，处于要死不死要活不活状态中，2-6搬家时“放生”)~~
	* 井栏边草：12月某日对受《毛蕨：维尔姆兰最后的骄傲》的影响对蕨草产生极大兴趣，搜遍小区找寻得此草（勉强适应中，2.13仍然存活）




入点室(给出租屋的名字)至今已一月有余，有意义的东西没干出来，花儿倒是越养越多。

还记得租房子当天，铺盖还没搬过来，什么东西都没齐备，鬼使神差地买了一盆茉莉。茉莉进了屋后才是大包小包，牙刷牙膏。现在回想这事，真觉得那么可笑，大概是基因所致吧。之后又陆陆续续添了玫瑰，多肉，水仙等等。

10.27下午发生了“炸盆”事故——简易花架不堪重负，西边忽然倒塌，致使水仙瓷盆一破为三，粘好盆后，趁它晾干时出去寻些好土来，屋里的土实在太少。冒着稀拉拉，绵软软的毛毛雨，走到东边的河口，徒手挖了些颜色看起来很健康的湿土，又从工地边捧了两捧沙(养多肉用)，掂量一下有七八斤，费老鼻子劲才把它搬到屋里。挖泥巴时候听了『Organic Minimalism』，里头『Cultural Swirl』一曲尤为印象深刻，便按照惯例把这首歌名组合一下成为计划的名字。整顿好阳台后，根据阴阳(采光)，风水(插座，窗户，水龙头)，把屋里的桌子柜子换换位置，顿时有了点新鲜感！当然，要是再能活下去就更好了。

2.6日，在马哥帮助下，搬家到『撇室』，搬家前放生了一批花。此屋阳台朝北，光照也不是那么强烈，相比朝南，这些花反而能适应地更好了。
唯一麻烦的是隔一段时间要转转方向，尤其是那盆十二卷。
{% gallery %}
![元祖：茉莉+水仙](https://fish.si-on.top/file/0ce71494-6f0c-4771-9957-49b71533ce47.jpg)

![分手玫瑰](../../images/20241012/index-20250101002413431.png)

![🍂水之仙兮列如麻](../../images/20241012/index-20250101002440584.png)

![空气高尔夫](../../images/20241012/index-20250101002547969.png)

![](../../images/20241012/IMG_20250213_232936.jpg)
![](../../images/20241012/1000042735.jpg)
![](../../images/20241012/IMG_20250213_231843.jpg)
![](../../images/20241012/1000042702.jpg)
![](../../images/20241012/1000042701.jpg)
![](../../images/20241012/1000042723.jpg)
![](../../images/20241012/1000042717%20(1).jpg)
![](../../images/20241012/1000042696.jpg)
{% endgallery %}
