---
title: {{title}}
abbrlink: unnamedPost
categories: 
tags: []
date: {{date:YYYY-MM-DD HH:mm:ss}}
updated: {{date:YYYY-MM-DD HH:mm:ss}}
cover: 
root: ../../
copyright_author: 
copyright_author_href: 
copyright_url: 
copyright_info: 
aside: 
comments: 
katex: false
password: 
theme: xray
hidden: 
published: false
description: 
sticky: 
keywords:
---

> <center>当你在引用别人的时候，你在引用自己。</center>
> <p align="right">——Sion</p>

{% folding green, ✍文章痕迹 %}
{% timeline 写作时间线, green %}
<!-- timeline 2023-08-01-->
 
<!-- endtimeline -->
{% endtimeline %}
{% endfolding %}
{% markmap 300px %}

{% endmarkmap %}

-----

